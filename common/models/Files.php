<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "files".
 *
 * @property integer $file_id
 * @property string $title
 * @property string $created
 * @property string $uploaded
 * @property integer $entity
 * @property integer $type
 * @property string $extension
 * @property string $description
 * @property string $patch
 */
class Files extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'entity', 'extension', 'patch'], 'required'],
            [['created', 'uploaded'], 'safe'],
            [['entity', 'type'], 'integer'],
            [['description'], 'string'],
            [['title', 'patch'], 'string', 'max' => 255],
            [['extension'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'file_id' => Yii::t('app', 'File ID'),
            'title' => Yii::t('app', 'Title'),
            'created' => Yii::t('app', 'Created'),
            'uploaded' => Yii::t('app', 'Uploaded'),
            'entity' => Yii::t('app', 'Entity'),
            'type' => Yii::t('app', 'Type'),
            'extension' => Yii::t('app', 'Extension'),
            'description' => Yii::t('app', 'Description'),
            'patch' => Yii::t('app', 'Patch'),
        ];
    }
}
