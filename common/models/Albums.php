<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "albums".
 *
 * @property integer $id
 * @property string $name
 * @property string $timestamp
 */
class Albums extends \yii\db\ActiveRecord
{
public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        
		if (Yii::$app->language == 'en') {return 'albums_en';} else {return 'albums';} 
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['timestamp'], 'safe'],
           
			[['file'], 'file', 'maxFiles' => 40, 'extensions' => 'jpg, png', 'mimeTypes' => 'image/jpeg, image/png',],
            [['name'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('app', 'Name'),
            'timestamp' => Yii::t('app', 'Timestamp'),
        ];
    }
}
