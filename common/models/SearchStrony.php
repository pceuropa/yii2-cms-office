<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Strony;

/**
 * SearchStrony represents the model behind the search form about `common\models\Strony`.
 */
class SearchStrony extends Strony
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'czlonek', 'category_id'], 'integer'],
            [['autor', 'czas', 'tytul', 'tresc', 'data', 'zdjecie'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Strony::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'czas' => $this->czas,
            'czlonek' => $this->czlonek,
            'category_id' => $this->category_id,
        ]);

        $query->andFilterWhere(['like', 'autor', $this->autor])
            ->andFilterWhere(['like', 'tytul', $this->tytul])
            ->andFilterWhere(['like', 'tresc', $this->tresc])
            ->andFilterWhere(['like', 'data', $this->data])
            ->andFilterWhere(['like', 'zdjecie', $this->zdjecie]);

        return $dataProvider;
    }
}
