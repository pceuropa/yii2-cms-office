<?php
namespace common\models;

use Yii;

class BannersFooter extends \yii\db\ActiveRecord{

public $files;
const IMG_FOLDER = 'footer';


public static function tableName(){

	if (Yii::$app->language == 'en') {
		return 'banners_footer_en';
	} else {
		return 'banners_footer';
	}
}	
	
public function rules(){
        return [
            [['url', 'name'], 'required'],
            [['name'], 'string', 'max' => 255],
            ['files', 'file', 'extensions' => ['png', 'jpg', 'gif'], 'maxSize' => 2024*1024],
            [['url'], 'url']
        ];
    }
        
public function attributeLabels(){
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name of link'),
            'url' => Yii::t('app', 'Url'),
            'image' => Yii::t('app', 'image'),
            'files' => Yii::t('app', 'File'),
            'serialize' => Yii::t('app', 'Serialize'),
            'timestamp' => Yii::t('app', 'Timestamp'),
        ];
    }
    
public function findAllAsArray(){
    	return self::find()->orderBy(['serialize' => SORT_ASC, ])->asArray()->all();
    }
	
}
