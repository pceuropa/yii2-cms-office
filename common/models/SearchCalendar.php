<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Calendar;

/**
 * SearchCalendar represents the model behind the search form about `common\models\Calendar`.
 */
class SearchCalendar extends Calendar
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'czlonek'], 'integer'],
            [['date', 'event', 'content_event', 'place'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Calendar::find()->orderBy(['date' => SORT_DESC,]);;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
				'sort' => [
        'defaultOrder' => [
            'date' => SORT_DESC
        ]
    ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'czlonek' => $this->czlonek,
        ]);

        $query->andFilterWhere(['like', 'event', $this->event])
            ->andFilterWhere(['like', 'content_event', $this->content_event])
            ->andFilterWhere(['like', 'place', $this->place]);

        return $dataProvider;
    }
}
