<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

class Countries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'countries';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_iso3', 'area', 'population'], 'integer'],
            [['country'], 'string', 'max' => 44],
            [['country_iso', 'fips', 'continent'], 'string', 'max' => 2],
            [['country_iso2'], 'string', 'max' => 3],
            [['capital'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'country' => Yii::t('app', 'Country'),
            'country_iso' => Yii::t('app', 'Country Iso'),
            'country_iso2' => Yii::t('app', 'Country Iso2'),
            'country_iso3' => Yii::t('app', 'Country Iso3'),
            'fips' => Yii::t('app', 'Fips'),
            'capital' => Yii::t('app', 'Capital'),
            'area' => Yii::t('app', 'Area'),
            'population' => Yii::t('app', 'Population'),
            'continent' => Yii::t('app', 'Continent'),
        ];
    }
	
	public function getCountryArray()
    {
	$countries = Countries::find()->select('id,country')->all();
	return ArrayHelper::map($countries , 'country','country');
    }	
}
