<?php
namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
class Tags extends \yii\db\ActiveRecord
{

public static function tableName() 
{ 
	return 'tags';
} 

public function rules()
{
	return [

			[['name'], 'required'],
            [['news', 'news_en'], 'integer'],
            [['name'], 'string', 'max' => 75]
	];
}

public function attributeLabels()
{
	return [
	'name' => 'Name',
	];
}

public function getTags()
{

	$groups=Tags::find()->select('name')->distinct('name')->asArray()->all();
	return $listData=ArrayHelper::map($groups,'name','name');

}

public function saveTags($table, $tags, $modelId)
    {
	foreach ($tags as $val):
		$tag = new Tags;
		$tag->name = $val;
		($table == 'news') ? $tag->news = $modelId : $tag->news_en = $modelId;
		$tag->save();
	endforeach;
    }
	
}
