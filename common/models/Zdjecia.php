<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zdjecia".
 *
 * @property integer $id
 * @property integer $id_albumu
 * @property string $opis
 * @property string $zdjecie
 * @property string $dodano
 */
class Zdjecia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zdjecia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_albumu', 'opis', 'zdjecie'], 'required'],
            [['id_albumu'], 'integer'],
            [['dodano'], 'safe'],
            [['opis'], 'string', 'max' => 200],
            [['zdjecie'], 'string', 'max' => 18]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_albumu' => Yii::t('app', 'Id Albumu'),
            'opis' => Yii::t('app', 'Opis'),
            'zdjecie' => Yii::t('app', 'Zdjecie'),
            'dodano' => Yii::t('app', 'Dodano'),
        ];
    }
}
