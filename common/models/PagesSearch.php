<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Pages;

/**
 * PagesSearch represents the model behind the search form about `common\models\Pages`.
 */
class PagesSearch extends Pages
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'czlonek', 'news_add'], 'integer'],
            [['autor', 'timestamp', 'tytul', 'tresc', 'data', 'zdjecie', 'seo_title', 'seo_url', 'cat_list'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pages::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort' => [
			'defaultOrder' => [ 'data' => SORT_DESC]
			]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'timestamp' => $this->timestamp,
            'czlonek' => $this->czlonek,
            'news_add' => $this->news_add,
        ]);

        $query->andFilterWhere(['like', 'autor', $this->autor])
            ->andFilterWhere(['like', 'tytul', $this->tytul])
            ->andFilterWhere(['like', 'tresc', $this->tresc])
            ->andFilterWhere(['like', 'data', $this->data])
            ->andFilterWhere(['like', 'zdjecie', $this->zdjecie])
            ->andFilterWhere(['like', 'seo_title', $this->seo_title])
            ->andFilterWhere(['like', 'seo_url', $this->seo_url])
            ->andFilterWhere(['like', 'cat_list', $this->cat_list]);

        return $dataProvider;
    }
}
