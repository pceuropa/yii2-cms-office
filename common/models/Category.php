<?php
namespace common\models;
use Yii;

class Category extends \yii\db\ActiveRecord {

	public static function tableName(){
		if (Yii::$app->language == 'en') {return 'category_en';} else {return 'category';} 
	}

	public function rules(){
		    return [
		        [['name', 'group'], 'required'],
		        [['group'], 'integer'],
		        [['create'], 'safe'],
		        [['name'], 'string', 'max' => 255]
		    ];
		}

	public function attributeLabels(){
		    return [
		        'name' => Yii::t('app', 'Category name'),
		        'group' => Yii::t('app', 'Parent category'),
		        'create' => Yii::t('app', 'Date of creation'),
		    ];
	}
	
	public function getPags(){
		    return $this->hasMany(Pages::className(), ['id' => 'pages_id'])
			->viaTable('relationships', ['category_id' => 'id']);;
	}
	public function getNameById($id){
		if (($model = Category::findOne($id)) !== null) {
	        return $model->name;
	    } else {
	        return null;
	    }
	
		
	}
	
	public function getAllCategories() { // to view index
		
		foreach (Category::find()->all() as $v){
					$array[$v['id']] = $v['name'];
			
		}
	
		return $array;
	}
	
	protected function findModel($id) {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
	
}
