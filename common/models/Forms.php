<?php

namespace common\models;

use Yii;
class Forms extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'forms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          //  [['autor', 'title', 'context', 'date', 'date_expire', 'max', 'seo_title', 'seo_url'], 'required'],
            [['date', 'date_expire'], 'safe'],
            [['body'], 'string'],
            [['max', 'czlonek'], 'integer'],
            [['autor', 'seo_title', 'seo_url'], 'string', 'max' => 100],
            [['title'], 'string', 'max' => 400],
        ];
    }

    public function attributeLabels(){
        return [
            'form_id' => Yii::t('app', 'ID'),
            'autor' => Yii::t('app', 'Autor'),
            'title' => Yii::t('app', 'Title'),
            'body' => Yii::t('app', 'Body'),
            'date' => Yii::t('app', 'Date'),
            'date_expire' => Yii::t('app', 'Date Expire'),
            'max' => Yii::t('app', 'Max'),
            'seo_title' => Yii::t('app', 'Seo Title'),
            'seo_url' => Yii::t('app', 'Seo Url'),
            'czlonek' => Yii::t('app', 'Czlonek'),
        ];
    }

		public function findModel($id){
	    if (($model = Forms::find()->where(['form_id' => $id])->one()) !== null) {
	        return $model;
	    } else {
	        return (object) [
					'form' => '{}',
				  ];
	    }
	}
}
