<?php
namespace common\models;
use Yii;


class Calendar extends \yii\db\ActiveRecord
{

public static function tableName()     
	{
	if (Yii::$app->language == 'en')
	{return 'calendar_en';} else
	{return 'calendar';} 	
	}


public function rules()
    {
        return [
            [['date', 'event', 'place'], 'required'],
            [['date'], 'safe'],
            [['content_event'], 'string'],
            [['czlonek'], 'integer'],
            [['event', 'place'], 'string', 'max' => 255]
        ];
    }


public function attributeLabels()
    {
        return [
            'date' => Yii::t('app', 'Date'),
            'event' => Yii::t('app', 'Event'),
            'content_event' => Yii::t('app', 'Content Event'),
            'place' => Yii::t('app', 'Place'),
            'czlonek' => Yii::t('app', 'hidden'),
        ];
    }
public function findEventByDate($date)
	{
	$event = Calendar::find()
		->where(['date' => $date])
		->all();
return $event ;
	}
	
}
