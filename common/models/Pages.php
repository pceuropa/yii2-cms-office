<?php
namespace common\models;
use Yii;
class Pages extends \yii\db\ActiveRecord
{

public $category_id;
public $news_add;
public $pages_add;

public static function tableName()
    {
		if (Yii::$app->language == 'en') {return 'pages_en';} else {return 'pages';} 
    }

	
public function rules()
    {
        return [
            [['tytul', 'category_id' ], 'required'],
            [['timestamp'], 'safe'],
            [['tresc'], 'string'],
            [['czlonek'], 'integer', 'max' => 1],
            [['news_add', 'pages_add'], 'default'],
            [['autor', 'seo_url'], 'string', 'max' => 130],
            [['tytul'], 'string', 'max' => 400],
            [['data', 'cat_list'], 'string', 'max' => 30],
            [['seo_title'], 'string', "min" => 50, 'max' => 75]
			];
    }

    /**
     * @inheritdoc
     */
public function attributeLabels()
    {
        return [
            'autor' => Yii::t('app', 'Autor'),
            'timestamp' => Yii::t('app', 'Time'),
            'tytul' => Yii::t('app', 'Title'),
            'tresc' => Yii::t('app', 'Content'),
            'data' => Yii::t('app', 'Data'),
            'zdjecie' => Yii::t('app', 'Photo'),
            'seo_title' => Yii::t('app', 'SEO Title'),
            'seo_url' => Yii::t('app', 'SEO Url'),
            'czlonek' => Yii::t('app', 'hidden'),
            'cat_list' => Yii::t('app', 'Categories'),
            'category_id' => Yii::t('app', 'Categories'),
            'news_add' => Yii::t('app', 'Related news'),
            'pages_add' => Yii::t('app', 'Related pages'),
        ];
    }

public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])
		->viaTable('relationships', ['pages_id' => 'id']);
    }	
	
public function getCategories_en()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])
		->viaTable('relationships_en', ['pages_id' => 'id']);
    }	

	//	4 			3 			2		1
	// pages.id -> pages_id ::    news_add -> id
public function getRelatedNews()
{	
	return $this->hasMany(News::className(), ['id' => 'news_add'])
	->viaTable('pages_add', ['pages_id' => 'id']);
	
	//  $RelatedPages = News::findOne(690)->relatedNews;
}

public function getRelatedNewsIfExist($model)
{	
	$relatedNews = Pages::findOne($model->id)->relatedNews;
	if ($relatedNews) { return $relatedNews;} 
	
	return false;
	
}

public function getRelatedPages()
{	
	return $this->hasMany(Pages::className(), ['id' => 'pages_add'])
	->viaTable('pages_add', ['pages_id' => 'id']);
	
	//  $RelatedPages = News::findOne(690)->relatedNews;

}	

public function getRelatedPagesIfExist($model)
{	
	$relatedPages = Pages::findOne($model->id)->relatedPages;
	if ($relatedPages) { return $relatedPages;} 
	
	return false;
	
}	
}
