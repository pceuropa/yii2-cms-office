<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "view_albums".
 *
 * @property integer $id
 * @property string $name
 * @property string $timestamp
 * @property string $iloscphoto
 * @property string $photo
 */
class ViewAlbums extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'view_albums';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'iloscphoto'], 'integer'],
            [['name', 'photo'], 'required'],
            [['timestamp'], 'safe'],
            [['name'], 'string', 'max' => 200],
            [['photo'], 'string', 'max' => 18]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'timestamp' => 'Timestamp',
            'iloscphoto' => 'Iloscphoto',
            'photo' => 'Photo',
        ];
    }
}
