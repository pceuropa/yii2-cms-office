<?php
namespace common\models;

use Yii;

class FormOpenDays2015 extends \yii\db\ActiveRecord

{
public $adjustment1;
public $adjustment2;
public $email_repeat;


public static function tableName()
	{
		return 'form_open_days_2015';
	}


public function rules()
	{
	return [
	[['firstname', 'lastname', 'birth', 'phone', 'email', 'email_repeat',  'name_org', 'depart', 'section', 'adress_org', 'room', 'adjustment1', 'adjustment2'], 'required'],
	[['room', 'newsletter'], 'integer'],
	
	[['firstname', 'lastname', 'name_org', 'adress_org', 'section', 'depart' ], 'string', 'max' => 90],
	[['id_number', 'birth', 'phone', 'room_person_firstname', 'room_person_lastname'], 'string', 'max' => 35],
			
	['email', 'filter', 'filter' => 'trim'],
	['email', 'email'],
	['email3', 'filter', 'filter' => 'trim'],
	['email3', 'email'],
	['email', 'unique', 'targetClass' => '\common\models\FormOpenDays2015', 'message' => Yii::t('app', 'This email address is already registered.')],
	[['attention'], 'string', 'max' => 2500],
		
	[['email_repeat'], 'compare', 'compareAttribute' => 'email', 'message' => Yii::t('app', 'Email addresses do not match')],
	[['adjustment1', 'adjustment2'], 'compare', 'compareValue' => 1, 'operator' => '==', 'message' => 'required']

		];
	}

public function scenarios()
    {
        return [
            'default' => ['firstname', 'lastname', 'birth', 'id_number', 'phone', 'email', 'email_repeat', 'email3', 'name_org', 'depart', 'section', 'adress_org', 'room', 'room_person_firstname', 'room_person_lastname', 'attention', 'adjustment1', 'adjustment2'],
            'backend' => ['firstname', 'lastname', 'birth', 'id_number', 'phone', 'email', 'email3', 'name_org', 'depart', 'section', 'adress_org', 'room', 'room_person_firstname', 'room_person_lastname', 'attention']
        ];
    }
	

public function attributeLabels()
	{
		return [
			'firstname' => Yii::t('app', 'First name *'),
            'lastname' => Yii::t('app', 'Last name *'),
            'birth' => Yii::t('app', 'Birth *'),
            'id_number' => Yii::t('app', 'Id Number *'),
            'phone' => Yii::t('app', 'Phone *'),
            'email' => Yii::t('app', 'Email *'),
            'email_repeat' => Yii::t('app', 'Email repeat *'),
			
            
            'name_org' => Yii::t('app', 'Organisation name  *'),
            'depart' => Yii::t('app', 'Department  *'),
            'section' => Yii::t('app', 'Section  *'),
            'adress_org' => Yii::t('app', 'Address  *'),
			
            'room' => Yii::t('app', 'Room'),
            'room_person_firstname' => Yii::t('app', 'Room Person Firstname'),
			'room_person_lastname' => Yii::t('app', 'Room Person Lastname'),
			
			'email3' => Yii::t('app', 'Additional email address'),
            'attention' => Yii::t('app', 'Attention'),
            'adjustment1' => Yii::t('app', ''),
            'adjustment2' => Yii::t('app', ''),
            'newsletter' => Yii::t('app', 'Newsletter'),
            'id' => Yii::t('app', 'ID'),
			
			
		];
	}
}
