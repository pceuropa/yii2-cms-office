<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\FormEnergyWeek2015;

/**
 * FormEnergyWeek2015Search represents the model behind the search form about `common\models\FormEnergyWeek2015`.
 */
class FormEnergyWeek2015Search extends FormEnergyWeek2015
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'birth', 'id_number', 'phone', 'email', 'email3', 'name_org', 'depart', 'section', 'adress_org', 'room_person_firstname', 'room_person_lastname', 'attention'], 'safe'],
            [['room', 'newsletter', 'id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormEnergyWeek2015::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'room' => $this->room,
            'newsletter' => $this->newsletter,
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'birth', $this->birth])
            ->andFilterWhere(['like', 'id_number', $this->id_number])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'email3', $this->email3])
            ->andFilterWhere(['like', 'name_org', $this->name_org])
            ->andFilterWhere(['like', 'depart', $this->depart])
            ->andFilterWhere(['like', 'section', $this->section])
            ->andFilterWhere(['like', 'adress_org', $this->adress_org])
            ->andFilterWhere(['like', 'room_person_firstname', $this->room_person_firstname])
            ->andFilterWhere(['like', 'room_person_lastname', $this->room_person_lastname])
            ->andFilterWhere(['like', 'attention', $this->attention]);

        return $dataProvider;
    }
}
