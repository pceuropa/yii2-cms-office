<?php
namespace common\models;

use Yii;

class AnkietaUczelni extends \yii\db\ActiveRecord

{

public $email_repeat;


public static function tableName()
	{
		return 'ankieta-uczelni';
	}


public function rules()
	{
	return [
	[['firstname', 'lastname', 'phone', 'email', 'email_repeat',  'name_org'], 'required'],
	[['firstname', 'lastname', 'name_org', 'phone' ], 'string', 'max' => 90],
	['email', 'filter', 'filter' => 'trim'],
	['email', 'email'],
	['email', 'unique', 'targetClass' => '\common\models\AnkietaUczelni', 'message' => 'Ten adres e-mail jest już zarejestrowany.'],
	[['email_repeat'], 'compare', 'compareAttribute' => 'email', 'message' => 'Adresy email nie zgadzają się'],
	[['attention', "attention1","attention2","attention3","attention4","attention5","attention6"], 'string', 'max' => 3000],
	[['time1', "time2","time3","time4","time5","time6"], 'safe']	
	

		];
	}
	

public function attributeLabels()
	{
		return [
				'firstname' => Yii::t('app', 'First name *'),
				'lastname' => Yii::t('app', 'Last name *'),
				'birth' => Yii::t('app', 'Birth *'),
				'id_number' => Yii::t('app', 'Id Number *'),
				'phone' => Yii::t('app', 'Phone *'),
				'email' => Yii::t('app', 'Email *'),
				'email_repeat' => Yii::t('app', 'Email repeat *'),


				'name_org' => Yii::t('app', 'Organisation name  *'),
				'depart' => Yii::t('app', 'Department  *'),
				'section' => Yii::t('app', 'Section  *'),
				'adress_org' => Yii::t('app', 'Address  *'),

				'room' => Yii::t('app', 'Room'),
				'room_person_firstname' => Yii::t('app', 'Room Person Firstname'),
				'room_person_lastname' => Yii::t('app', 'Room Person Lastname'),

				'email3' => Yii::t('app', 'Additional email address'),
				'attention' => Yii::t('app', 'Attention'),
				'attention1' => 'Wizyty studyjne',
				'attention2' => 'Bilateralne spotkania eksperckie',
				'attention3' => 'Spotkania brokerskie',
				'attention4' => 'Seminaria informacyjne',
				'attention5' => 'Szkolenia/warsztaty',
				'attention6' => 'Inne',
				'adjustment1' => Yii::t('app', ''),
				'adjustment2' => Yii::t('app', ''),
				'newsletter' => Yii::t('app', 'Newsletter'),
				'id' => Yii::t('app', 'ID'),
			
			
		];
	}
}
