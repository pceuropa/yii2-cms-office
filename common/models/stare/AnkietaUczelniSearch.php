<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AnkietaUczelni;

/**
 * AnkietaUczelniSearch represents the model behind the search form about `common\models\AnkietaUczelni`.
 */
class AnkietaUczelniSearch extends AnkietaUczelni
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
	return [
	[['attention', "attention1","attention2","attention3","attention4","attention5","attention6"], 'string', 'max' => 3000],
	[['time1', "time2","time3","time4","time5","time6"], 'safe']		
	];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AnkietaUczelni::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }




        return $dataProvider;
    }
}
