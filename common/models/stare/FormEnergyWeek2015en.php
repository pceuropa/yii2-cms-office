<?php
namespace common\models;

use Yii;

class FormEnergyWeek2015en extends \yii\db\ActiveRecord
{
public $adjustment1;
public $adjustment2;
public $email_repeat;


	public static function tableName()
	{
		return 'form_energy_week_2015en';
	}


public function rules()
	{
	return [
	[['firstname', 'lastname', 'birth', 'phone', 'email', 'email_repeat',  'name_org', 'depart', 'section', 'adress_org', 'adjustment1'], 'required'],
	[['room', 'newsletter'], 'integer'],
	
	[['firstname', 'lastname', 'name_org', 'adress_org', 'section', 'depart' ], 'string', 'max' => 90],
	[['id_number', 'birth', 'phone', 'room_person_firstname', 'room_person_lastname'], 'string', 'max' => 35],
			
	['email', 'filter', 'filter' => 'trim'],
	['email', 'email'],
	['email3', 'filter', 'filter' => 'trim'],
	['email3', 'email'],
	['email', 'unique', 'targetClass' => '\common\models\FormEnergyWeek2015en', 'message' => 'Ten adres e-mail jest już zarejestrowany.'],
	[['attention'], 'string', 'max' => 500],
		
	[['email_repeat'], 'compare', 'compareAttribute' => 'email', 'message' => 'Adresy email nie zgadzają się'],
	[['adjustment1'], 'compare', 'compareValue' => 1, 'operator' => '==', 'message' => 'required']

		];
	}

	public function scenarios()
    {
        return [
            'default' => ['firstname', 'lastname', 'birth', 'id_number', 'phone', 'email', 'email_repeat', 'email3', 'name_org', 'depart', 'section', 'adress_org', 'room', 'room_person_firstname', 'room_person_lastname', 'attention', 'adjustment1', 'adjustment2'],
            'backend' => ['firstname', 'lastname', 'birth', 'id_number', 'phone', 'email', 'email3', 'name_org', 'depart', 'section', 'adress_org', 'room', 'room_person_firstname', 'room_person_lastname', 'attention']
        ];
    }
	

	public function attributeLabels()
	{
		return [
         'firstname' => Yii::t('app', 'First name *'),
            'lastname' => Yii::t('app', 'Last name *'),
            'birth' => Yii::t('app', 'Birth *'),
            'id_number' => Yii::t('app', 'Id Number *'),
            'phone' => Yii::t('app', 'Phone *'),
            'email' => Yii::t('app', 'Email *'),
            'email_repeat' => Yii::t('app', 'Email repeat *'),
            'email3' => Yii::t('app', 'Email3'),
            'name_org' => Yii::t('app', 'Organisation name  *'),
            'depart' => Yii::t('app', 'Job function  *'),
            'section' => Yii::t('app', 'City *'),
            'adress_org' => Yii::t('app', 'Country *'),
     //       'room' => Yii::t('app', 'Room'),
      //      'room_person_firstname' => Yii::t('app', 'Room Person Firstname'),
     //       'room_person_lastname' => Yii::t('app', 'Room Person Lastname'),
            'attention' => Yii::t('app', 'Attention'),
            'adjustment1' => Yii::t('app', ''),
            'newsletter' => Yii::t('app', 'Newsletter'),
            'id' => Yii::t('app', 'ID'),
			
			
		];
	}
}
