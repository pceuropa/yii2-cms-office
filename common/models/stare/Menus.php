<?php

namespace common\models;

use Yii;

class Menus extends \yii\db\ActiveRecord
{
public $menu_0;

public static function tableName(){
	if (Yii::$app->language == 'en') {return 'menus_en';} else {return 'menus';}
}

public function scenarios()
    {
        return [
            'default' => ['url', 'name', 'gr'],
            'group' => ['url', 'name',]
        ];
    }

public function rules()
{
return [

[['url', 'name', 'gr'], 'required'],
[['gr', 'serialize'], 'integer'],
[['name', 'url', 'menu_0'], 'string', 'max' => 200],
];
}

/**
* @inheritdoc
*/
public function attributeLabels()
{
return [
'id_menu' => Yii::t('app', 'Id Menu'),
'name' => Yii::t('app', 'Name'),
'url' => Yii::t('app', 'Url'),
'gr' => Yii::t('app', 'Group'),
];
}
}
