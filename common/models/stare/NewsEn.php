<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "news_en".
 *
 * @property integer $id
 * @property string $autor
 * @property string $timestamp
 * @property string $tytul
 * @property string $tresc
 * @property string $data
 * @property string $zdjecie
 * @property string $tags
 * @property integer $czlonek
 * @property integer $category_id
 */
class NewsEn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_en';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['autor', 'tytul', 'tresc', 'data', 'tags', 'czlonek', 'category_id'], 'required'],
            [['timestamp'], 'safe'],
            [['tresc'], 'string'],
            [['czlonek', 'category_id'], 'integer'],
            [['autor', 'tags'], 'string', 'max' => 100],
            [['tytul'], 'string', 'max' => 200],
            [['data'], 'string', 'max' => 10],
            [['zdjecie'], 'string', 'max' => 18]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('news', 'ID'),
            'autor' => Yii::t('news', 'Autor'),
            'timestamp' => Yii::t('news', 'Czas'),
            'tytul' => Yii::t('news', 'Tytul'),
            'tresc' => Yii::t('news', 'Tresc'),
            'data' => Yii::t('news', 'Data'),
            'zdjecie' => Yii::t('news', 'Zdjecie'),
            'tags' => Yii::t('news', 'Tags'),
            'czlonek' => Yii::t('news', 'Czlonek'),
            'category_id' => Yii::t('news', 'Category ID'),
        ];
    }
}
