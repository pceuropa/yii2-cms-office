<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "form_inicjatywy_eu".
 *
 * @property string $firstname
 * @property string $lastname
 * @property string $birth
 * @property string $id_number
 * @property string $phone
 * @property string $email
 * @property string $email3
 * @property string $name_org
 * @property string $depart
 * @property string $section
 * @property string $adress_org
 * @property integer $room
 * @property string $room_person_firstname
 * @property string $room_person_lastname
 * @property string $attention
 * @property integer $newsletter
 * @property integer $id
 */
class FormInicjatywyEu extends \yii\db\ActiveRecord
{
public $adjustment1;
public $adjustment2;
public $email_repeat;


    public static function tableName()
    {
        return 'form_inicjatywy_eu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
    [['firstname', 'lastname', 'phone', 'email', 'email_repeat',  'name_org', 'adress_org', 'room', 'dinner', 'adjustment1', 'adjustment2'], 'required'],
	[['room', 'dinner', 'newsletter'], 'integer'],
	
	[['firstname', 'lastname', 'name_org', 'adress_org', 'section', 'depart' ], 'string', 'max' => 90],
	[['id_number', 'birth', 'phone', 'room_person_firstname', 'room_person_lastname'], 'string', 'max' => 35],
			
	['email', 'filter', 'filter' => 'trim'],
	['email', 'email'],
	['email3', 'filter', 'filter' => 'trim'],
	['email3', 'email'],
	['email', 'unique', 'targetClass' => '\common\models\FormInicjatywyEu', 'message' => 'Ten adres e-mail jest już zarejestrowany.'],
	[['attention'], 'string', 'max' => 500],
		
	[['email_repeat'], 'compare', 'compareAttribute' => 'email', 'message' => 'Adresy email nie zgadzają się'],
	[['adjustment1'], 'compare', 'compareValue' => 1, 'operator' => '==', 'message' => 'wymagane'],
	[['adjustment2'], 'compare', 'compareValue' => 1, 'operator' => '==', 'message' => 'wymagane']
        ];
    }
public function scenarios()
    {
        return [
            'default' => ['firstname', 'lastname', 'phone', 'email', 'email_repeat', 'email3', 'name_org', 'depart', 'section', 'adress_org', 'room', 'room_person_firstname', 'dinner', 'room_person_lastname', 'attention', 'adjustment1', 'adjustment2'],
            'backend' => ['firstname', 'lastname', 'phone', 'email', 'email3', 'name_org', 'depart', 'section', 'adress_org', 'room', 'room_person_firstname', 'room_person_lastname', 'dinner', 'attention']
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
          'firstname' => 'Imię',
			'lastname' => 'Nazwisko',
			'birth' => 'Data urodzenia(dd.mm.rrrr)',
			'id_number' => 'Numer dowodu osobistego lub PESEL',
			'phone' => 'Telefon kontaktowy',
			'email' => 'Email',
			'email3' => 'Dodatkowy adres mailowy',
			'name_org' => 'Nazwa',
			'depart' => 'Departament',
			'section' => 'Wydział ',
			'adjustment1' => '',
			'adjustment2' => '',
			'room' => 'Pokój',
			'room_person_firstname' => 'Imię',
			'room_person_lastname' => 'Nazwisko',
			'dinner' => 'kolacja',
			'attention' => 'Uwagi',
        ];
    }
}
