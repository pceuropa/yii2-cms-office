<?php

namespace common\models;

use Yii;

class Relationships extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
	{
	if (Yii::$app->language == 'en') {return 'relationships_en';} else {return 'relationships';} 
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pages_id', 'category_id'], 'required'],
            [['pages_id', 'category_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pages_id' => Yii::t('app', 'Pages ID'),
            'category_id' => Yii::t('app', 'Category ID'),
        ];
    }
}
