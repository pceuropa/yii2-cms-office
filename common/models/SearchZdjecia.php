<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Zdjecia;

/**
 * SearchZdjecia represents the model behind the search form about `common\models\Zdjecia`.
 */
class SearchZdjecia extends Zdjecia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_albumu'], 'integer'],
            [['opis', 'zdjecie', 'dodano'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Zdjecia::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_albumu' => $this->id_albumu,
            'dodano' => $this->dodano,
        ]);

        $query->andFilterWhere(['like', 'opis', $this->opis])
            ->andFilterWhere(['like', 'zdjecie', $this->zdjecie]);

        return $dataProvider;
    }
}
