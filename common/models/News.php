<?php

namespace common\models;

use Yii;

use yii\helpers\ArrayHelper;
use common\models\PagesAdd;

class News extends \yii\db\ActiveRecord{

	public $file;
	public $cat;
	public $pages_add;

	public static function tableName(){
		return (Yii::$app->language == 'en') ? 'news_en' : 'news';  
	}


	public function rules()
		{
		    return [
		        [['tytul', 'category_id'], 'required'],
		        [['timestamp'], 'safe'],
		        [['tresc', 'table_name'], 'string'],
		        [['czlonek', 'category_id'], 'integer'],
		        [['autor', 'seo_url'], 'string', 'max' => 100],
				[['tags', 'pages_add'], 'safe'],
		        [['tytul'], 'string', 'max' => 300],
		        [['data'], 'string', 'max' => 10],
		        [['zdjecie'], 'string', 'max' => 200],
		        [['seo_title'], 'string', 'max' => 80],
				[['file'], 'image', 'maxSize' => 4144576, 'mimeTypes' => 'image/jpeg, image/pjpeg,  image/gif, image/png',
				'minWidth' => 50, 'maxWidth' => 1624,
				'minHeight' => 50, 'maxHeight' => 1024,]
		    ];
		}

	public function attributeLabels()
		{
		    return [
		        'id' => Yii::t('app', 'ID'),
		        'autor' => Yii::t('app', 'Autor'),
		        'timestamp' => Yii::t('app', 'Time'),
		        'tytul' => Yii::t('app', 'Title'),
		        'tresc' => Yii::t('app', 'Content'),
		        'data' => Yii::t('app', 'Data'),
		        'zdjecie' => Yii::t('app', 'Photo'),
		        'seo_title' => Yii::t('app', 'SEO Title'),
		        'seo_url' => Yii::t('app', 'SEO Url'),
		        'file' => Yii::t('app', 'Photo'),
		        'category_id' => Yii::t('app', 'Category ID'),
		        'tags' => Yii::t('app', 'Tags'),
				'czlonek' => Yii::t('app', 'hidden'),
				'pages_add' => Yii::t('app', 'Powiązane strony'),
		        
		    ];
		}

	public function getTitle(){	
			return $this->tytul;
		}
	
	public function getCategory(){
		    return $this->hasOne(Category::className(), ['id' => 'category_id']);
	}
	
		//	4 			3 			2		1
		// news.id -> news_add :: pages_id ->  id
	
	public function getRelatedPages(){	

		return $this->hasMany(Pages::className(), ['id' => 'pages_id'])->viaTable('pages_add', ['news_add' => 'id']);
		//  $RelatedPages = News::findOne(690)->relatedPages;

	}

	public function getIdRelatedPages($model){	
		if (!$model->isNewRecord) {
			$relatedPages = self::findOne($model->id)->relatedPages;
			return ArrayHelper::getColumn($relatedPages, 'id');
		}
		return null;
	}

}
