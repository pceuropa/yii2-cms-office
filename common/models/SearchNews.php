<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\News;


class SearchNews extends News {

public function rules(){
        return [
            [['id', 'czlonek', 'category_id'], 'integer'],
            [['autor', 'timestamp', 'tytul', 'tresc', 'data', 'zdjecie', 'tags'], 'safe'],
        ];
    }


public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


public function search($params) {
        $query = News::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
			'sort' => [
        		'defaultOrder' => [
            		'data' => SORT_DESC
        		]
   			],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
           	'timestamp' => $this->timestamp,
            'czlonek' => $this->czlonek,
            'category_id' => $this->category_id,
        ]);

        $query
        	->andFilterWhere(['like', 'autor', $this->autor])
            ->andFilterWhere(['like', 'tytul', $this->tytul])
            ->andFilterWhere(['like', 'tresc', $this->tresc])
            ->andFilterWhere(['like', 'data', $this->data])
            ->andFilterWhere(['like', 'zdjecie', $this->zdjecie])
            ->andFilterWhere(['like', 'tags', $this->tags]);

        return $dataProvider;
    }
}
