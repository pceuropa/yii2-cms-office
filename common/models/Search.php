<?php
namespace common\models;

use Yii;

class Search extends \yii\base\Model{

	public $q;
	public $from_date;
	public $to_date;
	public $options = 1;
		
	public function rules(){
		    return [
		      //  [['autor', 'title', 'context', 'date', 'date_expire', 'max', 'seo_title', 'seo_url'], 'required'],
		        [['q'], 'required'],
		        [['q'], 'string', 'min' => 3, 'max' => 20],
		        [['from_date', 'to_date'], 'safe'],
		        [['options'], 'integer', 'min' => 1, 'max' => 2],
		    ];
		}

	public function attributeLabels(){
		    return [
		        'q' => Yii::t('app', 'Search term'),
		        'options' => Yii::t('app', 'Searching in'),
		    ];
		}
    
}

?>
