<?php

namespace common\models;


use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
/**
 * This is the model class for table "strony".
 *
 * @property integer $id
 * @property string $autor
 * @property string $czas
 * @property string $tytul
 * @property string $tresc
 * @property string $data
 * @property string $zdjecie
 * @property string $seo_title
 * @property string $seo_url
 * @property integer $czlonek
 * @property integer $category_id
 */
class Strony extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
	 public $file;
    public static function tableName()
    {
        return 'strony';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tytul', 'category_id'], 'required'],
            [['czas'], 'safe'],
            [['tresc'], 'string'],
            [['czlonek', 'category_id'], 'integer'],
            [['autor', 'seo_title', 'seo_url'], 'string', 'max' => 130],
            [['tytul'], 'string', 'max' => 400],
            [['data'], 'string', 'max' => 10],
            [['zdjecie'], 'string', 'max' => 200],
            [['seo_title'], 'string', 'max' => 75],
			[['file'], 'file', 'extensions' => 'jpg, png',]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'autor' => 'Autor',
            'czas' => 'Czas',
            'tytul' => 'Tytul',
            'tresc' => 'Tresc',
            'data' => 'Data',
            'zdjecie' => 'Zdjecie',
            'seo_title' => 'Tytuł SEO',
            'seo_url' => 'Adres Url',
            'file' => 'Zdjecie',
            'category_id' => 'Kategoria',
        ];
    }
}

