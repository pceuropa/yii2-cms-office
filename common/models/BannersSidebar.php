<?php namespace common\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

class BannersSidebar extends \yii\db\ActiveRecord {

public $files;
const IMG_FOLDER = 'sidebar';

public static function tableName(){
	if (Yii::$app->language == 'en') {
		return 'banners_sidebar_en';
	} else {
		return 'banners_sidebar';}
}
	
	
public function rules(){
        return [
            [['url', 'name'], 'required'],
            [['name'], 'string', 'max' => 255],
            ['files', 'file', 'extensions' => ['png', 'jpg', 'gif'], 'maxSize' => 2024*1024],
            [['url'], 'url']
        ];
    }


public function attributeLabels(){
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name of link'),
            'url' => Yii::t('app', 'Url'),
            'image' => Yii::t('app', 'image'),
            'file' => Yii::t('app', 'File'),
            'serialize' => Yii::t('app', 'Serialize'),
            'timestamp' => Yii::t('app', 'Timestamp'),
        ];
    }
    
public function findAllAsArray(){
    return BannersSidebar::find()->orderBy(['serialize' => SORT_ASC, ])->asArray()->all();
}
    

	
}
