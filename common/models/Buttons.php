<?php

namespace common\models;

use Yii;

class Buttons extends \yii\db\ActiveRecord
{
public $file;

public static function tableName()
    {
	if (Yii::$app->language == 'en') 
	{return 'buttons_en';} else 
	{return 'buttons';}
	 }
	 
	

public function scenarios()
    {
        return [
            'default' => ['url', 'name' , 'file',],
            'serialize' => ['serialize']
        ];
    }
	
	
public function rules()
    {
        return [
            [['url', 'name', 'serialize',], 'required'],
			[['file'], 'file', 'mimeTypes' => 'image/jpeg, image/pjpeg,  image/gif, image/png',],
            [['name', 'image'], 'string', 'max' => 255],
            [['url'], 'url']
        ];
    }


public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name of link'),
            'url' => Yii::t('app', 'Url'),
            'image' => Yii::t('app', 'image'),
            'file' => Yii::t('app', 'File'),
            'serialize' => Yii::t('app', 'Serialize'),
            'timestamp' => Yii::t('app', 'Timestamp'),
        ];
    }
	
}