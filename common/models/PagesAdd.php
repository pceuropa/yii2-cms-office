<?php

namespace common\models;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "pages_add".
 *
 * @property integer $id
 * @property integer $pages_id
 * @property integer $news_add
 * @property integer $pages_add
 */
class PagesAdd extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages_add';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['pages_id', 'news_add', 'pages_add'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pages_id' => 'Pages ID',
            'news_add' => 'News Add',
            'pages_add' => 'Pages Add',
        ];
    }
	
public static function setPagesAddNews($model)
{
	if (!empty($model->news_add)){
		
		foreach ($model->news_add as $value):
			$relation = new PagesAdd;
			$relation->news_add = $value;
			$relation->pages_id = $model->id;
			$relation->save();
		endforeach;
	return true;
	}	
return false;
}

public static function setPagesAddPages($model)
{
	if (!empty($model->pages_add)){
		
		foreach ($model->pages_add as $value):
			$relation = new PagesAdd;
			$relation->pages_add = $value;
			$relation->pages_id = $model->id;
			$relation->save();
		endforeach;
	return true;
	}
return false;	
}

public static function setNewsAddPages($model)
{
	if (!empty($model->pages_add)){
		
		foreach ($model->pages_add as $value):
			$relation = new PagesAdd;
			$relation->news_add = $model->id;
			$relation->pages_id = $value;
			$relation->save();
		endforeach;
	return true;
	}	
return false;
}

public static function getIdPagesRelatedNews($model)
{
	$pages = PagesAdd::find()->select('pages_id')
			->where('news_add = :news_add', ['news_add' => $model->id])
			->all();
	$pagesID = ArrayHelper::getColumn($pages, 'pages_id');
		
	return $pagesID;
}

public static function getIdPagesRelatedPages($model)
{	
	 if (!$model->isNewRecord) {
		$pages = PagesAdd::find()->select('pages_add')
		->where('pages_id = :pages_id and pages_add is not :pages_add', 
			[
			'pages_id' => $model->id,
			'pages_add' => null
			])->asArray()->all();
		
		$pagesID = ArrayHelper::getColumn($pages, 'pages_add');
		
	return $pagesID;	

	}
return null;	
	
}

public static function getIdNewsRelatedPages($model)
{	
	 if (!$model->isNewRecord) {
		
		$newsAdd = PagesAdd::find()->select('news_add')
				->where('pages_id = :pages_id and news_add is not :news_add', [
						'pages_id' => $model->id,
						'news_add' => null
				])->asArray()->all();
				
		$newsID = ArrayHelper::getColumn($newsAdd, 'news_add');
		return $newsID;
			}
			
return null;	
	
}
}
