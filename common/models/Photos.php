<?php

namespace common\models;

use Yii;
use yii\helpers\Html;
/**
 * This is the model class for table "photos".
 *
 * @property integer $id
 * @property integer $id_album
 * @property string $description
 * @property string $photo
 * @property string $timestamp
 */
class Photos extends \yii\db\ActiveRecord
{
public $file;

    public static function tableName()
    {
        
		if (Yii::$app->language == 'en') {return 'photos_en';} else {return 'photos';} 
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_album'], 'integer'],
            [['timestamp'], 'safe'],
            [['description'], 'string', 'max' => 255],

			[['file'], 'image', 'maxSize' => 4144576, 'mimeTypes' => 'image/jpeg, image/pjpeg,  image/gif, image/png',
			'minWidth' => 10, 'maxWidth' => 1624,
			'minHeight' => 10, 'maxHeight' => 1024,
			'maxFiles' => 40],
            [['photo'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_album' => 'Id Album',
            'description' => 'Description',
            'photo' => 'Photo',
            'timestamp' => 'Timestamp',
        ];
    }
	public function getImageUrl()
{
    return Html::img('http://localhost/a-pomorskie-yii/frontend/web/images/galeria/max/' . $this->photo, ['class' => 'img-thumbail', 'height'=> '140px']);
	
}

}
