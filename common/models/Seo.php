<?php
namespace common\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\BaseInflector;
use yii\helpers\StringHelper;

class Seo extends \yii\base\Widget {

public $model;
private $action;

	public function init(){
		parent::init();
		if ($this->model === null) {
			print_r('model is required');die();
		}
		$seo_title = BaseInflector::slug(StringHelper::truncateWords($this->model->tytul, 10), "-");
		
		$this->action = Url::to(['', 'id' => $this->model->id, 'seotitle' => $seo_title], true);
	}

	public function title(){
		Yii::$app->view->title = ($this->model->seo_title != null) ? $this->model->seo_title : $this->model->tytul;
	}

	public function description(){
		Yii::$app->view->registerMetaTag([
			'name' => 'description',
			'content' => Yii::t('app', 'Pomorskie Regional Office in Brussels. '). $this->model->tytul
		]); 
	}
	
	public function canonical(){
		Yii::$app->view->registerLinkTag(['rel' => 'canonical', 'href' => $this->action]);
	}
}
?>
