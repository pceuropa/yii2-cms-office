<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Category;

class CategorySearch extends Category{
    

	public function rules(){
		    return [
		        [['id', 'group'], 'integer'],
		        [['name', 'create'], 'safe'],
		    ];
	}

	public function scenarios(){
	    return Model::scenarios();
	}

	public function search($params){
	    $query = Category::find();

	    $dataProvider = new ActiveDataProvider([ 'query' => $query, ]);

	    if (!($this->load($params) && $this->validate())) { return $dataProvider; }

	    $query->andFilterWhere([
	        'id' => $this->id,
	        'group' => $this->group,
	        'create' => $this->create,
	    ]);

	    $query->andFilterWhere(['like', 'name', $this->name]);

	    return $dataProvider;
	}
}
