<?php
return [

'Home' => 'Strona główna',
'News' => 'Aktualności',
'Add' => 'Dodaj',
'Add new' => 'Dodaj nową',
'Albums' => 'Albumy',
'Bulletin' => 'Biuletyn',
'Pomeranian region in the EU. ' => 'Region Pomorski w Unii. ',
'Pomorskie Regional Office in Brussels. ' => 'Biuro Regionalne Województwa Pomorskiego w Brukseli. ',
'Pomeranian Association in the European Union' => 'Stowarzyszenie Pomorskie w Unii Europejskiej',


'All news' => 'Wszystkie',
'Archives' => 'Archiwum',
'About Us' => 'O nas',
'About Association' => 'O Stowarzyszeniu',
'Authorities of the Association' => 'Władze Stowarzyszenia',
'Members of the Association' => 'Członkowie Stowarzyszenia',
'Statute' => 'Statut',
'Contacts and team' => 'Kontakt i Zespół',
'Contact' => 'Kontakt',
'Join Us' => 'Dołącz do nas',
'Zone login' => 'Strefa Logowania',
'About the Region' => 'O Regionie',
'Pomorskie Region' => 'Województwo Pomorskie',
'Priorities for International Cooperation' => 'Priorytety Współpracy z Zagranicą',
'RPO 2014-2020 for Pomorskie Region' => 'RPO 2014-2020 dla Województwa Pomorskiego',
'Pomeranian Spatial Development Plan' => 'Pomorski Plan Zagospodarowania Przestrzennego',

'Projects and activites' => 'Projekty i działalność',
'All Projects' => 'Wszystkie',
'InnoPomorze' => 'InnoPomorze',
'WFOŚiGW 2014/2015' => 'WFOŚiGW 2014/2015',
'Initiative of the Association' => 'Inicjatywy Stowarzyszenia',
'Office Gdańsk' => 'Biuro Gdańsk',
'Office Brussels' => 'Biuro Bruksela',
'European Funds 2014-2020' => 'Fundusze Europejskie 2014-2020',

'logo for Pomorskie' => 'Logo region Pomorskie',
'Office in Brussels' => 'Biuro w Brukseli',
'Office in Gdańsk' => 'Biuro w Gdańsku',
'logo for Pomerianians in Brussels' => 'Logo stowarzyszenie Pomorskie',
'Association Headquarters' => 'Siedziba Stowarzyszenia',
'VAT' => 'NIP',
"2 floor" => "2 piętro",
"Brussels" => "Bruksela",
"Belgium" => "Belgia",
"room" => "pokój",
'Pomorskie in Brussels' => 'Pomorskie w Brukseli',
'Pomorskie Regional EU Office' => 'Stowarzyszenie „Pomorskie w Unii Europejskiej”',
'Office Pomorskie in Brussels' => 'Pomorskie Biuro w Brukseli',
'Pomerianians in Brussels' => 'Pomorzanie w Brukseli',
'Committee of the Regions' => 'Komitet Regionów',
'EU institutions' => 'Instytucje Unijne',
'Polish Representation in Brussels' => 'Polskie Przedstawicielstwa w Brukseli',

// strony backend
'Pages' => 'Strony',
'Users' => 'Użytkownicy',
'Themes' => 'Szablon',
'All' => 'Wszystkie',
'Browse' => 'Przeglądaj',
'Title' => 'Tytuł',
'Column right' => 'Banery kolumna prawa',
'Banners in sidebar' => 'Zdjęcia w prawej kolumnie',
'Banners in footer' => 'Zdjęcia w stopce',
'Menus' => 'Menu główne',
'Content' => 'Treść',
'Photo' => 'Zdjęcie',
'SEO Title' => 'Tytuł dla wyszukiwarek',
'Category ID' => 'Kategoria',
'Created: ' => 'Data stworzenia: ',
'Updated: ' => 'Ostatnia aktualizacja: ',

'Related news' => 'Powiązane aktualności',
'Related pages' => 'Powiązane strony',
'Pages Add' => 'Powiąż z inną stroną',
'Link to page' => 'Zobacz stronę',

//meta articles
'entity' => 'podmiot',
'The entity issuing the document: ' => 'Podmiot wystawiający dokument :',


// strony frontend
'Read more ...' => 'Czytaj więcej…',
'Tags' => 'Tagi',

// galleries
'Gallery' => 'Galeria',
'Galleries' => 'Galerie',
'photos in album' => 'zdjęć w galerii',

// kategorie
'Categories' => 'Kategorie',
"Select one or two category" => "Wybierz jedną lub dwie kategorie",
'Category' => 'Kategoria',
'Create Category' => 'Nowa kategoria',
'Update Category: ' => 'Aktualizacja kategorii: ',
'Category name' => 'Nazwa kategorii',
'Parent category' => 'Kategoria nadrzędna',
'Date of creation' => 'Data utworzenia',
'Select category' => 'Wybierz kategorie',

// banery
'Buttons' => 'Bannery',
'Name of link' => 'Nazwa linka',
'Url' => 'Adres Url',
'File' => 'Plik',
'Sort' => 'Sortuj',
'Recommended sites:' => 'Polecane strony:',

// FILES
'Files' => 'Pliki',


// Menus
'Create Group' => 'Utwórz grupę',
'Create Link' => 'Utwórz link',
'Segregate' => 'Zapisz ułożenie linków',

// kalendarz
'Event' => 'Wydarzenie',
'Place' => 'Miejsce',
'Place' => 'Miejsce',
'Content Event' => 'Opis wydarzenia',
'hidden' => 'Treść ukryta',
'Calendars' => 'Kalendarium',
'Calendar' => 'Kalendarium',
'No events for this day' => 'Brak wydarzeń w ten dzień',

// Search
'search engine' => 'wyszukiwarka',
'Search term' => 'Szukana fraza',
'Search advanced' => 'Szukanie zawansowane',
'Searching in' => 'Wyszukiwanie w',
'Time range' => 'Zakres czasu',
'articles' => 'artykułach',
'the files to download' => 'dokumentach do pobrania',
'Search' => 'Szukaj',
'Reset search' => 'Reset szukania',
'You can use the' => 'Możesz użyć',

// form
'Step 1: Personal Informations' => 'Krok 1: Dane osobowe Uczestnika',

'First name *' => 'Imię *',
'Last name *' => 'Nazwisko *',
'Birth *' => 'Data urodzenia *',
'Id Number *' => 'Seria i numer dowodu osobistego bądź nr PESEL *',
'Phone *' => 'Telefon *',

'This email address is already registered.' => 'Ten adres e-mail jest już zarejestrowany.',
'Email addresses do not match' => 'Adresy email nie zgadzają się',
'Email *' => 'Email *',
'Email repeat *' => 'Powtórz email',
'Additional email address' => 'Dodatkowy adres email',

'Step 2: Organisation Informations' => 'Krok 2: Dane Organizacji',

'Organisation name  *' => 'Nazwa organizacji *',
'Department  *' => 'Departament *',
'Section  *' => 'Wydział *',
'Address  *' => 'Adres *',
'Country *' => 'Kraj *',
'City *' => 'Miasto *',

// Step 3 form
'Step 3: Your consent' => 'Krok 3: Dane osobowe Uczestnika',
'Step 3: Room reservation' => 'Krok 3: Rezerwacja pokoju',
'Attention' => 'Uwagi',
'Room Person Firstname' => 'Imię osoby towarzyszącej',
'Room Person Lastname' => 'Nazwisko osoby towarzyszącej',

'Step 3 addional information' => 'Pokoje zostaną przydzielone z puli pokoi wstępnie zarezerwowanych przez Stowarzyszenie „Pomorskie w 
Unii Europejskiej” w Hotelu Agenda Louise w terminie  „12 – 15 października (3 noce)” oraz cena pokoi 130,50/noc/pokój jednoosobowy bądź pokój dwuosobowy. Rezerwacja Państwa hotelu 
zostanie potwierdzona osobnym mailem.',

'comments' => 'Jeżeli mają Państwo jakieś dodatkowe uwagi, prośby bądź zapytania, bardzo prosimy o 
umieszczenie ich poniżej: ',
'comments_email3' => 'Formularz przeznaczony jest do rejestracji indywidualnej. Istnieje możliwość przesłania potwierdzenia 
rejestracji na dodatkowy adres mailowy (np. sekretariat, asystent) :',

// respond form
'Thank you for your registration.' => 'Dziękujemy za rejestrację w wydarzeniu.
',
'Registration successfully completed' => 'Rejestracja została zakończona sukcesem',
'message_response_in_email' => 'Znajduje się Pan/Pani na liście zgłoszonych. 
Pracownicy Stowarzyszenia „Pomorskie w Unii Europejskiej” skontaktują się z Panem/Panią  po zakończeniu rejestracji 11 września br. w celu udzielenia dalszych informacji 
organizacyjnych.

Kontakt 
Stowarzyszenie „Pomorskie w Unii Europejskiej”',


// error 404
'The requested page does not exist.' => 'Żądana strona nie istnieje',

// fotter layouts main
'text_footer_layouts_main' => 'Znajdujesz się na stronie Biura Regionalnego Województwa Pomorskiego w Brukseli 
oraz Biura Miejscowego w Gdańsku, które zostały założone przez Stowarzyszenie "Pomorskie w Unii 
Europejskiej".',

'(not set)' => '(brak wartości)',
'An internal server error occurred.' => 'Wystąpił wewnętrzny błąd serwera.',
'Are you sure you want to delete this item?' => 'Czy na pewno usunąć ten element?',
'Delete' => 'Usuń',
'Error' => 'Błąd',
'File upload failed.' => 'Wgrywanie pliku nie powiodło się.',

'Invalid data received for parameter "{param}".' => 'Otrzymano nieprawidłowe dane dla parametru "{param}".',
'Login Required' => 'Wymagane zalogowanie się',
'Missing required arguments: {params}' => 'Brak wymaganych argumentów: {params}',
'Missing required parameters: {params}' => 'Brak wymaganych parametrów: {params}',
'No' => 'Nie',
'No help for unknown command "{command}".' => 'Brak pomocy dla nieznanego polecenia "{command}".',
'No help for unknown sub-command "{command}".' => 'Brak pomocy dla nieznanego pod-polecenia "{command}".',
'No results found.' => 'Brak wyników.',
'Only files with these MIME types are allowed: {mimeTypes}.' => 'Dozwolone są tylko pliki z następującymi typami MIME:  {mimeTypes}.',
'Only files with these extensions are allowed: {extensions}.' => 'Dozwolone są tylko pliki z następującymi rozszerzeniami: {extensions}.',
'Page not found.' => 'Nie odnaleziono strony.',
'Please fix the following errors:' => 'Proszę poprawić następujące błędy:',
'Please upload a file.' => 'Proszę wgrać plik.',
'Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{item} other{items}}.' => 'Wyświetlone <b>{begin, number}-{end, number}</b> z <b>{totalCount, number}</b>.',
'The file "{file}" is not an image.' => 'Plik "{file}" nie jest obrazem.',
'The file "{file}" is too big. Its size cannot exceed {limit, number} {limit, plural, one{byte} other{bytes}}.' => 'Plik "{file}" jest zbyt duży. Jego rozmiar nie może przekraczać {limit, number} {limit, plural, one{bajtu} few{bajtów} many{bajtów} other{bajta}}.',
'The file "{file}" is too small. Its size cannot be smaller than {limit, number} {limit, plural, one{byte} other{bytes}}.' => 'Plik "{file}" jest za mały. Jego rozmiar nie może być mniejszy niż {limit, number} {limit, plural, one{bajtu} few{bajtów} many{bajtów} other{bajta}}.',
'The format of {attribute} is invalid.' => 'Format {attribute} jest nieprawidłowy.',
'The image "{file}" is too large. The height cannot be larger than {limit, number} {limit, plural, one{pixel} other{pixels}}.' => 'Obraz "{file}" jest zbyt duży. Wysokość nie może być większa niż {limit, number} {limit, plural, one{piksela} few{pikseli} many{pikseli} other{piksela}}.',
'The image "{file}" is too large. The width cannot be larger than {limit, number} {limit, plural, one{pixel} other{pixels}}.' => 'Obraz "{file}" jest zbyt duży. Szerokość nie może być większa niż {limit, number} {limit, plural, one{piksela} few{pikseli} many{pikseli} other{piksela}}.',
'The image "{file}" is too small. The height cannot be smaller than {limit, number} {limit, plural, one{pixel} other{pixels}}.' => 'Obraz "{file}" jest za mały. Wysokość nie może być mniejsza niż {limit, number} {limit, plural, one{piksela} few{pikseli} many{pikseli} other{piksela}}.',
'The image "{file}" is too small. The width cannot be smaller than {limit, number} {limit, plural, one{pixel} other{pixels}}.' => 'Obraz "{file}" jest za mały. Szerokość nie może być mniejsza niż {limit, number} {limit, plural, one{piksela} few{pikseli} many{pikseli} other{piksela}}.',
'The requested view "{name}" was not found.' => 'Żądany widok "{name}" nie został odnaleziony.',
'The verification code is incorrect.' => 'Kod weryfikacyjny jest nieprawidłowy.',
'Total <b>{count, number}</b> {count, plural, one{item} other{items}}.' => 'Razem <b>{count, number}</b> {count, plural, one{rekord} few{rekordy} many{rekordów} other{rekordu}}.',
'Unable to verify your data submission.' => 'Nie udało się zweryfikować przesłanych danych.',
'Unknown command "{command}".' => 'Nieznane polecenie "{command}".',
'Unknown option: --{name}' => 'Nieznana opcja: --{name}',
'Update' => 'Aktualizuj',
'Create' => 'Utwórz',
'Content for register users' => 'Treść dla zarejestrowanych',
'View' => 'Zobacz szczegóły',
'Yes' => 'Tak',
'Timestamp' => 'Data',
'Name' => 'Nazwa',
'You are not allowed to perform this action.' => 'Brak upoważnienia do wykonania tej czynności.',
'You can upload at most {limit, number} {limit, plural, one{file} other{files}}.' => 'Możliwe wgranie najwyżej {limit, number} {limit, plural, one{pliku} few{plików} many{plików} other{pliku}}.',
'in {delta, plural, =1{a day} other{# days}}' => 'za {delta, plural, =1{jeden dzień} other{# dni}}',
'in {delta, plural, =1{a minute} other{# minutes}}' => 'za {delta, plural, =1{minutę} few{# minuty} many{# minut} other{# minuty}}',
'in {delta, plural, =1{a month} other{# months}}' => 'za {delta, plural, =1{miesiąc} few{# miesiące} manu{# miesięcy} other{# miesiąca}}',
'in {delta, plural, =1{a second} other{# seconds}}' => 'za {delta, plural, =1{sekundę} few{# sekundy} many{# sekund} other{# sekundy}}',
'in {delta, plural, =1{a year} other{# years}}' => 'za {delta, plural, =1{rok} few{# lata} many{# lat} other{# dni}}',
'in {delta, plural, =1{an hour} other{# hours}}' => 'za {delta, plural, =1{godzinę} few{# godziny} many{# godzin} other{# godziny}}',
'the input value' => 'wartość wejściowa',
'{attribute} "{value}" has already been taken.' => '{attribute} "{value}" jest już w użyciu.',
'{attribute} cannot be blank.' => '{attribute} nie może pozostać bez wartości.',
'{attribute} is invalid.' => '{attribute} zawiera nieprawidłową wartość.',
'{attribute} is not a valid URL.' => '{attribute} nie zawiera prawidłowego adresu URL.',
'{attribute} is not a valid email address.' => '{attribute} nie zawiera prawidłowego adresu email.',
'{attribute} must be "{requiredValue}".' => '{attribute} musi mieć wartość "{requiredValue}".',
'{attribute} must be a number.' => '{attribute} musi być liczbą.',
'{attribute} must be a string.' => '{attribute} musi być tekstem.',
'{attribute} must be an integer.' => '{attribute} musi być liczbą całkowitą.',
'{attribute} must be either "{true}" or "{false}".' => '{attribute} musi mieć wartość "{true}" lub "{false}".',
'{attribute} must be greater than "{compareValue}".' => '{attribute} musi mieć wartość większą od "{compareValue}".',
'{attribute} must be greater than or equal to "{compareValue}".' => '{attribute} musi mieć wartość większą lub równą "{compareValue}".',
'{attribute} must be less than "{compareValue}".' => '{attribute} musi mieć wartość mniejszą od "{compareValue}".',
'{attribute} must be less than or equal to "{compareValue}".' => '{attribute} musi mieć wartość mniejszą lub równą "{compareValue}".',
'{attribute} must be no greater than {max}.' => '{attribute} musi wynosić nie więcej niż {max}.',
'{attribute} must be no less than {min}.' => '{attribute} musi wynosić nie mniej niż {min}.',
'{attribute} must be repeated exactly.' => 'Wartość {attribute} musi być dokładnie powtórzona.',
'{attribute} must not be equal to "{compareValue}".' => '{attribute} musi mieć wartość różną od "{compareValue}".',
'{attribute} should contain at least {min, number} {min, plural, one{character} other{characters}}.' => '{attribute} powinien zawierać co najmniej {min, number} {min, plural, one{znak} few{znaki} many{znaków} other{znaku}}.',
'{attribute} should contain at most {max, number} {max, plural, one{character} other{characters}}.' => '{attribute} powinien zawierać nie więcej niż {max, number} {min, plural, one{znak} few{znaki} many{znaków} other{znaku}}.',
'{attribute} should contain {length, number} {length, plural, one{character} other{characters}}.' => '{attribute} powinien zawierać dokładnie {length, number} {length, plural, one{znak} few{znaki} many{znaków} other{znaku}}.',
'{delta, plural, =1{a day} other{# days}} ago' => '{delta, plural, =1{jeden dzień} other{# dni} other{# dnia}} temu',
'{delta, plural, =1{a minute} other{# minutes}} ago' => '{delta, plural, =1{minutę} few{# minuty} many{# minut} other{# minuty}} temu',
'{delta, plural, =1{a month} other{# months}} ago' => '{delta, plural, =1{miesiąc} few{# miesiące} many{# miesięcy} other{# miesiąca}} temu',
'{delta, plural, =1{a second} other{# seconds}} ago' => '{delta, plural, =1{sekundę} few{# sekundy} many{# sekund} other{# sekundy}} temu',
'{delta, plural, =1{a year} other{# years}} ago' => '{delta, plural, =1{rok} few{# lata} many{# lat} other{# roku}} temu',
'{delta, plural, =1{an hour} other{# hours}} ago' => '{delta, plural, =1{godzinę} few{# godziny} many{# godzin} other{# godziny}} temu',
'{nFormatted} B' => '{nFormatted} B',
'{nFormatted} GB' => '{nFormatted} GB',
'{nFormatted} GiB' => '{nFormatted} GiB',
'{nFormatted} KB' => '{nFormatted} KB',
'{nFormatted} KiB' => '{nFormatted} KiB',
'{nFormatted} MB' => '{nFormatted} MB',
'{nFormatted} MiB' => '{nFormatted} MiB',
'{nFormatted} PB' => '{nFormatted} PB',
'{nFormatted} PiB' => '{nFormatted} PiB',
'{nFormatted} TB' => '{nFormatted} TB',
'{nFormatted} TiB' => '{nFormatted} TiB',
'{nFormatted} {n, plural, =1{byte} other{bytes}}' => '{nFormatted} {n, plural, =1{bajt} few{bajty} many{bajtów} other{bajta}}',
'{nFormatted} {n, plural, =1{gibibyte} other{gibibytes}}' => '{nFormatted} {n, plural, =1{gibibajt} few{gigabajty} many{gibiajtów} other{gibiajta}}',
'{nFormatted} {n, plural, =1{gigabyte} other{gigabytes}}' => '{nFormatted} {n, plural, =1{gigabajt} few{gigabajty} many{gigabajtów} other{gigabaja}}',
'{nFormatted} {n, plural, =1{kibibyte} other{kibibytes}}' => '{nFormatted} {n, plural, =1{kibibajt} few{kibibajty} many{kibibajtów} other{kibibajtów}}',
'{nFormatted} {n, plural, =1{kilobyte} other{kilobytes}}' => '{nFormatted} {n, plural, =1{kilobajt} few{kilobajty} many{kilobajtów} other{kilobajtów}}',
'{nFormatted} {n, plural, =1{mebibyte} other{mebibytes}}' => '{nFormatted} {n, plural, =1{mebibajt} few{mebibajty} many{mebibajtów} other{mebibajta}}',
'{nFormatted} {n, plural, =1{megabyte} other{megabytes}}' => '{nFormatted} {n, plural, =1{megabajt} few{megabajty} many{megabajtów} other{megabajta}}',
'{nFormatted} {n, plural, =1{pebibyte} other{pebibytes}}' => '{nFormatted} {n, plural, =1{pebibajt} few{pebibajty} many{pebibajtów} other{pebibajta}}',
'{nFormatted} {n, plural, =1{petabyte} other{petabytes}}' => '{nFormatted} {n, plural, =1{petabajt} few{petabajty} many{petabajtów} other{petabajta}}',
'{nFormatted} {n, plural, =1{tebibyte} other{tebibytes}}' => '{nFormatted} {n, plural, =1{tebibajt} few{tebibajty} many{tebibajtów} other{tebibajta}}',
'{nFormatted} {n, plural, =1{terabyte} other{terabytes}}' => '{nFormatted} {n, plural, =1{terabajt} few{terabajty} many{terabajtów} other{terabajta}}',
];
