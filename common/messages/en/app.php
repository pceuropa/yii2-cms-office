<?php
/**
* Message translations.
*
* This file is automatically generated by 'yii message' command.
* It contains the localizable messages extracted from source code.
* You may modify this file by translating the extracted messages.
*
* Each array element represents the translation (value) of a message (key).
* If the value is empty, the message is considered as not translated.
* Messages that no longer need translation will have their translations
* enclosed between a pair of '@@' marks.
*
* Message string can be used with plural forms format. Check i18n section
* of the guide for details.
*
* NOTE: this file must be saved in UTF-8 encoding.
*/
return [

// form

'message_response_in_email' => 'If you have any questions, do not hesitate to contact Ms. Ada Guzniczak 
a.guzniczak@pomorskieregion.eu; +32 2 207 13 74',

'comments' => 'Should you have comments, requires or questions, please do not hesitate to note them below:',
'comments_email3' => 'The form is designed to use for the individual registration. We can also confirm your 
registration using your additional e-mail box (CCopy) :',
'adjustment1' => '',
'adjustment2' => '',

'(not set)' => '(not set)',

// fotter layouts main
'text_footer_layouts_main' => 'You are on the website of the Regional Office of the Pomeranian Region in Brussels
and the Local Office in Gdańsk, which are set by the "Union Pomorskie European ".',
];
