<?php
namespace frontend\controllers;

use Yii;
use common\models\Calendar;
use common\models\SearchCalendar;

use yii\helpers\Url;
use yii\web\NotFoundHttpException;

use yii\filters\VerbFilter;


class CalendarController extends \yii\web\Controller {


	public function behaviors(){
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
			],
		];
	}

	public function actionIndex(){
		$searchModel = new SearchCalendar();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

	return $this->render('index', [
		'searchModel' => $searchModel,
		'dataProvider' => $dataProvider,
		]);
	}


	public function actionView($date){
		$model = Calendar::findEventByDate($date);
		if (!$model) {	
		Yii::$app->session->setFlash('warning', Yii::t('app', 'No events for this day'));
		}
		return $this->render('view', [ 'model' => $model, ]);
	}

	public function actionJsonEventCalendar(){
		 
		$model = Calendar::find()->asArray()->all();
		echo '[';
	
		foreach ($model as $event){
			echo '{ 
			"date": "'.$event['date'].' 17:30:00",
			"title": "'.htmlspecialchars($event['place']).' '.htmlspecialchars($event['event']).'", 
			"url": "/events-'.$event["date"].'" },';	
		}

	echo '  { "date": "2015-1-1 23:00:00", "title": "Test Last Year", "description": "Sed u.", "url": "" }]';

	}

	public function actionJsonFullCalendar(){
		 
		$model = Calendar::find()->asArray()->all();
		echo '[';
	
		foreach ($model as $event){
			echo '{ 
			"start": "'.$event['date'].'",
			"title": "'.htmlspecialchars($event['place']).' '.htmlspecialchars($event['event']).'", 
			"url": "'.Url::to(['calendar/view', 'date' => $event["date"] ]) .'"
				},';	
		}

	echo '  { "date": "2015-1-1 23:00:00", "title": "Test Last Year", "description": "Sed u.", "url": "" }]';

	}


	protected function findModel($id){
		if (($model = Calendar::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
