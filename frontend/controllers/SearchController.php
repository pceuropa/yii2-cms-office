<?php
namespace frontend\controllers;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\Search;
use common\models\News;
use common\models\Pages;
use common\models\Files;
use yii\sphinx\Query;
use yii\data\Pagination;
class SearchController extends \yii\web\Controller {

public function actionIndex() {

    	$searchModel = new Search();
    	
    	if ($searchModel->load(Yii::$app->request->queryParams) && $searchModel->validate()){
    	
    		$querySphinx = new Query();
    		if ($searchModel->options == 2){
    		
    			$files_ids = $querySphinx->from(Files::tableName())->select('id')->match($searchModel->q)->all();
    			$files_ids = ArrayHelper::getColumn($files_ids, 'id');
    			
    			$unionQuery = Files::find()->select(['file_id', 'title', 'patch', 'filename', 'extension', 'created'])->where(['file_id' => $files_ids]);
    			
    		} else {
    			
    			$news_ids = $querySphinx->from(News::tableName())->select('id')->match($searchModel->q)->all();
				$pages_ids = $querySphinx->from(Pages::tableName())->select('id')->match($searchModel->q)->all();
				
				$news_ids = ArrayHelper::getColumn($news_ids, 'id');
				$pages_ids = ArrayHelper::getColumn($pages_ids, 'id');
			
				$sql = ['id', 'tytul', 'tresc', 'zdjecie', 'timestamp', 'table_name'];
				$news = News::find()->select($sql)->where(['id' => $news_ids])->andFilterWhere(['between', 'timestamp', $searchModel->from_date, $searchModel->to_date]);
				$pages = Pages::find()->select($sql)->where(['id' => $pages_ids])->andFilterWhere(['between', 'timestamp', $searchModel->from_date, $searchModel->to_date]);
			
				$unionQuery = $news->union($pages);
    			
    		}
			
			$pagination = new Pagination(['defaultPageSize' => 7, 'totalCount' => $unionQuery->count(), ]);
			
			$unionQuery = $unionQuery
						->offset($pagination->offset)
						->limit($pagination->limit)
						->all();
		    if ($searchModel->options == 2){ 
		    	return $this->render('files', [
					'files' => $unionQuery,
					'pagination' => $pagination,
					'searchModel' => $searchModel
				]);
		    
		    } else {
		    	return $this->render('index', [
					'news' => $unionQuery,
					'pagination' => $pagination,
					'searchModel' => $searchModel
				]);
		    }		
				
    	}
    	
    	return $this->render('_form', ['searchModel' => $searchModel]);
    	
    }
    
public function actionAdvanced(){

	$array = \yii\helpers\FileHelper::findFiles('.', ['only'=>['*.pdf','*.doc','*.docx' ,'*.xls' ,'*.xlsx']]);
	sort($array);

	foreach ($array as $key => $value) {

		$path_parts = pathinfo($value);
		$title = str_replace(["-", "_", "(", ")", "[", "]"], " ", \yii\helpers\BaseStringHelper::basename($path_parts['filename']));
		
		$model = new Files();
		$model->title = $title;
		$model->filename = $path_parts['filename'];
		$model->created = "2016-12-31 12:00:00";
		$model->uploaded = "2016-12-31 12:00:00";
		$model->entity = 1;
		$model->extension = $path_parts['extension'];
		$model->patch = str_replace(["./"], "", $path_parts['dirname']); ;
		
	//	if ($model->save()){ echo('ok'); } else { print_r($model->getErrors()); }
		
	}
//echo('<pre>');print_r($array);die();
}


protected function findModel($id) {

	if (($model = SearchSphinx::findOne($id)) !== null) {
		return $model;
	} else {
		throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
	}
}


}
