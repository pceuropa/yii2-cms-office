<?php
namespace frontend\controllers;

use Yii;
use common\models\News;
use common\models\NewsEn;
use common\models\LoginForm;

use yii\data\Pagination;
use yii\web\Request;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;


class SiteController extends \yii\web\Controller {

	public function actions(){

		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function actionIndex() {

		$query = News::find();

		$pagination = new Pagination(['defaultPageSize' => 5,'totalCount' => $query->count(), ]);

		$news = $query->orderBy(['id' => SORT_DESC,])
						->offset($pagination->offset)
						->limit($pagination->limit)
						->all();

		return $this->render('index', [
			'news' => $news,
			'pagination' => $pagination,
		]);

	}

	public function actionNews($id) {

		if (!$model = News::findOne($id)) { 
			throw new BadRequestHttpException('The requested page does not exist.'); 
		}

		return $this->render('news', [ 'model' => $model, ]);
	}

	public function actionLogin() {

		if (!\Yii::$app->user->isGuest) { return $this->goHome(); }

		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {

			return $this->goBack();
		} else 
			{ return $this->render('login', [ 'model' => $model, ]); }
	}

}
