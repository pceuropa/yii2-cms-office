<?php
namespace frontend\controllers;

use Yii;
use common\models\Pages;
use common\models\PagesSearch;
use common\models\News;
use common\models\Category;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class PagesController extends \yii\web\Controller {

	public function behaviors(){
	
		    return [
		        'verbs' => [
		            'class' => VerbFilter::className(),
		            'actions' => [
		                'delete' => ['post'],
		            ],
		        ],

		    ];
		}

	
	public function actionIndex(){

		    $searchModel = new PagesSearch();
		    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		    return $this->render('index', [
		        'searchModel' => $searchModel,
		        'dataProvider' => $dataProvider,
		    ]);
		}


	public function actionView($id) {	
		$model = $this->findModel($id);

		if ($model->czlonek == 1){
			$user = Yii::$app->user->can('admin');
		} else {
			$user = true;
		}

		if ($user) {
			return $this->render('view', [
				'model' => $model,
				'categories' => Category::findAll(explode(',', $model->cat_list)),
				'news' => Pages::getRelatedNewsIfExist($model),
				'pages' => Pages::getRelatedPagesIfExist($model)
			]);
		}
		

	}

	protected function findModel($id){
		    if (($model = Pages::findOne($id)) !== null) {
		        return $model;
		    } else {
		        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
		    }
		}
}
