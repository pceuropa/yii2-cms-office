<?php
namespace frontend\controllers;


use Yii;
use yii\db\Query;
use yii\data\ActiveDataProvider;

use yii\web\Response;
use yii\web\NotFoundHttpException;

use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

use pceuropa\forms\FormBase;
use pceuropa\forms\Form;
use pceuropa\forms\Module;
use pceuropa\forms\models\FormModel;
use pceuropa\forms\models\FormModelSearch;
use pceuropa\email\Send as SendEmail;
use yii\validators\DateValidator;

class FormsController extends \yii\web\Controller{
	
//	public $enableCsrfValidation = false;
	
    public function actionIndex(){
        return $this->render('index');
    }
	
    public function actionView(string $url) {
      $form = FormModel::findModelByUrl($url);
      if ($form->endForm()) { return $this->render('end'); } 
      
        if (($data = Yii::$app->request->post('DynamicModel')) !== null) {

            foreach ($data as $i => $v) {
                if (is_array($data[$i])) $data[$i] = join(',', $data[$i]);
            }

            $query = (new Query)->createCommand()->insert('form_'.$form->form_id, $data);

            if ($query->execute()) {
                $form->updateCounters(['answer' => 1 ]);
                Yii::$app->session->setFlash('success', Yii::t('builder', 'Rejestracja przebiegła pomyślnie'));

                if (isset($data['email']) && isset($form['response']) && $form['response'] != '') {
                    SendEmail::widget([
                                          'from' => 'pomorskie@pomorskieregion.eu',
                                          'to' => $data['email'],
                                          'subject' => Yii::t('builder', 'Rejestracja przebiegła pomyślnie'),
                                          'textBody' => $form['response'],
                                      ]);
                }


            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'An confirmation email was not sent'));
            }

            return $this->redirect(['index']);
        } else {
            return $this->render('view', [ 'form' => $form] );
        }
    }


}
