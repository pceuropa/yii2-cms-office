<?php
namespace frontend\controllers;
use Yii;
use yii\db\Query;
use yii\data\Pagination;
use yii\web\{Controller, NotFoundHttpException};

class GalleryController extends Controller {

public function actionIndex(){

	if (\Yii::$app->language == 'en') {
		$variable_albums = 'albums_en a';
		$variable_photos = 'photos_en p';
	} else {
		$variable_albums = 'albums a';
		$variable_photos = 'photos p';}

	
	
	$query = (new Query())->select('a.id, name, a.timestamp, COUNT(photo) AS iloscphoto, photo')
	->from([$variable_albums, $variable_photos])
	->where('a.id=p.id_album')
	->orderBy(['id' => SORT_DESC, ])
	->groupBy('name');

	$countQuery = clone $query;
	$pages = new Pagination(['defaultPageSize' => 5, 'totalCount' => $countQuery->count()]);
	
		$albums = $query
		->offset($pages->offset)
        ->limit($pages->limit)
        ->all();

	return $this->render('index', ['albums' => $albums,'pages' => $pages,]);
}


public function actionView($id){

	if (\Yii::$app->language == 'en') {
		$variable_albums = 'albums_en a';
		$variable_photos = 'photos_en p';
	} else {
		$variable_albums = 'albums a';
		$variable_photos = 'photos p';
	}
	
	
	$album = (new Query())
		->select('photo, a.name')
		->from([$variable_albums, $variable_photos])
		->where('p.id_album = a.id and a.id=:id', [':id'=> $id])
		->all();
		
	if (!empty($album)) {
            return $this->render('view', ['album' => $album, ]);
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
