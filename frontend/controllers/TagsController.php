<?php
namespace frontend\controllers;

use Yii;
use common\models\News;
use yii\data\Pagination;
use yii\web\Request;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;


class TagsController extends \yii\web\Controller {

public function actions(){

	if (\Yii::$app->request->get('lang') == 'en' || \Yii::$app->request->get('lang') == 'en/') {
		Yii::$app->language = 'en';
		Yii::setAlias('@url_lang', '@web/en');
		Yii::setAlias('@url_img', '@web/en/images');
	} else {
		Yii::setAlias('@url_lang', '@web');
		Yii::setAlias('@url_img', '@web/images');
	}


	return [
		'error' => [
			'class' => 'yii\web\ErrorAction',
		],

	];
}

public function actionIndex($tag){

	$query = News::find();

	$pagination = new Pagination(['defaultPageSize' => 5,'totalCount' => $query->count(), ]);

	$pages = $query->orderBy(['id' => SORT_DESC,])
			->offset($pagination->offset)
			->where("`tags` like '%$tag%'")
			->limit($pagination->limit)
			->all();

	return $this->render('@views/site/index', [
		'news' => $pages,
		'pagination' => $pagination,
	]);

}






}
