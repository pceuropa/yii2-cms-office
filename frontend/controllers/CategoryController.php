<?php
namespace frontend\controllers;

use common\models\News;
use common\models\Category;

use Yii;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class CategoryController extends \yii\web\Controller {

public function actionIndex(){

	$categories = Category::find()->all();
	return $this->render('index', [
		'categories' => $categories,
		]);
}

public function actionView($id) {
	$category = $this->findModel($id);
	$query = News::find()->where('category_id= :id', [':id' => $id ]);
	
	$pagination = new Pagination(['defaultPageSize' => 5,'totalCount' => $query->count(), ]);

	$news = $query->orderBy(['id' => SORT_DESC,])
		->offset($pagination->offset)
		->where('category_id= :id', [':id' => $id ])
		->limit($pagination->limit)
		->all();

	return $this->render('view', [
		'category' => $category,
		'news' => $news,
		'pagination' => $pagination,
	]);

}


protected function findModel($id){
	    if (($model = Category::findOne($id)) !== null) {
	        return $model;
	    } else {
	        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
	    }
	}
}
