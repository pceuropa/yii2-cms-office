<?php
namespace frontend\controllers;

use common\models\FormKomisjiRolnictwa;
use Yii;
use yii\validators\EmailValidator;

class FormKomisjiRolnictwaController extends \yii\web\Controller
{
	
public $enableCsrfValidation = false;
	
	
public function actionIndex()
    {
		
	$validator_email = new EmailValidator();
	$model = new FormKomisjiRolnictwa();

if ($model->load(Yii::$app->request->post()) && $model->save()) {

	$htmlbody = Yii::t('app', 'Thank you for your registration.' ) .
	
	Yii::t('app', 'We would like to confirm that you are on the list of participants.<br />
	If you have questions or need for changes in registration, please contact us 
	pomorskie@pomorskieregion.eu' );

	$body_text = 	Yii::t('app', 'Thank you for your registration.' ) .
'
Dziękujemy za dokonanie rejestracji. Znajduje się Pan/Pani na liście zgłoszonych. Pracownicy Stowarzyszenia „Pomorskie w Unii Europejskiej” skontaktują się z Panem/Panią  po zakończeniu rejestracji 15 lutego br. w celu udzielenia dalszych informacji organizacyjnych.

Kontakt 
Stowarzyszenie „Pomorskie w Unii Europejskiej”
tel. 58 333 41 25
pomorskie@pomorskieregion.eu';
	
	$array_emails[] = $model->email;
	if (!empty($model->email3)) {
		$array_emails[] = $model->email3;
	}
	
	$message = Yii::$app->mailer->compose()
		->setFrom('pomorskie@pomorskieregion.eu')
		->setTo($array_emails)
		->setSubject(Yii::t('app', 'Registration successfully completed'))
		->setTextBody($body_text);
		
	if ($message->send()) {
		Yii::$app->session->setFlash('success', Yii::t('app', 'Registration successfully completed')) ;}
	else {
		Yii::$app->session->setFlash('error', Yii::t('app', 'An confirmation email was not sent')) ;
	}
            return $this->render('view', ['model' => $model]);
        } else {
            return $this->render('index', ['model' => $model]);
        }

    }

}
