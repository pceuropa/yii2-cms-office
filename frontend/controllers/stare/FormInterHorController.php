<?php
namespace frontend\controllers;

use common\models\FormInterHor;
use Yii;
use yii\validators\EmailValidator;

class FormInterHorController extends \yii\web\Controller
{
	
public $enableCsrfValidation = false;
		
public function actionIndex()
    {
		
	$validator_email = new EmailValidator();
	$model = new FormInterHor();

if ($model->load(Yii::$app->request->post()) && $model->save()) {

$body_text = 'Dziękujemy za dokonanie rejestracji na spotkanie „Interaktywnie z Horyzontem” 12  października 2015 roku w Gdańsku.

Stowarzyszenie „Pomorskie w Unii Europejskiej”
Sylwia Skwara
tel. 58 333 43 13, e-mail: innobroker@pomorskieregion.eu';

	$htmlbody = $body_text;
	
	$emails[] = $model->email;
	if (!empty($model->email3)) {
		$emails[] = $model->email3;
	}
	
	$message = Yii::$app->mailer->compose()
		->setFrom('pomorskie@pomorskieregion.eu')
		->setTo($emails)
		->setSubject(Yii::t('app', 'Registration successfully completed'))
		->setTextBody($body_text);
		
	if ($message->send()) {
		Yii::$app->session->setFlash('success', Yii::t('app', 'Registration successfully completed')) ;
	} else {
		Yii::$app->session->setFlash('error', Yii::t('app', 'An confirmation email was not sent')) ;
	}
	
            return $this->render('view', ['model' => $model]);
        } else {
            return $this->render('index', ['model' => $model]);
        }

    }

}
