<?php
namespace frontend\controllers;

use common\models\AnkietaUczelni;
use Yii;
use yii\validators\EmailValidator;

class AnkietaUczelniController extends \yii\web\Controller{
	
	public $enableCsrfValidation = false;
	public function actionIndex(){
		
		$model = new AnkietaUczelni();

		if ($model->load(Yii::$app->request->post())) {

			if ($model->time1) { $model->time1 = join(',',$model->time1);}
			if ($model->time2) { $model->time2 = join(',',$model->time2);}
			if ($model->time3) { $model->time3 = join(',',$model->time3);}
			if ($model->time4) { $model->time4 = join(',',$model->time4);}
			if ($model->time5) { $model->time5 = join(',',$model->time5);}
			if ($model->time6) { $model->time6 = join(',',$model->time6);}

				$model->save();
		
			return $this->render('view', ['model' => $model]);
		} else {
			return $this->render('index', ['model' => $model]);
		}

	}

}
