<?php
namespace frontend\controllers;

use common\models\FormUczelnie;
use Yii;
use yii\validators\EmailValidator;

class UczelnieController extends \yii\web\Controller
{
public $enableCsrfValidation = false;
	
	
public function actionIndex()
    {
		
	$validator_email = new EmailValidator();
	$model = new FormUczelnie();

	if ($model->load(Yii::$app->request->post()) && $model->save()) {

	$body_text = 'Dziękujemy za dokonanie rejestracji na wizytę studyjną dla uczelni i 
jednostek naukowo-badawczych. Pracownicy Stowarzyszenia „Pomorskie w Unii 
Europejskiej” skontaktują się z Panią/Panem po zakończonej rejestracji w dniu 3 
listopada br. w celu przekazania dalszych informacji organizacyjnych.

Kontakt 
Stowarzyszenie „Pomorskie w Unii Europejskiej”
tel. 58 32 68 709, 58 32 68 714
m.matkowska@pomorskie.eu 
a.kielbratowska@pomorskie.eu ';
	
	$htmlbody = $body_text;
	$array_emails[] = $model->email;
	if (!empty($model->email3)) {
		$array_emails[] = $model->email3;
	}
	
	$message = Yii::$app->mailer->compose()
		->setFrom('pomorskie@pomorskieregion.eu')
		->setTo($array_emails)
		->setSubject(Yii::t('app', 'Registration successfully completed'))
		->setTextBody($body_text);
		
	if ($message->send()) {
		Yii::$app->session->setFlash('success', Yii::t('app', 'Registration successfully completed')) ;}
	else {
		Yii::$app->session->setFlash('error', Yii::t('app', 'An confirmation email was not sent')) ;
	}
            return $this->render('view', ['model' => $model]);
        } else {
            return $this->render('index', ['model' => $model]);
        }

    }

}
