<?php

namespace frontend\controllers;
use common\models\FormEnergyWeek2015en;
use Yii;
use yii\validators\EmailValidator;

class FormEnergyWeek2015enController extends \yii\web\Controller
{
public $enableCsrfValidation = false;
	
	
public function actionIndex()
    {
		
	$validator_email = new EmailValidator();
	$model = new FormEnergyWeek2015en();

	if ($model->load(Yii::$app->request->post()) && $model->save()) {

	$htmlbody = '
	Thank you for your registration. 
We would like to confirm that you are on the list of participants.<br />
If you have questions or need for changes in registration, please contact us a.guzniczak@pomorskieregion.eu' ;

$body_text = 'Thank you for your registration. We would like to confirm that you are on the list of participants.
If you have questions or need for changes in registration, please contact us a.guzniczak@pomorskieregion.eu ';

		
		
		$message = Yii::$app->mailer->compose()
		->setFrom('pomorskie@pomorskieregion.eu')
		->setTo($model->email)
		->setSubject('Registration successfully completed')
		->setTextBody($body_text);
	if ($message->send()) {Yii::$app->session->setFlash('success', Yii::t('app', 'The data saved correctly')) ;}
	else {Yii::$app->session->setFlash('error', Yii::t('app', 'An confirmation email was not sent')) ;}
	
            return $this->render('view', ['model' => $model]);
        } else {
            return $this->render('index', ['model' => $model]);
        }

    }

}
