<?php
namespace frontend\controllers;

use common\models\FormOpenDays2015en;
use Yii;
use yii\validators\EmailValidator;

class FormOpenDays2015enController extends \yii\web\Controller
{
	
public $enableCsrfValidation = false;
	
public function actionIndex()
    {
		
	$validator_email = new EmailValidator();
	$model = new FormOpenDays2015en();

	if ($model->load(Yii::$app->request->post()) && $model->save()) {

	$htmlbody = Yii::t('app', 'Thank you for your registration.' ) .
	
	Yii::t('app', 'We would like to confirm that you are on the list of participants.<br />
	If you have questions or need for changes in registration, please contact us 
	a.guzniczak@pomorskieregion.eu' );

	$body_text = Yii::t('app', 'Thank you for your registration.' ) .
	Yii::t('app', 'message_response_in_email' );

		$message = Yii::$app->mailer->compose()
		->setFrom('pomorskie@pomorskieregion.eu')
		->setTo($model->email)
		->setSubject(Yii::t('app', 'Registration successfully completed'))
		->setTextBody($body_text);
		
	if ($message->send()) {
		Yii::$app->session->setFlash('success', Yii::t('app', 'Registration successfully completed')) ;}
	else {
		Yii::$app->session->setFlash('error', Yii::t('app', 'An confirmation email was not sent')) ;
		}
	
            return $this->render('view', ['model' => $model]);
        } else {
            return $this->render('index', ['model' => $model]);
        }

    }

}
