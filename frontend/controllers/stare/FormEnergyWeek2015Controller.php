<?php

namespace frontend\controllers;
use common\models\FormEnergyWeek2015;
use Yii;
use yii\validators\EmailValidator;

class FormEnergyWeek2015Controller extends \yii\web\Controller
{
public $enableCsrfValidation = false;
public function actionIndex()
    {
$validator_email = new EmailValidator();
$model = new FormEnergyWeek2015();

if ($model->load(Yii::$app->request->post()) && $model->save()) {

$htmlbody = "
Bardzo dziękujemy za zainteresowanie udziałem w „Energy Week 2015
Tydzień Zrównoważonej Energii 2015” – Państwa zgłoszenie zostało przyjęte.<br />
<h4>Dane osobowe Uczestnika:</h4>
Imię i Nazwisko :$model->firstname $model->lastname<br />
Data urodzenia : $model->birth<br />
Numer dowodu lub PESEL: $model->id_number<br />
Telefon : $model->phone<br />
Mail : $model->email<br />
Dodatkowy adres e-mail : $model->email3<br />
<h4>Dane Organizacji:</h4>
Nazwa organizacji: $model->name_org<br />
Departament: $model->depart<br />
Wydział: $model->section<br />
Adres organizacji: $model->adress_org<br />
<h4>Rezerwacja pokoju:</h4>
Ilość miejsc zarezerwowanych:  $model->room<br />
Imię osoby towarzyszącej :$model->room_person_firstname $model->room_person_lastname<br />
<h4>Uwagi : </h4>$model->attention<br /><br />
W razie pytań, wątpliwości bądź potrzeby zmian w rejestracji, prosimy o kontakt mailowy m.matkowska@pomorskie.eu";

$body_text = 'Bardzo dziękujemy za zainteresowanie udziałem w „Pomorskich Dniach Energii 2014” – Państwa zgłoszenie zostało przyjęte.
	W razie pytań, wątpliwości bądź potrzeby zmian w rejestracji, prosimy o kontakt mailowy m.matkowska@pomorskie.eu';

	Yii::$app->mailer->compose()
    ->setFrom('pomorskie@pomorskieregion.eu')
    ->setTo($model->email)
    ->setSubject('Rejestracja została zakończona sukcesem')
    ->setTextBody($body_text)
    ->setHtmlBody($htmlbody)->send();

if ($validator_email->validate($model->email3)) {
Yii::$app->mailer->compose()
    ->setFrom('pomorskie@pomorskieregion.eu')
    ->setTo($model->email3)
    ->setSubject('Rejestracja została zakończona sukcesem')
    ->setTextBody($body_text)
    ->setHtmlBody($htmlbody)->send();}
	
	
	
            return $this->render('view', ['model' => $model]);
        } else {
            return $this->render('index', ['model' => $model]);
        }

    }

}
