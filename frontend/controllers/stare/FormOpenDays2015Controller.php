<?php
namespace frontend\controllers;

use common\models\FormOpenDays2015;
use Yii;
use yii\validators\EmailValidator;

class FormOpenDays2015Controller extends \yii\web\Controller
{
	
public $enableCsrfValidation = false;
	
	
public function actionIndex()
    {
		
	$validator_email = new EmailValidator();
	$model = new FormOpenDays2015();

if ($model->load(Yii::$app->request->post()) && $model->save()) {

	$htmlbody = Yii::t('app', 'Thank you for your registration.' ) .
	
	Yii::t('app', 'We would like to confirm that you are on the list of participants.<br />
	If you have questions or need for changes in registration, please contact us 
	a.guzniczak@pomorskieregion.eu' );

	$body_text = 	Yii::t('app', 'Thank you for your registration.' ) .
'
Znajduje się Pan/Pani na liście zgłoszonych. 
Pracownicy Stowarzyszenia „Pomorskie w Unii Europejskiej” skontaktują się z Panem/Panią  
po zakończeniu rejestracji 11 września br. w celu udzielenia dalszych informacji 
organizacyjnych.

Kontakt 
Stowarzyszenie „Pomorskie w Unii Europejskiej”
tel. 58 32 68 709, 58 32 68 714
m.matkowska@pomorskie.eu 
a.kielbratowska@pomorskie.eu';
	
	$array_emails[] = $model->email;
	if (!empty($model->email3)) {
		$array_emails[] = $model->email3;
	}
	
	$message = Yii::$app->mailer->compose()
		->setFrom('pomorskie@pomorskieregion.eu')
		->setTo($array_emails)
		->setSubject(Yii::t('app', 'Registration successfully completed'))
		->setTextBody($body_text);
		
	if ($message->send()) {
		Yii::$app->session->setFlash('success', Yii::t('app', 'Registration successfully completed')) ;}
	else {
		Yii::$app->session->setFlash('error', Yii::t('app', 'An confirmation email was not sent')) ;
	}
            return $this->render('view', ['model' => $model]);
        } else {
            return $this->render('index', ['model' => $model]);
        }

    }

}
