<?php
namespace frontend\controllers;

use yii\web\Controller;
use yii\filters\AccessControl;


class MembersController extends Controller
{
public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'login', 'logout', 'signup'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


public function actionIndex()
{
	

return $this->render('index');



}




}
