<?php
namespace frontend\assets;

class AppAsset extends \yii\web\AssetBundle {
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	
	public $css = [
		'css/site.min.css',
	//	'css/settings.css',
		['css/print.css', 'media' => 'print']
		//'css/layers.css', 
		//'css/navigation.css', 
	];
	
	public $js = [ 
		//'js/moment.js', 
		'js/jquery.eventCalendar.js',
		'js/smoot-div-scroll/jquery-ui-1.10.3.custom.min.js', 	// BannersFooter
		'js/smoot-div-scroll/jquery.mousewheel.min.js', 			// BannersFooter
		'js/smoot-div-scroll/jquery.smoothdivscroll-1.3-min.js',
		'js/slider/jquery.themepunch.tools.min.js',
		'js/slider/jquery.themepunch.revolution.min.js',
		'js/lightbox.min.js',
	];
	
	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
	];
}
