<?php
use yii\helpers\Html;
use yii\helpers\Url;


$this->title = Yii::t('app', 'Calendar');
?>

<h2><?= Html::encode($this->title) ?></h2>

<?= \talma\widgets\FullCalendar::widget([
   
    'loading' => 'Carregando...', // Text for loading alert. Default 'Loading...'
    'config' => [
	'header'=>[
                    'left'=>'prev,next today',
                    'center'=> 'title',
                    'right'=> 'month,agendaWeek,agendaDay'
            ],
	'events' =>  Url::to(['calendar/json-full-calendar']),	
        // put your options and callbacks here
        // see http://arshaw.com/fullcalendar/docs/
        'lang' => 'pl'
    ],
]); ?>

<script>$('#fullcalendar').fullCalendar({
    dayClick: function() {
        alert('a day has been clicked!');
    }
});</script>