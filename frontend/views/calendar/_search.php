<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CalendarNews */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="calendar-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
<div class="raw">
<div class="col-md-6">
    <?= $form->field($model, 'date') ?>
	<?= $form->field($model, 'place') ?>
	</div>
	<div class="col-md-6">
    <?= $form->field($model, 'event') ?>
    <?= $form->field($model, 'content_event') ?>
</div></div>
    

    <?php // echo $form->field($model, 'czlonek') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
 
    </div>

    <?php ActiveForm::end(); ?>

</div>
