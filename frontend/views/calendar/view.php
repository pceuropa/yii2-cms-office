<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\widgets\Url2;
/* @var $this yii\web\View */
/* @var $model common\models\Calendar */

$this->title = 'Pomorskie Events '.Yii::$app->request->get('date');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Calendar'), 'url' => 'calendar' ];
$this->params['breadcrumbs'][] =  Yii::$app->request->get('date');


foreach ($model as $element): ?>
	<?= $this->render('@app/views/calendar/_boxEvent.php', ['element' => $element,]) ?>
<?php endforeach;

?>
