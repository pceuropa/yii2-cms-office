<?php
use yii\helpers\Html;
use yii\helpers\BaseInflector;
use yii\helpers\StringHelper;
use yii\widgets\LinkPager;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'kategoria';



 foreach ($news as $new): ?>

<?php Yii::setAlias('@bar', Yii::getAlias('@url_lang').'/'. BaseInflector::slug(StringHelper::truncateWords("{$new->tytul}", 10), "-") .'-n'. Html::encode("{$new->id}"));?>

<div class="row">

<div class="col-md-4 col-xs-5">
<a href ="<?= Yii::getAlias('@bar') ?>">
<img class="float_left img-responsive img-rounded" src="<?php echo Url::to('@url_img/news/'.$new->zdjecie) ?>" />
</a>
</div>

<div class="col-md-8 col-xs-12">
<a href ="<?= Yii::getAlias('@bar') ?>">
<h4><?= Html::encode("{$new->tytul}") ?></h4>
</a>
<p class="text-right"><small class="date_news"><?= Html::encode("{$new->timestamp}") ?></small></p>
<p><?= StringHelper::truncateWords(Html::decode("{$new->tresc}"), 22, ' <a href="'.Yii::getAlias('@bar').'"><em>Czytaj więcej…</em></a>')   ;  ?></p>
</div>
</div>
<hr>
<?php endforeach; ?>


<?= LinkPager::widget(['pagination' => $pagination]) ?>

