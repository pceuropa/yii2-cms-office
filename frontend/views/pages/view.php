<?php
use common\models\Seo;

$seo = new Seo([
	'model' => $model,
]);
$seo->title();
$seo->description();
$seo->canonical();
?>

<article>
	<h1><?= $model->tytul ?></h1>
	<?= $model->tresc ?>
</article>

<?= $this->render('_meta-article', [ 'model' => $model, 'categories' => $categories]); ?>
<?= $this->render('_related', [ 
	'model' => $model, 
	'pages' => $pages,
	'news' => $news
]); ?>
