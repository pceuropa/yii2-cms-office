<?php
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\BaseInflector;
use yii\helpers\StringHelper;
/* @var $this yii\web\View */
/* @var $searchModel common\models\SearchStrony */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Strony';
$this->params['breadcrumbs'][] = $this->title;
?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
			$a = BaseInflector::slug(StringHelper::truncateWords($model->tytul, 10), "-");
            return Html::a($model->tytul, ['pages/view', 'id' => $model->id, 'seotitle' => $a]);
        }
    ]) ?>