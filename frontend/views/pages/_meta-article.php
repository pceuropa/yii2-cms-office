<?php
	use Yii;
    use yii\helpers\Html;

?><div class="row">


	<blockquote class="col-md-5 small">
	
		<?= Yii::t('app', 'Author: ') . $model->autor  ?><br />
		<?= Yii::t('app', 'The entity issuing the document: ') . $model->entity ?><br />
		<?= Yii::t('app', 'Created: ') . Yii::$app->formatter->asDate($model->data, 'long')  ?><br />
		<?= Yii::t('app', 'Updated: ') . Yii::$app->formatter->asDate($model->timestamp, 'long') ?><br />

		<?php if (!Yii::$app->user->isGuest) {
				$iso_lang = (Yii::$app->language == 'en') ? 'en/' :  '';
				echo Html::a( 'Edytuj news', "/panelx/{$iso_lang}pages/update?id=".$model->id );
		}?> 
	</blockquote>
	<blockquote class="col-md-2">
		<?php $img_print = '<span class="glyphicon glyphicon-print" aria-hidden="true"></span>'; ?>
		<?= Html::a ($img_print , false, ['class' => 'print-preview '] ); ?>
	</blockquote>
</div>


<?php $this->registerJs(" $('a.print-preview').click(function(){window.print()})", 3);?>
