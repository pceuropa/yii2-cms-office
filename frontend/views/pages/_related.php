<?php
    use yii\helpers\Html;
	use yii\helpers\BaseInflector;
	use yii\helpers\StringHelper;
?>

<div class="row">
	<?php if ($news){ ?>

		<div class="list-group col-md-6">
		<a class="list-group-item disabled">
			<span class="glyphicon glyphicon-resize-horizontal" aria-hidden="true"></span> <?= Yii::t('app', 'Related news:');?>
		</a>

		<?php foreach ($news as $v){
			echo Html::a($v->tytul, ['site/news', 'id' => $v->id, 'seotitle' => BaseInflector::slug(StringHelper::truncateWords($v->tytul, 10), "-")], ['class' => 'list-group-item']); 
		} ?>
		</div>
	<?php }

	if ($pages){ ?>

		<div class="list-group col-md-6">
		<a class="list-group-item disabled">
			<span class="glyphicon glyphicon-resize-vertical" aria-hidden="true"></span> <?= Yii::t('app', 'Related pages:');?>
		</a>

		<?php foreach ($pages as $v){
			echo Html::a($v->tytul, ['pages/view', 'id' => $v->id, 'seotitle' => BaseInflector::slug(StringHelper::truncateWords($v->tytul, 10), "-")], ['class' => 'list-group-item']); 
		} ?>
		</div>
	<?php } ?>

</div>
