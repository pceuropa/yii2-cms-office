<?php
use yii\widgets\LinkPager;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Files on Pomeranian Association in the European Union');

$this->registerMetaTag([
	'name' => 'description',
	'content' => Yii::t('app', 'Pomorskie Regional Office in Brussels. ') . ' +32 2 207 13 70'
]);


echo $this->render('_form', ['searchModel' => $searchModel]);

    foreach ($files as $v){ 
	
		$a = DIRECTORY_SEPARATOR . $v->patch . DIRECTORY_SEPARATOR. $v->filename .".". $v->extension;
	
		?>

		<div class="row">
			<div class="col-md-12">
				<h3><?= Html::a($v->title, $a, ['target' => 'new']) ;?></h3>
					<?=  $v->description ?>
					Podmiot który wytworzył informację: Pomorskie| 
					Data uwtworzenia: <?=  $v->created ?>
			
			</div>
		</div>
		<hr>
<?php } ?>

<?= LinkPager::widget(['pagination' => $pagination]) ?>
