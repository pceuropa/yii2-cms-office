<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Search;
use kartik\date\DatePicker;
?>

<?php $form = ActiveForm::begin(['method' => 'get', 'action' => ['index']]); ?>

	<div class="row">
		<div class="col-md-7">
			<?= $form->field($searchModel, 'q')->textInput() ?>
		</div>
		<div class="col-md-5">
			<?php   
				echo '<label>'. Yii::t('app', 'Time range').'</label>';
				echo DatePicker::widget([
					'name' => 'Search[from_date]',
					'value'=> $searchModel->from_date,
					'type' => DatePicker::TYPE_RANGE,
					'name2' => 'Search[to_date]',
					'value2'=> $searchModel->to_date,
					'pluginOptions' => [
						'autoclose'=>true,
						'format' => 'yyyy-mm-dd'
					]
				]); ?>
		</div>
	</div>

<?php
	echo $form->field($searchModel, 'options')->radioList([
		1 => Yii::t('app', 'articles'), 
		2 => Yii::t('app', 'the files to download')
	]);

	echo  Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-success']) ?>
	
<?php ActiveForm::end(); 
$this->registerCss("label { font-weight: normal;}"); ?>
<hr />
