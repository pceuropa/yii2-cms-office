<?php
use yii\widgets\LinkPager;
use yii\helpers\Html;
use yii\helpers\BaseInflector;
use yii\helpers\StringHelper;
use frontend\widgets\Url2;
$this->title = Yii::t('app', 'Gallery');

?>
<h1><?= $this->title ?></h1>

<?php foreach ($albums as $album): ?>

	<div class="row">
		<?php $a = BaseInflector::slug(StringHelper::truncateWords($album['name'], 10), "-");?>

		<div class="col-md-3">
			<?= Html::img(Url2::img('galeria/max').$album['photo'], ['class' => 'img-responsive img-rounded']) ?>
		</div>
	
		<div class="col-md-9">
		<h4><?= Html::a($album['name'], ['gallery/view', 'id' => $album['id'], 'seotitle' => $a]) ?></h4>
			<small>
				<?= Yii::$app->formatter->asDate($album['timestamp'], 'long')  ;?><br /> 
				<?= Yii::t('app', 'photos in album') ?>: <?= $album['iloscphoto'] ;?>
			</small>
		</div>
	</div>
	
<hr>
<?php endforeach;?>
<?= LinkPager::widget(['pagination' => $pages,]);?>
