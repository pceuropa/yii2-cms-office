<?php
use yii\helpers\Html;
use frontend\widgets\Url2;

$this->title = $album[0]['name'] ?? 'Galery';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gallery'), 'url' => 'gallery' ];
$this->params['breadcrumbs'][] =  Yii::t('app', 'Gallery'). ' ' . Yii::$app->request->get('id');
?>

<h1><?= $this->title;?></h1>

<div class="row">
<?php foreach ($album as $photo): ?>
	<div class="col-lg-3 col-md-3 col-xs-6">
		<?php $link = Url2::img('galeria/max').$photo['photo'];?>
		
		<?= Html::a(Html::img($link, ['class' => 'img-responsive img-thumbnail photo']),
			$link, ['data-lightbox' => 'album']
		);?>
	</div>
<?php endforeach;?>
</div>
