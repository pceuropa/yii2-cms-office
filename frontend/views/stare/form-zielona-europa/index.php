<center>
<h1>Finansowanie i dobre praktyki w projektach przyrodniczych i związanych ze zmianami klimatu<br/>
<small>Program LIFE, Regionalny Program Operacyjny Województwa Pomorskiego na lata 2014-2020</small></h1>
<br />
<b>19 maja 2016 r.</b><br/>


sala im. Lecha Bądkowskiego, Urząd Marszałkowski Województwa Pomorskiego<br/>
ul. Okopowa 21/27, 80-810 Gdańsk<br/>


Organizator: Stowarzyszenie „Pomorskie w Unii Europejskiej” w ramach projektu „Zielona Europa-Zróbmy to razem” współfinansowanego ze środków WFOŚiGW w Gdańsku</center>
	
	<?= $this->render('_form.php', [ 'model' => $model, ]) ?>
	
	<hr>
	<p><?= Yii::t('app', 'Contact') ;?> :<br />
	<?= Yii::t('app', 'Pomorskie Regional EU Office') ;?><br />
tel. +48 58 32 68 709<br />
pomorskie@pomorskieregion.eu <br />


