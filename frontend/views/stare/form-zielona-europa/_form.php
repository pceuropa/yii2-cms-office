<?php
use common\models\Countries;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\FormNiebieski */
/* @var $form ActiveForm */
?>
<div class="form-niebieski-wzrost">

    <?php $form = ActiveForm::begin(); ?>


<h3><?= Yii::t('app', 'Step 1: Personal Informations');?></h3>
 
	<div class="row ">
		<div class="col-md-6"><?= $form->field($model, 'firstname' )->label('Imię*') ?></div> 
		<div class="col-md-6"><?= $form->field($model, 'lastname')->label('Nazwisko*') ?></div>
	</div>
	<div class="row">
		<div class="col-md-6"><?= $form->field($model, 'phone')->label('Telefon kontaktowy* ') ?></div>
		<div class="col-md-6"><?= $form->field($model, 'name_org') ?></div>
	</div>
		
	<div class="row">
		<div class="col-md-6"><?= $form->field($model, 'email')->label('E-mail*') ?></div> 
		<div class="col-md-6"><?= $form->field($model, 'email_repeat')->label('Powtórz e-mail*') ?></div>
	</div>
	
<!--  $form->field($model, 'adress_org')->widget(Select2::classname(), [
		 'data' => Countries::getCountryArray(),
		 'options' => ['multiple'=>false],
		 'pluginOptions' => [ 'allowClear' => true, ],
		 ]);
	-->

<?= Yii::t('app', 'comments_email3') ;?>
<?= $form->field($model, 'email3') ?>

<br />
<p>Udział w spotkaniu jest bezpłatny.<br/>
Wydarzenie jest organizowane przez Stowarzyszenie „Pomorskie w Unii Europejskiej” w ramach projektu <?= Html::a ( '„Zielona Europa-Zróbmy to razem”', 'https://pomorskieregion.eu/projekt-zielona-europa-zrobmy-to-razem-p64', ['target' => 'new'] ) ?> współfinansowanego ze środków Wojewódzkiego Funduszu Ochrony Środowiska i Gospodarki Wodnej w Gdańsku.</p>

<div class="row alert alert-success" role="alert">
<div class="col-md-1">
<?= $form->field($model, 'adjustment1',['template' => "{input}{error}"])->checkbox() ?>
</div> <div class="col-md-11">
"Wyrażam zgodę na przetwarzanie moich dobrowolnie podanych danych osobowych zawartych w niniejszym formularzu, zgodnie z ustawą z dnia 29 sierpnia 1997 roku o ochronie danych osobowych (Dz. U. z 2002 r. Nr 101, poz. 926 ze zm.).

</div></div>

<div class="row alert alert-warning" role="alert">
<div class="col-md-1">
<?= $form->field($model, 'adjustment2',['template' => "{input}{error}"])->checkbox() ?></div> <div class="col-md-11">
Jednocześnie oświadczam, iż przyjmuję do wiadomości, że Administratorem tak zebranych danych osobowych jest Stowarzyszenie „Pomorskie w Unii Europejskiej”, ul. Okopowa 21/27, 80-810 Gdańsk. Moje dane osobowe będą przetwarzane wyłącznie w celu rekrutacji na spotkanie. Mam prawo dostępu do treści swoich danych i ich poprawiania”.<br/>
Wypełnienie niniejszego formularza jest jednoznaczne z wyrażeniem zgody na robienie zdjęć podczas spotkania a także zgoda na ich publikację i rozpowszechnianie w celach informacyjno-promocyjnych.

</div></div>

	
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Rejestruj'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- form-niebieski-wzrost -->
