<?php
use common\models\Countries;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\FormNiebieski */
/* @var $form ActiveForm */
?>
<div class="form-niebieski-wzrost">

    <?php $form = ActiveForm::begin(); ?>

<div class="panel panel-default">
<div class="panel-heading"><?= Yii::t('app', 'Step 1: Personal Informations');?></div>
 <div class="panel-body">
 
	<div class="row ">
		<div class="col-md-5"><?= $form->field($model, 'firstname' ) ?></div> 
		<div class="col-md-7"><?= $form->field($model, 'lastname') ?></div>
	</div>
	<?= $form->field($model, 'name_org') ?>
	<?= $form->field($model, 'position') ?>
	
	<div class="row ">
	<div class="col-md-7"><?= $form->field($model, 'phone') ?></div>

		
	</div>
        <div class="row">
		<div class="col-md-6"><?= $form->field($model, 'email') ?></div> 
		<div class="col-md-6"><?= $form->field($model, 'email_repeat') ?></div>
	</div>
        
		

 </div>
</div>

            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
      
    <?php ActiveForm::end(); ?>

</div><!-- form-niebieski-wzrost -->
