<?php
use common\models\Countries;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\FormNiebieski */
/* @var $form ActiveForm */
?>
<div class="form-niebieski-wzrost">

    <?php $form = ActiveForm::begin(); ?>

<div class="panel panel-default">
<div class="panel-heading"><?= Yii::t('app', 'Step 1: Personal Informations');?></div>
 <div class="panel-body">
 
	<div class="row ">
		<div class="col-md-6"><?= $form->field($model, 'firstname' )->label('Imię*') ?></div> 
		<div class="col-md-6"><?= $form->field($model, 'lastname')->label('Nazwisko*') ?></div>
	</div>
	<div class="row">
		<div class="col-md-4"><?= $form->field($model, 'birth')->input('text', ['placeholder' => 'dd.mm.rrrr'])->label('Data urodzenia*') ?></div>
		<div class="col-md-4"><?= $form->field($model, 'id_number' )->input('text', ['placeholder' => 'osobistego lub PESEL'])->label('Numer dowodu*') ?></div>
		<div class="col-md-4"><?= $form->field($model, 'phone')->label('Telefon kontaktowy* ') ?></div>
	</div>
        
        
        
		
	<div class="row">
		<div class="col-md-6"><?= $form->field($model, 'email')->label('E-mail*') ?></div> 
		<div class="col-md-6"><?= $form->field($model, 'email_repeat')->label('Powtórz e-mail*') ?></div>
	</div>
	Numer dowodu osobistego oraz data urodzenie to dane niezbędne w przypadku ubiegania się o wydanie przepustki do Parlamentu Europejskiego i Komisji Europejskiej. Wszystkie podane dane traktowane są jako poufnie i nie będą udostępniane osobom postronnym, ani przetwarzane czy przechowywane po zakończeniu wizyty studyjnej.
 </div>
</div>

<div class="panel panel-default">
	<div class="panel-heading"><?= Yii::t('app', 'Step 2: Organisation Informations');?></div>
	
	<div class="panel-body">
        <?= $form->field($model, 'name_org') ?>

        <?= $form->field($model, 'adress_org') ?>
	</div>
	
<!--  $form->field($model, 'adress_org')->widget(Select2::classname(), [
		 'data' => Countries::getCountryArray(),
		 'options' => ['multiple'=>false],
		 'pluginOptions' => [ 'allowClear' => true, ],
		 ]);
	-->
</div>



<div class="panel panel-default">
<div class="panel-heading"><?= Yii::t('app', 'Step 3: Room reservation');?></div>
<div class="panel-body">
<p>Pokoje zostaną przydzielone z puli pokoi wstępnie zarezerwowanych przez Stowarzyszenie „Pomorskie w Unii Europejskiej” w Hotelu Agenda Louise w terminie 29 lutego — 3 marca (3 noce). Cena pokoi to koszt 110,5 EUR/noc/pokój jednoosobowy bądź 124,5 EUR/noc/pokój dwuosobowy. Rezerwacja Państwa hotelu zostanie potwierdzona osobnym mailem. (formularz do wyboru jednokrotnego)</p>

<?=Html::activeRadioList($model, 'room', [
	0 => 'Zarezerwuję pokój we własnym zakresie',
	1 => 'Rezerwuj pokój jednoosobowy w terminie 29 lutego — 3 marca (3 noce)',
	2 => 'Rezerwuj pokój dwuosobowy w terminie 29 lutego — 3 marca (3 noce), imię i nazwisko osoby towarzyszącej'
	],
	
	[
        'item' => function ($index, $label, $name, $checked, $value) {
		if ($value == 2) 
		{return '<label>' . Html::radio($name, $checked, ['value'  => $value, 'onclick' => "document.getElementById('see_notsee').style.display = this.checked ? 'block' : 'none'"]) . $label . '</label>';}
           else 
		   {return '<p><label>' . Html::radio($name, $checked, ['value'  => $value, 'onclick' => "document.getElementById('see_notsee').style.display = this.checked ? 'none' : 'block'"]) . $label . '</label></p>';}
        }
    ])?>
	
<div class="row" id="see_notsee" style="display: none">
	<div class="col-md-6"><?= $form->field($model, 'room_person_firstname') ?></div> 
	<div class="col-md-6"><?= $form->field($model, 'room_person_lastname') ?></div>
</div>

</div></div>

<div class="panel panel-default">
<div class="panel-heading">UWAGI</div>
 <div class="panel-body">
 <?= Yii::t('app', 'comments') ;?>
 
<?= $form->field($model, 'attention', ['template' => "{input}\n{hint}\n{error}\n<div class='gdzieś_daleko'>{error}</div>"])->textarea(['rows' => 4]) ?>

</div></div>

<?= Yii::t('app', 'comments_email3') ;?>

<?= $form->field($model, 'email3') ?>

<div class="row alert alert-success" role="alert">
<div class="col-md-1">
<?= $form->field($model, 'adjustment1',['template' => "{input}{error}"])->checkbox() ?>
</div> <div class="col-md-11">
Wyrażam zgodę na przetwarzanie danych osobowych przez Stowarzyszenie „Pomorskie w Unii Europejskiej” do celów związanych z organizacją wizyty studyjnej w Brukseli pt. „Wizyta studyjna Komisji Rolnictwa, Gospodarki Żywnościowej i Rozwoju Obszarów Wiejskich” zgodnie z ustawą z dn. 29.08.1997 r. o Ochronie Danych Osobowych (Dz. U. Nr 133 poz. 883).
</div></div>

<div class="row alert alert-warning" role="alert">
<div class="col-md-1">
<?= $form->field($model, 'adjustment2',['template' => "{input}{error}"])->checkbox() ?></div> <div class="col-md-11">
Zobowiązuję się do niezwłocznego informowania pracowników Stowarzyszenia odnośnie jakichkolwiek zmian w planie podróży, które mogą mieć wpływ na ilość noclegów w Brukseli. W przypadku niestawienia się w hotelu w wyznaczonym terminie i nie powiadomienia o tym pracowników Stowarzyszenia bądź bezpośrednio hotelu min. na 24 h przed planowanym pobytem, jestem świadomy/a konieczności pokrycia kosztów pierwszej doby hotelowej.

</div></div>




	
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- form-niebieski-wzrost -->
