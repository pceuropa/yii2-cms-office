<?php
use common\models\Countries;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\FormNiebieski */
/* @var $form ActiveForm */
?>
<div class="form-niebieski-wzrost">

    <?php $form = ActiveForm::begin(); ?>


<div class="panel panel-default">
<div class="panel-heading">Dane osoby wypełniającej ankietę:</div>
<div class="panel-body">

	<div class="row ">
	<div class="col-md-6"><?= $form->field($model, 'firstname' )->label('Imię*') ?></div> 
	<div class="col-md-6"><?= $form->field($model, 'lastname')->label('Nazwisko*') ?></div>
	</div>
	
	<div class="row">
	<div class="col-md-6"><?= $form->field($model, 'name_org') ?></div>
	<div class="col-md-6"><?= $form->field($model, 'phone')->label('Telefon kontaktowy* ') ?></div>
	
	
	
	</div>
	<div class="row">
	<div class="col-md-6"><?= $form->field($model, 'email')->label('E-mail*') ?></div> 
	<div class="col-md-6"><?= $form->field($model, 'email_repeat')->label('Powtórz e-mail*') ?></div>
	</div>
</div>
</div>



<div class="panel panel-default">
<div class="panel-heading">Prosimy o wskazanie preferowanych form, tematyki oraz terminów spotkań w Brukseli:</div>
 <div class="panel-body">
 
<b>Wizyty studyjne</b> (maks. 3-dniowe spotkania z pracownikami instytucji unijnych, organizacji międzynarodowych usytuowanych w Brukseli, dedykowane grupie uczelni i jednostek naukowo-badawczych z Pomorza)
 <br />
 - tematyka:
<?= $form->field($model, 'attention1', ['template' => "{input}\n{hint}\n{error}\n<div class='gdzieś_daleko'>{error}</div>"])->textarea(['rows' => 4]) ?>
- terminy
<?= $form->field($model, 'time1[]', ['template' => "{input}\n{hint}\n{error}\n"])->checkboxList(['I' => 'I kwartał 2017 roku', 'II' => 'II kwartał 2017 roku', 'III' => 'III kwartał 2017 roku', 'IV' => 'IV kwartał 2017 roku']) ?>
<p>
Najbliższa wizyta studyjna została zaplanowana na wiosnę 2017 roku 
i odbędzie się w powiązaniu z przyszłoroczną brukselską edycją University Business Forum. Wśród proponowanych tematów wizyty jest m.in.: Europejska Karta Naukowca. 
 </p>

<b>Bilateralne spotkania eksperckie</b> (spotkania z pracownikami instytucji unijnych, organizacji międzynarodowych usytuowanych w Brukseli, dedykowane wybranej uczelni lub jednostce naukowo-badawczej z Pomorza)
 <br />
 - tematyka:
<?= $form->field($model, 'attention2', ['template' => "{input}\n{hint}\n{error}\n<div class='gdzieś_daleko'>{error}</div>"])->textarea(['rows' => 4]) ?>
- terminy
    
<?= $form->field($model, 'time2[]', ['template' => "{input}\n{hint}\n{error}\n"])->checkboxList(['I' => 'I kwartał 2017 roku', 'II' => 'II kwartał 2017 roku', 'III' => 'III kwartał 2017 roku', 'IV' => 'IV kwartał 2017 roku']) ?>

<b>Spotkania brokerskie</b> (spotkania dla uczelni i/lub jednostek naukowo-badawczych 
z Pomorza poszukujących partnerów do międzynarodowych konsorcjów projektowych)
 <br />
 - tematyka:
<?= $form->field($model, 'attention3', ['template' => "{input}\n{hint}\n{error}\n<div class='gdzieś_daleko'>{error}</div>"])->textarea(['rows' => 4]) ?>
- terminy
    
<?= $form->field($model, 'time3[]', ['template' => "{input}\n{hint}\n{error}\n"])->checkboxList(['I' => 'I kwartał 2017 roku', 'II' => 'II kwartał 2017 roku', 'III' => 'III kwartał 2017 roku', 'IV' => 'IV kwartał 2017 roku']) ?>  
       
 </div></div>

<div class="panel panel-default">
<div class="panel-heading">Prosimy o wskazanie preferowanych form, tematyki oraz terminów spotkań w regionie:<br />
</div>
 <div class="panel-body">
 
<b>Seminaria informacyjne</b> (przedstawienie możliwości finansowania projektów badawczych w wybranym obszarze tematycznym - prezentacja programu oraz naborów wniosków)
 <br />
 - tematyka:
<?= $form->field($model, 'attention4', ['template' => "{input}\n{hint}\n{error}\n<div class='gdzieś_daleko'>{error}</div>"])->textarea(['rows' => 4]) ?>
- terminy
    
<?= $form->field($model, 'time4[]', ['template' => "{input}\n{hint}\n{error}\n"])->checkboxList(['I' => 'I kwartał 2017 roku', 'II' => 'II kwartał 2017 roku', 'III' => 'III kwartał 2017 roku', 'IV' => 'IV kwartał 2017 roku']) ?>
	
  
<b>Szkolenia/warsztaty</b> (praca na praktycznymi elementami, takimi jak: przygotowanie wniosku aplikacyjnego, realizacja i rozliczanie projektu, inne)
 <br />
 - tematyka:
<?= $form->field($model, 'attention5', ['template' => "{input}\n{hint}\n{error}\n<div class='gdzieś_daleko'>{error}</div>"])->textarea(['rows' => 4]) ?>
- terminy
    
<?= $form->field($model, 'time5[]', ['template' => "{input}\n{hint}\n{error}\n"])->checkboxList(['I' => 'I kwartał 2017 roku', 'II' => 'II kwartał 2017 roku', 'III' => 'III kwartał 2017 roku', 'IV' => 'IV kwartał 2017 roku']) ?>


<b>Inne</b>
 <br />
 - tematyka:
<?= $form->field($model, 'attention6', ['template' => "{input}\n{hint}\n{error}\n<div class='gdzieś_daleko'>{error}</div>"])->textarea(['rows' => 4]) ?>
- terminy
    
<?= $form->field($model, 'time6[]', ['template' => "{input}\n{hint}\n{error}\n"])->checkboxList(['I' => 'I kwartał 2017 roku', 'II' => 'II kwartał 2017 roku', 'III' => 'III kwartał 2017 roku', 'IV' => 'IV kwartał 2017 roku']) ?>  
 <div class="alert alert-warning" role="alert">Organizacja powyższych spotkań jest uzależniona od dostępności ekspertów krajowych oraz zagranicznych. </div>   
 </div></div>
 
 
<div class="panel panel-default">
<div class="panel-heading">UWAGI<br />
Jeżeli macie Państwo dodatkowe uwagi, zachęcam do zamieszczenia ich poniżej</div>
 <div class="panel-body">

 
<?= $form->field($model, 'attention', ['template' => "{input}\n{hint}\n{error}\n<div class='gdzieś_daleko'>{error}</div>"])->textarea(['rows' => 4]) ?>

</div></div>

	
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Wyślij'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- form-niebieski-wzrost -->
