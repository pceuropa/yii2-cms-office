<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use yii\captcha\Captcha;

?>
<div class="form-niebieski-wzrost">

    <?php $form = ActiveForm::begin(); ?>

<div class="panel panel-default">
<div class="panel-heading">Krok 1: Dane osobowe Uczestnika</div>
 <div class="panel-body">
 
	<div class="row ">
		<div class="col-md-6"><?= $form->field($model, 'firstname' ) ?></div> 
		<div class="col-md-6"><?= $form->field($model, 'lastname') ?></div>
	</div>
	 <?= $form->field($model, 'name_org') ?>
	 <?= $form->field($model, 'phone') ?>
	<div class="row">
		<div class="col-md-6"><?= $form->field($model, 'email') ?></div> 
		<div class="col-md-6"><?= $form->field($model, 'email_repeat') ?></div>
	</div>

 </div>
</div>


<div class="panel panel-default">
<div class="panel-heading">Questions</div>
 <div class="panel-body">

 <?= Yii::t('app', 'comments') ?>
<?= $form->field($model, 'attention', ['template' => "{input}\n{hint}\n{error}\n<div class='gdzieś_daleko'>{error}</div>"])->textarea(['rows' => 4]) ?>

</div></div>








	
        <div class="form-group">
            <?= Html::submitButton('Rejestruj', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- form-niebieski-wzrost -->
