<?php
use common\models\Countries;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\FormNiebieski */
/* @var $form ActiveForm */
?>
<div class="form-niebieski-wzrost">

    <?php $form = ActiveForm::begin(); ?>

<div class="panel panel-default">
<div class="panel-heading"><?= Yii::t('app', 'Step 1: Personal Informations');?></div>
 <div class="panel-body">
 
	<div class="row ">
		<div class="col-md-5"><?= $form->field($model, 'firstname' ) ?></div> 
		<div class="col-md-7"><?= $form->field($model, 'lastname') ?></div>
	</div>
	
	<div class="row ">
	<div class="col-md-7"><?= $form->field($model, 'phone') ?></div>
		<div class="col-md-5">
		
			<?= $form->field($model, 'birth')->widget(DatePicker::classname(), [
		'options' => ['placeholder' => ''],
		'type' => DatePicker::TYPE_COMPONENT_APPEND,
		'pluginOptions' => [
		'startView' => 2,
		'minViewMode' => 0,
		'language' => 'en',
		'autoclose'=>true,
		'format' => 'yyyy-mm-dd',
		]]); ?>
		
		</div> 
		
	</div>
        
        
		
		
        
		
	<div class="row">
		<div class="col-md-6"><?= $form->field($model, 'email') ?></div> 
		<div class="col-md-6"><?= $form->field($model, 'email_repeat') ?></div>
	</div>

 </div>
</div>


<div class="panel panel-default">
	<div class="panel-heading"><?= Yii::t('app', 'Step 2: Organisation Informations');?></div>
	
	<div class="panel-body">
        <?= $form->field($model, 'name_org') ?>
		<div class="row">
		<div class="col-md-6"><?= $form->field($model, 'depart') ?></div> 
		<div class="col-md-6"><?= $form->field($model, 'section') ?></div>
	</div>
		
		<?= $form->field($model, 'adress_org')->widget(Select2::classname(), [
		'data' => Countries::getCountryArray(),
		'options' => ['multiple'=>false],
		'pluginOptions' => [ 'allowClear' => true, ],
		]);?>
		
		
	</div>

</div>



<div class="row alert alert-success" role="alert">
<div class="col-md-1">
<?= $form->field($model, 'adjustment1',['template' => "{input}{error}"])->checkbox() ?>
</div> <div class="col-md-11">
I hereby consent to the processing of personal data by Pomorskie Regional EU Office for the purpose of organizing an 
“Energy Brunch” in Brussels during Energy Week 2015.
</div></div>




	
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- form-niebieski-wzrost -->
