<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\checkbox\CheckboxX;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
?>

<?php $form = ActiveForm::begin(); ?>

<div class="panel panel-default">
<div class="panel-heading"><?= Yii::t('app', 'Step 1: Personal Informations');?></div>
 <div class="panel-body">
 
 <div class="row">
	<div class="col-md-6"><?= $form->field($model, 'firstname' ) ?></div>
	<div class="col-md-6"><?= $form->field($model, 'lastname') ?></div>
	</div>
  <div class="row ">
	<div class="col-md-6"><?= $form->field($model, 'email') ?></div> 
	<div class="col-md-6"><?= $form->field($model, 'email_repeat') ?></div>
  </div>
  
 </div>
</div>


<div class="panel panel-default">
	<div class="panel-heading"><?= Yii::t('app', 'Step 2: Organisation Informations');?></div>
	
	<div class="panel-body">
	<div class="row">
		<div class="col-md-6"><?= $form->field($model, 'name_org') ?> </div>
		<div class="col-md-6"><?= $form->field($model, 'phone') ?> </div>
	</div>
		<?= $form->field($model, 'adress_org') ?>
	</div>
</div>

<div class="panel panel-default">
<div class="panel-heading">Krok 3: Czy jesteście Państwo członkiem partnerstwa w ramach Inteligentnej Specjalizacji Pomorza?</div>
<div class="panel-body xxx">

<?= $form->field($model, 'room')->inline()->radioList([
	0 => 'Nie',
	1 => 'Tak',
])->label('');?>
<?php

$this->registerJS('
$(document).ready(function(){
	$( ".xxx input[value=0]" ).click(function() {
	  $( "#see_notsee" ).hide("fast");
	});
	$( ".xxx input[value=1]" ).click(function() {
	  $( "#see_notsee" ).show("fast");
	});
});
', 3);?>


<div class="row" id="see_notsee" style="display: none">
	<div class="col-md-12"><?= $form->field($model, 'attention')->label('- jakiej ?') ?></div> 
</div>

</div></div>

<?= Yii::t('app', 'comments_email3') ;?>

<?= $form->field($model, 'email3') ?>

<div class="row alert alert-success" role="alert">
<div class="col-md-1">
<?= $form->field($model, 'adjustment1',['template' => "{input}{error}"])->checkbox() ?>
</div> <div class="col-md-11">
Wyrażam zgodę na przetwarzanie danych osobowych przez Stowarzyszenie „Pomorskie w Unii 
Europejskiej” do celów związanych z organizacją spotkania zgodnie z ustawą z dn. 29.08.1997 r. o 
ochronie danych osobowych (Dz. U. Nr 133 poz. 883).</div></div>


	<div class="form-group">
		<?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
	</div>
<?php ActiveForm::end(); ?>

