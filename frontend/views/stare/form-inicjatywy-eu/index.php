<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
?>
<center><h3>Programy i inicjatywy UE 
na rzecz rozwoju nauki i szkolnictwa wyższego</h3>
4-6 marca 2015 roku<br />
(rejestracja na wydarzenie trwa do 16 lutego 2015 roku)<br />
<a href="<?php echo Url::to('@web/uploads/formularze/Program_wizyta studyjnaUczelnie_4-6.03.2015.pdf') ;?>">Zaproszenie</a>| 
<a href="<?php echo Url::to('@web/uploads/formularze/Zaproszenie_wizyta studyjnaUczelnie_4-6.03.2015.pdf') ;?>">Program</a></center>
<?= $this->render('_form-niebieski-wzrost.php', [ 'model' => $model, ]) ?>
<hr>
<p>Kontakt:<br />
Stowarzyszenie „Pomorskie w Unii Europejskiej”<br />
tel. 58 32 68 709, 58 32 68 714<br />
m.matkowska@pomorskie.eu <br />
k.rembiewska@pomorskieregion.eu </p>