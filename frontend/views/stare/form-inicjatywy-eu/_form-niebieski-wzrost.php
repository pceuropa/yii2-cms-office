<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;

/* @var $this yii\web\View */
/* @var $model common\models\FormNiebieski */
/* @var $form ActiveForm */
?>
<div class="form">

    <?php $form = ActiveForm::begin(); ?>

<div class="panel panel-default">
<div class="panel-heading">Krok 1: Dane osobowe Uczestnika</div>
 <div class="panel-body">
 
	<div class="row ">
		<div class="col-md-6"><?= $form->field($model, 'firstname' )->label('Imię*') ?></div> 
		<div class="col-md-6"><?= $form->field($model, 'lastname')->label('Nazwisko*') ?></div>
	</div>
   
        <?= $form->field($model, 'phone')->label('Telefon kontaktowy* ') ?>
		
	<div class="row">
		<div class="col-md-6"><?= $form->field($model, 'email')->label('E-mail*') ?></div> 
		<div class="col-md-6"><?= $form->field($model, 'email_repeat')->label('Powtórz e-mail*') ?></div>
	</div>
<em>Wszystkie podane dane traktowane są jako poufne i nie będą udostępniane osobom postronnym ani przetwarzane 
i przechowywane po zakończeniu wizyty studyjnej.</em>
 </div>
</div>


<div class="panel panel-default">
	<div class="panel-heading">Krok 2: Dane Organizacji</div>
	
	<div class="panel-body">
        <?= $form->field($model, 'name_org')->label('Nazwa*') ?>
        <?= $form->field($model, 'depart') ?>
        <?= $form->field($model, 'section') ?>
        <?= $form->field($model, 'adress_org')->label('Adres *') ?>
	</div>

</div>


<div class="panel panel-default">
<div class="panel-heading">Krok 3: Rezerwacja pokoju</div><div class="panel-body">
<p>Miejsca zostaną przydzielone z puli pokoi wstępnie zarezerwowanych przez Stowarzyszenie „Pomorskie w Unii Europejskiej” w Hotelu Agenda Louise w 
terminie 3-6 marca (3 noce). Cena pokoi to: 110,5 EUR/noc/pokój jednoosobowy bądź pokój dwuosobowy. Cena obejmuje śniadanie 
i podatek lokalny.</p>

	<?=Html::activeRadioList($model, 'room', [
	0 => 'Zarezerwuję pokój we własnym zakresie',
	1 => 'Rezerwuję pokój jednoosobowy w terminie 3-6 marca (3 noce)',
	2 => 'Rezerwuję pokój dwuosobowy w terminie 3-6 marca (3 noce), imię i nazwisko osoby towarzyszącej'
	],
	
	[
	'template' => '{input}\n{hint}\n{error}',
        'item' => function ($index, $label, $name, $checked, $value) {
		if ($value == 2) 
		{return '<label>' . Html::radio($name, $checked, ['value'  => $value, 'onclick' => "document.getElementById('see_notsee').style.display = this.checked ? 'block' : 'none'"]) . $label . '</label>';}
           else 
		   {return '<label>' . Html::radio($name, $checked, ['value'  => $value, 'onclick' => "document.getElementById('see_notsee').style.display = this.checked ? 'none' : 'block'"]) . $label . '</label>';}
        }
    ])?>
	
<div class="row" id="see_notsee" style="display: none">
	<div class="col-md-6"><?= $form->field($model, 'room_person_firstname') ?></div> 
	<div class="col-md-6"><?= $form->field($model, 'room_person_lastname') ?></div>
</div>

</div></div>

<div class="panel panel-default">
<div class="panel-heading">Krok 4: Udział we wspólnej kolacji </div><div class="panel-body">
<p>Zapraszamy do wspólnego wyjścia w środę 4 marca na kolację z tradycyjną belgijską kuchnią (koszt 25-40 EUR).</p>

<?= $form->field($model, 'dinner', ['template' => "{input}\n{hint}\n{error}"])->radioList(['1'=>'Tak',0=>'Nie']); ?>

</div></div>


Na University-Business Forum, odbywające się w dniach 5-6 marca br., obowiązuję osobna procedura rejestracyjna na stronie organizatora wydarzenia.
<div class="panel panel-default">
<div class="panel-heading">Uwagi</div>
 <div class="panel-body">
Jeżeli mają Państwo jakieś dodatkowe uwagi, prośby bądź zapytania, bardzo prosimy o umieszczenie ich poniżej: 
<?= $form->field($model, 'attention', ['template' => "{input}\n{hint}\n{error}"])->textarea(['rows' => 4]) ?>

</div></div>

	
Formularz przeznaczony jest do rejestracji indywidualnej. Istnieje możliwość przesłania potwierdzenia rejestracji na 
<?= $form->field($model, 'email3') ?>

<div class="row alert alert-success" role="alert">
<div class="col-md-1">
<?= $form->field($model, 'adjustment1',['template' => "{input}{error}"])->checkbox() ?>
</div> <div class="col-md-11">
Wyrażam zgodę na przetwarzanie danych osobowych przez Stowarzyszenie „Pomorskie w Unii Europejskiej” do celów związanych z 
organizacją wizyty studyjnej zgodnie z ustawą z dn. 29.08.1997 r. o Ochronie Danych Osobowych (Dz. U. Nr 133 poz. 883).
</div></div>

<div class="row alert alert-warning" role="alert">
<div class="col-md-1">
<?= $form->field($model, 'adjustment2',['template' => "{input}{error}"])->checkbox() ?></div> <div class="col-md-11">
Zobowiązuję się do niezwłocznego informowania pracowników Stowarzyszenia odnośnie jakichkolwiek zmian w planie podróży, 
które mogą mieć wpływ na ilość noclegów w Brukseli. W przypadku niestawienia się w hotelu w wyznaczonym terminie i 
niepowiadomienia o tym pracowników Stowarzyszenia bądź bezpośrednio hotelu min. na 24 h przed planowanym pobytem, jestem 
świadomy/a konieczności pokrycia kosztów pierwszej doby hotelowej.
</div></div>


	
        <div class="form-group">
            <?= Html::submitButton('Rejestruj', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- form-niebieski-wzrost -->
