<?php
/* @var $this yii\web\View */
?>
<center><h3>Niebieski wzrost w Regionie Morza Bałtyckiego</h3>
24-26 lutego 2015 r. <br />
(rejestracja na wydarzenie trwa do 31 stycznia 2015 r.)<br />
<a href="http://pomorskieregion.eu/uploads/formularze/zapro_wizytaStudyjnaBG_24-26_02_2015.pdf">Zaproszenie</a>| <a href="http://pomorskieregion.eu/uploads/formularze/prog_wizytyStudyjnej_niebieski_wzrost_24-26.02.2015.pdf">Program</a></center>
<?= $this->render('_form-niebieski-wzrost.php', [ 'model' => $model, ]) ?>
<hr>
<p>Kontakt:<br />
Stowarzyszenie „Pomorskie w Unii Europejskiej”<br />
tel. 58 32 68 709, 58 32 68 714<br />
m.matkowska@pomorskie.eu <br />
k.rembiewska@pomorskieregion.eu </p>