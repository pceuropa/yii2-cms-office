<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\checkbox\CheckboxX;
use yii\captcha\Captcha;
/* @var $this yii\web\View */
/* @var $model common\models\FormNiebieski */
/* @var $form ActiveForm */
?>
<div class="form-niebieski-wzrost">

    <?php $form = ActiveForm::begin(); ?>

<div class="panel panel-default">
<div class="panel-heading">Krok 1: Dane osobowe Uczestnika</div>
 <div class="panel-body">
 
	<div class="row ">
		<div class="col-md-6"><?= $form->field($model, 'firstname' )->label('Imię*') ?></div> 
		<div class="col-md-6"><?= $form->field($model, 'lastname')->label('Nazwisko*') ?></div>
	</div>
        <?= $form->field($model, 'birth')->input('text', ['placeholder' => 'dd.mm.rrrr'])->label('Data urodzenia*') ?>
        <?= $form->field($model, 'id_number' )->input('text', ['placeholder' => 'osobistego lub PESEL'])->label('Numer dowodu*') ?>
        <?= $form->field($model, 'phone')->label('Telefon kontaktowy* ') ?>
		
	<div class="row">
		<div class="col-md-6"><?= $form->field($model, 'email')->label('E-mail*') ?></div> 
		<div class="col-md-6"><?= $form->field($model, 'email_repeat')->label('Powtórz e-mail*') ?></div>
	</div>
<em>Numer dowodu osobistego/PESEL oraz data urodzenia to dane niezbędne do rejestracji grupy na wizytę w porcie 
w Antwerpii. Wszystkie podane dane traktowane są jako poufnie i nie będą udostępniane osobom postronnym ani przetwarzane 
czy przechowywane po zakończeniu wizyty studyjnej.</em>
 </div>
</div>


<div class="panel panel-default">
	<div class="panel-heading">Krok 2: Dane Organizacji</div>
	
	<div class="panel-body">
        <?= $form->field($model, 'name_org')->label('Nazwa*') ?>
        <?= $form->field($model, 'depart') ?>
        <?= $form->field($model, 'section') ?>
        <?= $form->field($model, 'adress_org')->label('Adres *') ?>
	</div>

</div>


<div class="panel panel-default">
<div class="panel-heading">Krok 3: Rezerwacja pokoju</div><div class="panel-body">
<p>Miejsca zostaną przydzielone z puli pokoi wstępnie zarezerwowanych przez Stowarzyszenie „Pomorskie w Unii Europejskiej” w Hotelu Agenda Louise w 
terminie 24-26 lutego  (2 noce). Cena pokoi to: 110,5 EUR/noc/pokój jednoosobowy bądź pokój dwuosobowy. Cena obejmuje śniadanie i podatek lokalny.</p>
	<?=Html::activeRadioList($model, 'room', [
	0 => 'Zarezerwuję pokój we własnym zakresie',
	1 => 'Rezerwuj pokój jednoosobowy w terminie 24-26 lutego (2 noce)',
	2 => 'Rezerwuj pokój dwuosobowy w terminie 24-26 lutego (2 noce), imię i nazwisko osoby towarzyszącej'
	],
	
	[
        'item' => function ($index, $label, $name, $checked, $value) {
		if ($value == 2) 
		{return '<label>' . Html::radio($name, $checked, ['value'  => $value, 'onclick' => "document.getElementById('see_notsee').style.display = this.checked ? 'block' : 'none'"]) . $label . '</label>';}
           else 
		   {return '<label>' . Html::radio($name, $checked, ['value'  => $value, 'onclick' => "document.getElementById('see_notsee').style.display = this.checked ? 'none' : 'block'"]) . $label . '</label>';}
        }
    ])?>
	
<div class="row" id="see_notsee" style="display: none">
	<div class="col-md-6"><?= $form->field($model, 'room_person_firstname') ?></div> 
	<div class="col-md-6"><?= $form->field($model, 'room_person_lastname') ?></div>
</div>

</div></div>

<div class="panel panel-default">
<div class="panel-heading">Obszary Zainteresowań - wizyta w porcie w Antwerpii/Uwagi</div>
 <div class="panel-body">
W związku z planowaną wizytą w porcie w Antwerpii zachęcamy do wpisywania w polu poniżej obszarów zainteresowań bądź 
szczegółowych pytań, na które chcieliby Państwo otrzymać odpowiedź podczas wizyty w porcie: 
<?= $form->field($model, 'attention', ['template' => "{input}\n{hint}\n{error}\n<div class='gdzieś_daleko'>{error}</div>"])->textarea(['rows' => 4]) ?>

</div></div>

	
Formularz przeznaczony jest do rejestracji indywidualnej. Istnieje możliwość przesłania potwierdzenia rejestracji na 
<?= $form->field($model, 'email3') ?>

<div class="row alert alert-success" role="alert">
<div class="col-md-1">
<?= $form->field($model, 'adjustment1',['template' => "{input}{error}"])->checkbox() ?>
</div> <div class="col-md-11">
Wyrażam zgodę na przetwarzanie danych osobowych przez Stowarzyszenie „Pomorskie w Unii Europejskiej” do celów związanych z 
organizacją wizyty studyjnej zgodnie z ustawą z dn. 29.08.1997 r. o Ochronie Danych Osobowych (Dz. U. Nr 133 poz. 883).
</div></div>

<div class="row alert alert-warning" role="alert">
<div class="col-md-1">
<?= $form->field($model, 'adjustment2',['template' => "{input}{error}"])->checkbox() ?></div> <div class="col-md-11">
Zobowiązuję się do niezwłocznego informowania pracowników Stowarzyszenia odnośnie jakichkolwiek zmian w planie podróży, 
które mogą mieć wpływ na ilość noclegów w Brukseli. W przypadku niestawienia się w hotelu w wyznaczonym terminie i 
niepowiadomienia o tym pracowników Stowarzyszenia bądź bezpośrednio hotelu min. na 24 h przed planowanym pobytem, jestem 
świadomy/a konieczności pokrycia kosztów pierwszej doby hotelowej.
</div></div>


	
        <div class="form-group">
            <?= Html::submitButton('Rejestruj', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- form-niebieski-wzrost -->
