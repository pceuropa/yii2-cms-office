<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\checkbox\CheckboxX;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\FormNiebieski */
/* @var $form ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="panel panel-default">
<div class="panel-heading"><?= Yii::t('app', 'Step 1: Personal Informations');?></div>
 <div class="panel-body">
 
  <div class="row ">
	<div class="col-md-4"><?= $form->field($model, 'firstname' ) ?></div> 
	<div class="col-md-4"><?= $form->field($model, 'lastname') ?></div>
	<div class="col-md-4"><?= $form->field($model, 'phone') ?></div>
	</div>
	 <div class="row ">
	<div class="col-md-6"><?= $form->field($model, 'email') ?></div> 
	<div class="col-md-6"><?= $form->field($model, 'email_repeat') ?></div>
  </div>
 </div>
</div>


<div class="panel panel-default">
	<div class="panel-heading"><?= Yii::t('app', 'Step 2: Organisation Informations');?></div>
	
	<div class="panel-body">
        <?= $form->field($model, 'name_org') ?>
    <div class="row">
		<div class="col-md-6"><?= $form->field($model, 'depart')->label('Departament') ?></div> 
		<div class="col-md-6"><?= $form->field($model, 'section')->label('Wydział') ?></div>
	</div>
        <?= $form->field($model, 'adress_org') ?>
	</div>
	
<!--  $form->field($model, 'adress_org')->widget(Select2::classname(), [
		 'data' => Countries::getCountryArray(),
		 'options' => ['multiple'=>false],
		 'pluginOptions' => [ 'allowClear' => true, ],
		 ]);
	-->
</div>



<div class="panel panel-default">
<div class="panel-heading"><?= Yii::t('app', 'Step 3: Room reservation');?></div>
<div class="panel-body xxx">
<p>Miejsca zostaną przydzielone z puli pokoi wstępnie zarezerwowanych przez Stowarzyszenie 
„Pomorskie w Unii Europejskiej” w Hotelu Agenda Louise w terminie 18-20 listopada 2015 
roku (2 noce). Cena pokoju to: 110,5 EUR/noc/pokój jednoosobowy lub 120,50 EUR/noc/pokój 
dwuosobowy. Cena obejmuje śniadanie oraz podatek lokalny.</p>

<?= $form->field($model, 'room')->radioList([
	0 => 'Zarezerwuję pokój we własnym zakresie',
	1 => 'Rezerwuję pokój jednoosobowy w terminie 18-20 listopada (2 noce)',
	2 => 'Rezerwuję pokój dwuosobowy w terminie 18-20 listopada (2 noce), imię i nazwisko osoby towarzyszącej',
])->label(false);

$this->registerJS('
$(document).ready(function(){
	$( ".xxx input[value=0], .xxx input[value=1]" ).click(function() {
	  $( "#see_notsee" ).hide("fast");
	});
	$( ".xxx input[value=2]" ).click(function() {
	  $( "#see_notsee" ).show("fast");
	});
});
', 3);?>
	
<div class="row" id="see_notsee" style="display: none">
	<div class="col-md-6"><?= $form->field($model, 'room_person_firstname') ?></div> 
	<div class="col-md-6"><?= $form->field($model, 'room_person_lastname') ?></div>
</div>

</div></div>

<div class="panel panel-default">
<div class="panel-heading">Krok 4: Udział w dodatkowym szkoleniu w dniu 18 listopada 2015 roku</div>
 <div class="panel-body">
Czy wyraża Pan/Pani chęć udziału w szkoleniu nt. przedkomercyjnych zamówień publicznych w Horyzoncie 2020 w dniu 18 listopada 2015 roku w godzinach od 12.00 do 15.00?
 
<?= $form->field($model, 'training')->radioList([
	0 => 'Nie',
	1 => 'Tak'
	])->label(false);
?>
</div></div>



<div class="panel panel-default">
<div class="panel-heading">UWAGI</div>
 <div class="panel-body">
 <h5>WAŻNE</h5>
Na wizytę na BRUSSELS INNOVA w dniu 19 listopada 2015 roku konieczna jest wcześniejsza 
indywidualna rejestracja na <a target="new" href="https://www.onetelecomticket.com/iframe.php?event=6RAL0T" >stronie organizatora wydarzenia</a> (bezpłatny bilet wstępu): 
 <br /><br />
 <p><?= Yii::t('app', 'comments') ;?></p>
 
<?= $form->field($model, 'attention', ['template' => "{input}\n{hint}\n{error}\n<div class='gdzieś_daleko'>{error}</div>"])->textarea(['rows' => 4]) ?>

</div></div>

<?= Yii::t('app', 'comments_email3') ;?>

<?= $form->field($model, 'email3') ?>

<div class="row alert alert-success" role="alert">
<div class="col-md-1">
<?= $form->field($model, 'adjustment1',['template' => "{input}{error}"])->checkbox() ?>
</div> <div class="col-md-11">
Wyrażam zgodę na przetwarzanie danych osobowych przez Stowarzyszenie „Pomorskie w Unii 
Europejskiej” do celów związanych z organizacją wizyty studyjnej zgodnie z ustawą z dn. 
29.08.1997 r. o Ochronie Danych Osobowych (Dz. U. Nr 133 poz. 883).
</div></div>

<div class="row alert alert-warning" role="alert">
<div class="col-md-1">
<?= $form->field($model, 'adjustment2',['template' => "{input}{error}"])->checkbox() ?></div> <div class="col-md-11">
Zobowiązuję się do niezwłocznego informowania pracowników Stowarzyszenia odnośnie 
jakichkolwiek zmian w planie podróży, które mogą mieć wpływ na ilość noclegów w Brukseli. 
W przypadku niestawienia się w hotelu w wyznaczonym terminie i niepowiadomienia o tym 
pracowników Stowarzyszenia bądź bezpośrednio hotelu na min. 24 h przed planowanym pobytem, 
jestem świadomy/a konieczności pokrycia kosztów pierwszej doby hotelowej.
</div></div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Zapisz mnie'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

