<?php
$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['category/index']]; 
$this->params['breadcrumbs'][] = $category->name;
?>

<h1><?= Yii::t('app', 'Category')?>: <?= $category->name ?></h1>
<?= $this->render('@views/site/_new.php', [ 'news' => $news,]) ?>
<?= \yii\widgets\LinkPager::widget(['pagination' => $pagination]) ?>
