<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\helpers\BaseInflector;
use yii\helpers\StringHelper;
/* @var $this yii\web\View */
/* @var $searchModel common\models\SearchCategory */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kategorie';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
<div class="col-md-4">
<ul>
<?php foreach ($categories as $val):
$a= BaseInflector::slug(StringHelper::truncateWords($val->name, 5), "_");
 ?>


<li><?= Html::a($val->name, ['category/view', 'id' => $val->id, 'seotitle' => $a]) ?></li>

<?php endforeach; ?>
</ul>

</div>


</div>

