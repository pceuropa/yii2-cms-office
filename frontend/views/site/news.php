<?php
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\Url2;
use yii\helpers\BaseInflector;
use yii\helpers\StringHelper;
use common\models\Seo;

$seo = new Seo([
	'model' => $model,
]);
$seo->title();
$seo->description();
$seo->canonical();

?>

<article>
	<h1><?= $model->tytul ?></h1>
	
	<?php if (!empty($model->zdjecie)) { 
		echo Html::img(Url2::img('news').$model->zdjecie, ['alt' => StringHelper::truncateWords($model->tytul, 3), 'class' => 'float_left img-thumbnail col-md-4']);
	}?>
	
	<?= $model->tresc ?>
</article>
<?= $this->render('_meta-article', [ 'model' => $model]); ?>


