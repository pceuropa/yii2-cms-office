<?php 
use yii\helpers\Html;
use yii\helpers\BaseInflector;
use yii\helpers\StringHelper;
use frontend\widgets\Url2;

foreach ($news as $val){ 

	$action = ($val->table_name === 'news') ? 'site/news' : 'pages/view';
	
	$a = [$action, 'id' => $val->id, 'seotitle' => BaseInflector::slug(StringHelper::truncateWords($val->tytul, 10), "-")];
	
	?>

	<div class="row">
	
		<div class="col-md-4 col-xs-5">
			<?php $img = Html::img( Url2::img('news') . $val->zdjecie, ['class' => 'img-thumbnail']); ?>
			<?= $val->zdjecie ? Html::a($img, $a) : '';?>
		</div>

		<div class="col-md-8 col-xs-12">
			<h4><?= Html::a($val->tytul, $a) ;?></h4>

			<p>
				<small class="date_news"><?= Html::encode($val->timestamp) ?></small>
			</p>
			<p>
				<?= StringHelper::truncateWords(Html::decode(strip_tags($val->tresc)), 22, ' <small>'.Html::a(Yii::t('app', 'Read more ...'), $a) )   ;  ?></small>
			</p>
		</div>
		
	</div>
	<hr>
<?php } ?>
