<?php
use yii\widgets\LinkPager;
$this->title = Yii::t('app', 'Pomeranian Association in the European Union');

$this->registerMetaTag([
	'name' => 'description',
	'content' => Yii::t('app', 'Pomorskie Regional Office in Brussels. ') . ' +32 2 207 13 70'
]);?>


<?= $this->render('_new', [ 'news' => $news,]) ?>
<?= LinkPager::widget(['pagination' => $pagination]) ?>
