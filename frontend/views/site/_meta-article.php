<?php

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\BaseInflector;
use yii\helpers\StringHelper;

$a = BaseInflector::slug(StringHelper::truncateWords($model->tytul, 10), "-");
$b = BaseInflector::slug(StringHelper::truncateWords($model->category['name'], 10), "-");
?>
<div class="row page-header">
	<blockquote class="col-md-5 small text-left">
		<?= Yii::t('app', 'Category'). ': ' ?>
		<?= Html::a($model->category['name'], ['category/view', 'id' => $model->category_id, 'seotitle' => $b] ).', ';?>
		<br />
	 
		<?php echo Yii::t('app', 'Tags').': ' ;
		foreach (explode(',', $model->tags) as $val):
			echo Html::a($val, Url::to(['tags/index', 'tag' => $val ]) ).', ';
		endforeach;
		?>
	</blockquote>

	<blockquote class="col-md-5 small">
	
		<?= Yii::t('app', 'Author: ') . $model->autor ?><br />
		<?= Yii::t('app', 'The entity issuing the document: ') . $model->entity ?><br />
		<?= Yii::t('app', 'Created: ') . Yii::$app->formatter->asDate($model->data, 'long');  ?><br />
		<?= Yii::t('app', 'Updated: ') . Yii::$app->formatter->asDate($model->timestamp, 'long'); ?><br />

		<?php if (!Yii::$app->user->isGuest) {
				$iso_lang = (Yii::$app->language == 'en') ? 'en/' :  '';
				echo Html::a( 'Edytuj news', "/panelx/{$iso_lang}news/update?id=".$model->id );
		}?> 
		
	</blockquote>
	<blockquote class="col-md-2">
		<?php $img_print = '<span class="glyphicon glyphicon-print" aria-hidden="true"></span>'; ?>
		<?= Html::a ($img_print , false, ['class' => 'print-preview '] ); ?>
	</blockquote>
</div>

<?php $this->registerJs(" $('a.print-preview').click(function(){window.print()})", 3);?>
