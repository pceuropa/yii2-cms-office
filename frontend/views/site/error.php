<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = $name;
?>


    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>
    
	
<?= Html::a ( Yii::t('app', 'search engine'), ['search/index']); ?>
	
