<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use pceuropa\menu\Menu;
use yii\helpers\Url;
use frontend\widgets\Url2;

$id = 1;
if (Yii::$app->language === 'en'){
	$id = 2;
}
NavBar::begin([ 'brandLabel' => '<span class="glyphicon glyphicon-home" aria-hidden="true"></span>',
            	'brandUrl' => Url::home(),
            ]);
            
echo Nav::widget([ 'options' => ['class' => 'navbar-nav navbar-left'], 'items' => Menu::NavbarLeft($id) ]);
echo Nav::widget([ 'options' => ['class' => 'navbar-nav navbar-right'], 'items' => Menu::NavbarRight($id)]);		

NavBar::end();			
?>

