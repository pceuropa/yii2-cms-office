<?php
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\Banners;  
use frontend\widgets\Alert;
use frontend\widgets\Url2;
?>
<footer id="footer">
<div class="container">
	
	<div class="row">
		
		<div class="col-md-3">
			<?= Html::img(Url2::img().'/logo/logo2.png', ['class' => 'img-responsive', "alt" => "logo for Pomorskie"]) ;?>
		</div>
		
		<div class="col-md-3">
			<h4><?= Yii::t('app', 'Office in Brussels') ?></h4>
			<small>
				Rue du Luxembourg 3 (<?= Yii::t('app', '2 floor') ?>)<br />
				1000 <?= Yii::t('app', 'Brussels') ?>, <?= Yii::t('app', 'Belgium') ?><br />
				+32 2 207 13 70 | +48 58 333 4291<br />
				<?= Yii::$app->formatter->asEmail('pomorskie@pomorskieregion.eu') ;?>
			</small>
		</div>
		
		<div class="col-md-3">
			<h4><?= Yii::t('app', 'Office in Gdańsk') ?></h4>
			<small>	
				Ul. Augustyńskiego 2<br />
				80-819 Gdańsk<br />
				+48 58 32 68 709 / 714<br />
				<?= Yii::$app->formatter->asEmail('spue@pomorskieregion.eu') ;?>
			</small>
		</div>
		
		<div class="col-md-3">
			<?= Html::img(Url2::img().'/logo/logo.png', ['class' => 'img-responsive', "alt" => Yii::t('app', 'logo for Pomerianians in Brussel') ]) ;?>
		</div>
		
		
	</div>
	<div class="row">
		<div class="col-md-12">
			<hr />
				<p class="text-center"><?= Yii::t('app', 'Association Headquarters') ?> 80-810 Gdańsk ul. Okopowa 21/27 <?= Yii::t('app', 'VAT') ?> PL 583 288 1404</p>
			<hr />
		</div>		
	</div>		
</div>
</footer>


<?php $this->endBody() ?>

<script type="text/javascript">

	   jQuery("#slider1").revolution({
		  sliderType:"standard",
		  sliderLayout:"auto",
		  disableProgressBar: 'on',
		  delay:9000,
		  navigation: {
		      arrows:{enable:false}
		  },
		  gridheight:175
		});

  var _paq = _paq || [];
_paq.push(['trackPageView']);
_paq.push(['enableLinkTracking']);
(function() {
var u="//pomorskieregion.eu/stats/";
_paq.push(['setTrackerUrl', u+'piwik.php']);
_paq.push(['setSiteId', '1']);
var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
                    })();

</script>
</body>
</html>
