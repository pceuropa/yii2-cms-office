<?php
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\Url2;
use frontend\widgets\EventCalendar;
use frontend\widgets\Banners;  
use yii\widgets\ActiveForm;

$img_facebook = Html::img(Url2::img().'fb.png', ["alt" => "pomorskie region", "class" => "center-block"]);
$img_bip = Html::img('@img/sidebar/bip.png', ['class' => 'clearfix img-responsive ']);

$form = ActiveForm::begin(['method' => 'get', 'action' => ['search/index']]); ?>
	<p>
		<input type="text" name="Search[q]" class="form-control" placeholder="<?= Yii::t('app', 'Search') ?>">
		<?= Html::a ( Yii::t('app', 'Search advanced'), ['search/index'], ['class' => 'pull-right small'] ) ?>
	</p>
<?php ActiveForm::end(); 


echo Html::a ( $img_bip, 'https://bip.pomorskieregion.eu/', ['target' => 'new']);
echo EventCalendar::widget(['language' => Yii::$app->language]); 		
echo Html::a( $img_facebook, 'https://www.facebook.com/POMORSKIE.Regional.EU.Office', ['target' => "blank"]);
?>
<div class="text-center">
<a href="https://twitter.com/Pomorskie_EU" class="twitter-follow-button" data-show-count="false" data-size="small">Follow @Pomorskie_EU</a></div> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

<?=  Banners::sidebar();?>
