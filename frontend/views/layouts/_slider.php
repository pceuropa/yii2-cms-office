<?php
use yii\helpers\Html;

if ($lang === 'pl'){
	$second_land = 'en';
	$url = '/images/slider/';
} else {
	$second_land ='pl';
	$url =  '/images/en/slider/';
}

$var = '<div class="tp-caption News-Title" 							
        data-x="left" data-hoffset="30" 
        data-y="top" data-voffset="15" 							
        data-transform_idle="o:1;"			
        data-transform_in="o:0" 
        data-transform_out="o:0" 							 
        data-start="0">'. Html::a ( Html::img('/images/logo/pomorskie_logo.png', ['class' => 'clearfix img-responsive',  "alt" => "region pomorskie"]), ['site/index', "lang" => $lang]).'
        </div>
        <div class="tp-caption News-Title" 							
        data-x="left" data-hoffset="420" 
        data-y="top" data-voffset="8" 							
        data-transform_idle="o:1;"			
        data-transform_in="o:0" 
        data-transform_out="o:0" 							 
        data-start="0">
        '. Html::a ( Html::img('/images/'.$second_land.'.png', ['class' => 'clearfix img-responsive ',  "alt" => "region pomorskie"]), ['site/index', "lang" => $second_land], [] ) .'</div>';  
?><div class="rev_slider_wrapper">
 <div id="slider1" class="rev_slider"  data-version="5.0">
  <ul>
		<li data-transition="fade"><img src="<?= $url ?>baner_1.jpg"><?= $var ?></li>
		<li data-transition="fade"><img src="<?= $url ?>baner_2.jpg"><?= $var ?></li>
		<li data-transition="fade"><img src="<?= $url ?>baner_3.jpg"><?= $var ?></li>
		<li data-transition="fade"><img src="<?= $url ?>baner_4.jpg"><?= $var ?></li>
		<li data-transition="fade"><img src="<?= $url ?>baner_5.jpg"><?= $var ?></li>
  </ul>				
 </div><!-- END REVOLUTION SLIDER -->
</div><!-- END OF SLIDER WRAPPER -->
