<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use frontend\widgets\Banners;  

AppAsset::register($this);
$this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>

<body>
<?php $this->beginBody();?>


<header>
	<?=  $this->render('_slider', ['lang' => Yii::$app->language]);?>
</header>

<?= $this->render('_navbar') ?>

<div class="container">
	<div class="row">

		<article class="col-md-9">
			<?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]) ?>
			<?= Alert::widget() ?>
			<?= $content ?>
		</article>

		<aside id="sidebar" class="col-md-3">
			<?= $this->render('_sidebar') ?>
		</aside>	

	</div>

	<section id="baners_scrolls"><?= Banners::footer(); ?></section>
</div>
    
<?= $this->render('_footer') ?>

<?php $this->endPage() ?>
