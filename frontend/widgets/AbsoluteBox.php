<?php
namespace frontend\widgets;

use Yii;
use yii\base\Widget;
class AbsoluteBox extends Widget
{
public $side;

public function init()
	{
		parent::init();
		if ($this->side === null) {
			$this->side = 'right';
		}
	}
	
	
public function run()
	{
		return $this->box();
	}
	
public function box()
{
	Yii::$app->view->registerJs( "
	$(document).ready(function(){
		$('[data-toggle=\"modal\"]').tooltip();   
	});");
	return $this->render('abosulte-box');

}
	

	
}
