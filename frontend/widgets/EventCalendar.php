<?php
namespace frontend\widgets;

use Yii;
use yii\helpers\Url;


class EventCalendar extends yii\base\Widget{

	public $language;
	private $js;

	public function init(){

			parent::init();
			if ($this->language === null) {
				$this->language = 'en';
			}
		}
	
	
	public function run(){

			if ($this->language == 'en')  {
			   return $this->en();
			}
		
			return $this->pl();
		}
	
	public function pl()
	{
		Yii::$app->view->registerJs("
			$('#Calendar').eventCalendar({
				eventsjson: '".Url::to('@www/calendar/json-event-calendar')."',
				monthNames: [ 'Styczeń', 'Luty', 'Marzec', 'Kwieceń', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień' ],
				dayNames: [ 'Niedziela','Poniedziałek','Wtorek','Środa','Czwartek', 'Piątek','Sobota' ],
				dayNamesShort: ['Nie', 'Pon','Wt','Śr','Czw', 'Pi','Sob' ],
				txt_noEvents: 'Brak wydarzeń w tym okresie',
				txt_SpecificEvents_prev: '',
				txt_SpecificEvents_after: 'wydarzenia:',
				txt_next: 'następny',
				txt_prev: 'poprzedni',
				txt_NextEvents: 'Wydarzenia',
				txt_GoToEventUrl: 'Zobacz wydarzenie',
			});", 3);

		return $this->render('calendar');

	}
	
	public function en(){
	
		Yii::$app->view->registerJs("
			$('#Calendar').eventCalendar({
				eventsjson: '".Url::to('@www/calendar/json-event-calendar')."',
			});", 3);
	
		return $this->render('calendar');

	}
	
}
