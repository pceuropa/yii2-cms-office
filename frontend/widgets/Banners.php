<?php
namespace frontend\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\Url2;
use common\models\BannersSidebar;
use common\models\BannersFooter;


class Banners {
	
	public function sidebar(){

		$images = BannersSidebar::find()->orderBy(['serialize' => SORT_ASC, ])->all();
		foreach ($images as $key => $val){
	
			$img = html::img(Url2::img(BannersSidebar::IMG_FOLDER) . $val->image, ['class' => 'img-responsive center-block', 'alt' => $val->name]);
			echo  Html::a($img ,  Url::to($val->url, false) );
	 	}	
	}
	
	public function footer(){

		$images = BannersFooter::find()->orderBy(['serialize' => SORT_ASC, ])->all();
		
		foreach ($images as $key => $val){ 

				$img = html::img(Url2::img(BannersFooter::IMG_FOLDER).$val->image, ['alt' => $val->name, 'class' => '']);
				echo Html::a($img,  Url::to($val->url, false) );
		}
	} 
}
