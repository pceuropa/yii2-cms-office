<?php

namespace frontend\widgets;
use common\models\Menus;
use yii\helpers\Html;

class Menu
{
public function PcMenu(){

$menuItems2 = [];
$Menus = Menus::find()->orderBy(['gr' => SORT_ASC, 'serialize' => SORT_ASC, ])->all();


foreach ($Menus as $link): 
 
	if ($link->serialize == 0) {

		$menuItems2[$link->gr] = ['label' => $link->name, 'url' => [$link->url]];

	} else {
		$link->name !== '--------------' ? 
		$menuItems2[$link->gr]['items'][] = ['label' => $link->name, 'url' => [$link->url]] :
		$menuItems2[$link->gr]['items'][] = '<li class="divider"></li>';
	}

endforeach;


return $menuItems2;
}

public function PcMenuEn(){


$menuItems2 = [];
$Menus = Menus::find()->orderBy(['gr' => SORT_ASC, 'serialize' => SORT_ASC, ])->all();


foreach ($Menus as $link): 
	if ($link->serialize == 0) {
		$menuItems2[$link->gr] = ['label' => $link->name];
	} else {
		$menuItems2[$link->gr]['items'][] = ['label' => $link->name, 'url' => [$link->url]];
	}
endforeach;

return $menuItems2;
}

}
