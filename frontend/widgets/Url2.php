<?php
namespace frontend\widgets;
use yii\helpers\Url;

use Yii;

class Url2 {

	public function lang(){
		return Yii::$app->language == 'en' ? 'en/' : '';
	}

	public function img(){
	
		$param = func_get_args();
		$url = Url::to(Yii::$app->language == 'en' ? "@img_en" : "@img");
		
		if (isset ($param[0])) {
			return $url . $param[0] . "/";
		} else {
			return $url;
		}
	}
	
	public function imgPath(){
		$param = func_get_args();
		$url = Url::to(Yii::$app->language == 'en' ? "@img_path_en" : "@img_path");
		if (isset ($param[0])) {
		
			return $url . DIRECTORY_SEPARATOR .  $param[0] . DIRECTORY_SEPARATOR;
		} else {
			return $url;
		}
	}
	
	
}
