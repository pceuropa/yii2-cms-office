﻿<?php 
use yii\helpers\html;
use yii\bootstrap\Modal;

use frontend\models\ContactForm;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
?>


<div id="absolute-box">
<?php
$iso_lang = '';
$r = Yii::$app->request;
if (!Yii::$app->user->isGuest) {
		
		if (Yii::$app->language == 'en') { $iso_lang = 'en/';}
		if ($r->resolve()[0] == 'pages/view'){
			echo Html::a( '<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>', "/panelx/{$iso_lang}pages/update?id=".$r->get('id'));
		}
		if ($r->resolve()[0] == 'site/news'){
			echo Html::a( '<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>', "/panelx/{$iso_lang}news/update?id=".$r->get('id') );
		}
	}
	?>
	
<?= Html::a('<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>', ['#'], ['data-toggle' =>"modal", 'data-placement' => 'left', 'title' => "feedback", 'data-target' => "#modalFeedback"]) ?>
<?= Html::a('<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>', ['#'], ['data-toggle' =>"modal", 'data-placement' => 'left', 'title' => "Like", 'data-target' => "#modalFacebook"]) ?>


</div>
<?php Modal::begin([
    'id' => 'modalFeedback',
    'header' => '<h2>Twoja opinia  o stronie dla webmastera</h2>',

]);
$model = new ContactForm();?>
<div class="row center-block">
	<div class="col-md-6">
		<?php $form = ActiveForm::begin(['id' => 'contact-form', 'action' => ['site/contact']]); ?>
			<?= $form->field($model, 'name') ?>
			<?= $form->field($model, 'email') ?>
			<?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
				'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
			]) ?>
	</div>
	<div class="col-md-6">
			<?= $form->field($model, 'body')->textArea(['rows' => 10]) ?>
			<?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
		<?php ActiveForm::end(); ?>
	</div>
</div><?php Modal::end();?>

<?php Modal::begin([
	'id' => 'modalFacebook',
    'header' => '<h2>POMORSKIE Regional EU Office</h2>'
]);?>
<div class="row">
	<div class="col-md-12"> 
		<div class="fb-like" data-href="https://www.facebook.com/POMORSKIE.Regional.EU.Office" data-layout="standard" data-action="like" data-width="570" data-show-faces="true" data-share="true"></div>
	</div>
	<hr />
	<div class="col-md-6"> x</div>
	<div class="col-md-6"> <div class="fb-send" data-href="<?= $r->getAbsoluteUrl() ;?>"></div> swoim znajomym tą podstronę.</div>
</div>
<?php Modal::end();?>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.4&appId=1393324607664060";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>