<?php
namespace backend\models;

use common\models\User;
use yii\base\Model;
use Yii;

class SignupForm extends \yii\db\ActiveRecord {
    
    public $username;
    public $Names;
    public $email;
    public $password;

    public function rules() {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

			['Names', 'filter', 'filter' => 'trim'],
            ['Names', 'required'],
            ['Names', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['Names', 'string', 'min' => 2, 'max' => 255],
			
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    public function signup() {
    
        if ($this->validate()) {
        
            $user = new User();
            $user->username = $this->username;
            $user->Names = $this->Names;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->save();
        return $user;
        }

        return null;
    }
    
	public function attributeLabels() {
        return [
            'Username' => Yii::t('app', 'xx'),
            'Names' => Yii::t('app', 'Imię Nazwisko'),
        ];
    }
}
