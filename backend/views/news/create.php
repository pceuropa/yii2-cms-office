<?php
use yii\helpers\Html;
$this->title = Yii::t('app', 'Add new');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('js/ckeditor/ckeditor.js', ['position' => 3, 'depends' => 'yii\web\YiiAsset']);

?>

<?= $this->render('_form', [ 'model' => $model, ]) ?>


