<?php
use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Category;
$this->title = Yii::t('app', 'News');
$this->params['breadcrumbs'][] = $this->title;
?>
<?= Html::a('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> '.Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-success']) ?>


<?= GridView::widget([
'dataProvider' => $dataProvider,
'filterModel' => $searchModel,

'columns' => [
['class' => 'yii\grid\SerialColumn'],
	'tytul',
	'autor',
	'data',
	'id',
	[
		'attribute' => 'category_id',
		'value' => function ($m, $key) {
					return  Yii::t('app', Category::getNameById($m->category_id));
				},
		'filter' => Html::activeDropDownList($searchModel, 'category_id', Category::getAllCategories(),['class'=>'form-control','prompt' => Yii::t('app', 'All')]),
	],
['class' => 'yii\grid\ActionColumn'],
],
]); ?>

</div>
