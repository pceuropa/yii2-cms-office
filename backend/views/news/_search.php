<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Category;
$groups=Category::find()->all();
$listData=ArrayHelper::map($groups,'id','name');
?>
<div class="col-md-5">
<?php
$form = ActiveForm::begin([ 'action' => ['index'], 'method' => 'get', ]); 

echo $form->field($model, 'category_id')->widget(Select2::classname(), [
'data' => array_merge(["" => ""], $listData),
'options' => ['placeholder' => Yii::t('app', 'Select category')],
'pluginOptions' => [ 'allowClear' => true ],
]);?>
</div>
<div class="col-md-4"><?= $form->field($model, 'tresc') ?></div>
<div class="col-md-1"><?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?> </div>

<div class="col-md-2">
<?= Html::a( Yii::t('app', 'All news'), '@web/news/index', ['class' => 'btn btn-default'] ) ?>
<?php ActiveForm::end(); ?>
</div>