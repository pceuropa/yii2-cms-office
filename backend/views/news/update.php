<?php
use yii\helpers\Html;

$this->title = Yii::t('app', 'Update').': ';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tytul, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
$this->registerJsFile('js/ckeditor/ckeditor.js', ['position' => 3, 'depends' => 'yii\web\YiiAsset']);
?>

<?= $this->render('_form', [ 'model' => $model, ]) ?>
