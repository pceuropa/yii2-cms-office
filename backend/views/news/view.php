<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\BaseInflector;
use yii\helpers\Url;

Yii::$app->language == 'pl' ? $f = '@www/' : $f = '@wwwEn/';

$this->title = $model->tytul;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h2><?= Html::encode($this->title) ?></h2>
<p>
<?= Html::a('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> '.Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

<?= Html::a('<span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span> '.Yii::t('app', 'Link to page'), $f.BaseInflector::slug($model->tytul, "-").'-n'.$model->id, ['class' => "btn btn-success", 'target' => 'blank']) ?>

<?= Html::a('<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
	'class' => 'btn btn-danger',
	'data' => [
		'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
		'method' => 'post',
	],
]) ?>
</p>

<?= DetailView::widget([
'model' => $model,
'attributes' => [
	'autor',
	'data',
	'tresc:html',
	'zdjecie',
	'seo_title',
	//'seo_url',
	'tags',
	//'czlonek:boolean',
	'category_id',
	'id',
],
]) ?>