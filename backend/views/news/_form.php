<?php
use yii\helpers\Html;

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseStringHelper;

use kartik\datecontrol\Module;
use kartik\widgets\DatePicker;
use kartik\widgets\TimePicker;
use kartik\checkbox\CheckboxX;
use kartik\select2\Select2;
use kartik\widgets\FileInput;

use common\models\Category;
use common\models\News;
use common\models\Pages;
use common\models\PagesAdd;
$this->registerJsFile('@web/js/ckeditor/ckeditor.js');

$category = ArrayHelper::map(Category::find()->all(),'id','name');
$pages = ArrayHelper::map(Pages::find()->all(), 'id','tytul');

use common\models\Tags;
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

<div class="row">
<div class="col-md-4"> <?= $form->field($model, 'tytul')->textInput(['maxlength' => 400]) ?> </div>
<div class="col-md-3">
<?php echo $form->field($model, 'category_id')->widget(Select2::classname(), [
'data' => $category,
'options' => ['placeholder' => Yii::t('app', 'Select category'),],
'pluginOptions' => [
	'allowClear' => true
],
]);?>
</div>
<div class="col-md-2"> <?= $form->field($model, 'autor')->textInput() ?> </div>
<div class="col-md-3">
<?= $form->field($model, 'entity')->textInput() ?>

</div>
</div>

<div class="row">
<div class="col-md-4"><?php $model->isNewRecord ? '' : $model->tags = explode(',', $model->tags);?>
	<?= $form->field($model, 'tags')->widget(Select2::classname(), [
			'data' => Tags::getTags(),
			'options' => ['placeholder' => 'Dodaj, słowa, kluczowe, ...', 'multiple' => true],
			'pluginOptions' => [
				'tags' => true,
				'tokenSeparators' => [','],
				'maximumInputLength' => 35
			],
		]);
	?> </div>
<div class="col-md-4"><?= $form->field($model, 'seo_title')->textInput(['maxlength' => 100])->label(Yii::t('app', 'SEO Title')." <span data-toggle='tooltip' data-placement='right' title='Tytuł dla wyszukiwarek, to zooptymalizwoana wersja tytułu pod google, yahoo. Nie widoczny na stronie, a jedynie w wynikach wyszukiwania.'>(?)</span>") ?></div>


<div class="col-md-4"><?= $form->field($model, 'pages_add')->widget(Select2::classname(), [
		'data' => $pages,
		'options' => ['multiple'=>true],
		'pluginOptions' => [ 'allowClear' => true, ],
		])->label(Yii::t('app', 'Related pages')." <span data-toggle='tooltip' data-placement='right' title='Dodając powiązane strony - na dole tego artykułu pojawią się odnośniki do tych stron'>(?)</span>");?> 
	</div>
</div>
<div class="row">
<div class="col-md-9"> <?= $form->field($model, 'tresc', ['template' => "{input}\n{hint}\n{error}"])->textarea(['col' => 15, 'class' => 'ckeditor',]) ?></div>
<div class="col-md-3">
<?php
		echo $form->field($model, 'data')->widget(DatePicker::classname(), [
		'type' => DatePicker::TYPE_COMPONENT_APPEND,
		'pluginOptions' => [
			'language' => 'pl',
			'autoclose'=>true,
			'format' => 'yyyy-mm-dd'
			]
		]);
	?>
	
	
	<?= $form->field($model, 'file')->widget(FileInput::classname(), [
    'options' => ['accept' => 'image/*'],
	    'pluginOptions' => [
        'showPreview' => false,
        'showCaption' => true,
        'showRemove' => true,
        'showUpload' => false
    ]
]);?> 

<?php
		//$model->pages_add = PagesAdd::getIdPagesRelatedNews($model);
		$model->pages_add = $model->getIdRelatedPages($model);
	?>
	
		
		<div class="form-group">
<?= Html::submitButton(
	$model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), 
	['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

</div>
</div>
</div>
<?php

	$this->registerJs("$(document).ready(function(){
     $('[data-toggle=\"tooltip\"]').tooltip();
});", 3);

 ActiveForm::end(); ?>
