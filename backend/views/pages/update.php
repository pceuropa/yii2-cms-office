<?php
use yii\helpers\Html;

$this->title = 'Aktualizacja ' . $model->tytul;
$this->params['breadcrumbs'][] = ['label' => 'Strony', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tytul, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Aktualizacja';
$this->registerJsFile('js/ckeditor/ckeditor.js', ['position' => 3, 'depends' => 'yii\web\YiiAsset']);
?>

<?= $this->render('_form', [ 'model' => $model,]) ?>
