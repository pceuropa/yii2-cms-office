<?php
use yii\helpers\Html;

$this->title = 'Nowa Strona';
$this->params['breadcrumbs'][] = ['label' => 'Strony', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('js/ckeditor/ckeditor.js', ['position' => 3, 'depends' => 'yii\web\YiiAsset']);
?>
 <?= $this->render('_form', ['model' => $model,]) ?>
