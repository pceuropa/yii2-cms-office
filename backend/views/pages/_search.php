<?php

/* @var $this yii\web\View */
/* @var $model common\models\SearchStrony */
/* @var $form yii\widgets\ActiveForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use common\models\Category;

$groups = Category::find()->all();
$listData = ArrayHelper::map($groups,'id','name');

?>
<div class="col-md-5">
	<?php $form = ActiveForm::begin([ 'action' => ['index'], 'method' => 'get', ]); 
	echo $form->field($model, 'cat_list')->widget(Select2::classname(), [
		'data' => $listData,
		'options' => ['placeholder' => 'Wybierz kategorię'],
		'pluginOptions' => [ 'allowClear' => true ],
	]);?>
</div>
<div class="col-md-4"><?= $form->field($model, 'tresc') ?></div>
<div class="col-md-1"><?= Html::submitButton('Szukaj', ['class' => 'btn btn-primary']) ?> </div>

<div class="col-md-2">
	<?= Html::a( 'Pełna lista', '@web/strony/index', ['class' => 'btn btn-default'] ) ?>
	<?php ActiveForm::end(); ?>
</div>
