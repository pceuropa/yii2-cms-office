<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use kartik\widgets\DatePicker;
use kartik\checkbox\CheckboxX;
use kartik\widgets\Select2;

use common\models\Category;
use common\models\News;
use common\models\Pages;
use common\models\PagesAdd;

$this->registerJsFile('@web/js/ckeditor/ckeditor.js');

$category = ArrayHelper::map(Category::find()->all(),'id','name');
$news = ArrayHelper::map(News::find()->all(), 'id','tytul');
$pages = ArrayHelper::map(Pages::find()->all(), 'id','tytul');

?>


<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<div class="row">
<div class="col-md-4"> <?= $form->field($model, 'tytul')->textInput() ?> </div>
<div class="col-md-4"> <?= $form->field($model, 'autor')->textInput() ?> </div>


	
<div class="col-md-4">
	<?= $form->field($model, 'entity')->textInput() ?>
	
</div>

</div>

<div class="row">
	<div class="col-md-3">
		<?php echo $form->field($model, 'data')->widget(DatePicker::classname(), [
		'options' => ['placeholder' => ''],
		'type' => DatePicker::TYPE_COMPONENT_APPEND,
		'pluginOptions' => [
		'language' => 'pl',
		'autoclose'=>true,
		'format' => 'yyyy-mm-dd'
		]
		]);?>
	</div>
	
	<div class="col-md-4">
		<?= $form->field($model, 'seo_title')->textInput()->label(Yii::t('app', 'SEO Title')." <span data-toggle='tooltip' data-placement='right' title='Tytuł dla wyszukiwarek, to zooptymalizwoana wersja tytułu pod google, yahoo. Nie widoczny na stronie, a jedynie w wynikach wyszukiwania.'>(?)</span>") ?>
	</div>
	
	<div class="col-md-5">
	
		<?php $model->isNewRecord ? '' : $model->category_id = explode(',', $model->cat_list); 
		
		echo $form->field($model, 'category_id')->widget(Select2::classname(), [
			'data' => array_merge(["" => ""], $category),
			'options' => ['placeholder' => Yii::t('app', 'Select one or two category'), 'multiple'=>true],
			'pluginOptions' => [ 'allowClear' => true, ],
		])->label(Yii::t('app', 'Categories')." <span data-toggle='tooltip' data-placement='right' title='Tworzenie kategorii pomaga segregować i wyszukiwać informacje na stronie.'>(?)</span>");?>
		
	</div>
	
	
	<div class="col-md-12">
		<?= $form->field($model, 'tresc', ['template' => "{input}\n{hint}\n{error}"])->textarea(['rows' => 6, 'class' => 'ckeditor',]) ?>
	</div>
	
	
			
	<?php $model->news_add  = PagesAdd::getIdNewsRelatedPages($model);?>
	<div class="col-md-6">
		<?= $form->field($model, 'news_add')->widget(Select2::classname(), [
		'data' => $news,
		'options' => ['multiple'=>true],
		'pluginOptions' => [ 'allowClear' => true, ],
		])->label(Yii::t('app', 'Related news')." <span data-toggle='tooltip' data-placement='right' title='Dodając powiązane aktualności - na dole tego artykułu pojawią się odnośniki do tych aktualności'>(?)</span>");?>
	</div>

	
	<?php $model->pages_add  = PagesAdd::getIdPagesRelatedPages($model);?>
	
	<div class="col-md-6">
		<?= $form->field($model, 'pages_add')->widget(Select2::classname(), [
		'data' => $pages,
		'options' => ['multiple'=>true],
		'pluginOptions' => [ 'allowClear' => true, ],
		])->label(Yii::t('app', 'Related pages')." <span data-toggle='tooltip' data-placement='right' title='Dodając powiązane strony - na dole tego artykułu pojawią się odnośniki do tych stron'>(?)</span>");?>
	</div>

</div>

<div class="form-group">
	<?= Html::submitButton($model->isNewRecord ? 'Utwórz' : 'Aktualizuj', 
	['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	
	
</div>
<?php

	$this->registerJs("$(document).ready(function(){
     $('[data-toggle=\"tooltip\"]').tooltip();
});", 3);

?>
<?php ActiveForm::end(); ?>
