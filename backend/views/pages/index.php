<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
$this->title = Yii::t('app', 'Strony');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= Html::a('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> '.Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-success']) ?>

<?= GridView::widget([
'dataProvider' => $dataProvider,
'filterModel' => $searchModel,
'columns' => [
		['class' => 'yii\grid\SerialColumn'],
		'tytul',
		'autor',
		'data','id',
		
		// 'czas',
		// 'tresc:ntext',
		// 'zdjecie',
		// 'seo_title',
		// 'seo_url:url',
		// 'czlonek',
		// 'category_id',

		['class' => 'yii\grid\ActionColumn',],
],
]); ?>
