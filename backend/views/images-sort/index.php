<?php
use pceuropa\sort\SortWidget;
use yii\helpers\Url;
use frontend\widgets\Url2;

	$this->title = Yii::t('app', 'Create', ['modelClass' => 'BannersFooter',]);
	$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'BannersFooter'), 'url' => ['index']];
	$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
	<div class="col-md-4">
			<?= SortWidget::widget([
				'image_url' => Url2::img($model::IMG_FOLDER),  // example Url::to('@banners', 'http'),
				'data' => $model::find()->orderBy(['serialize' => SORT_ASC])->all(),
			]) ?>
	</div>
	<div class="col-md-8"><?= $this->render('_form', [ 'model' => $model]) ?></div>
</div>
