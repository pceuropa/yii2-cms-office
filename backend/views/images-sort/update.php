<?php
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\Url2;

	$this->title = Yii::t('app', 'Update');
	$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Buttons'), 'url' => ['index']];
	$this->params['breadcrumbs'][] = $model->name;
	$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>


    <h2><?= Yii::t('app', 'Update') ?></h2>
    
    <div class="row">
		<div class="col-md-4">
			<?php
				if ($model->__isset('image')){
					echo Html::img( Url2::img($model::IMG_FOLDER) .$model->image, ['class' => 'clearfix img-responsive ']);
				}
			?>
		</div>
		<div class="col-md-8"><?= $this->render('_form', [ 'model' => $model]) ?></div>
		
		<?= Html::a ( Yii::t('app', 'Back'), ['index'], ['class' => 'pull-right btn btn-primary'] ); ?>
	</div>
