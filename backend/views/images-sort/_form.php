<?php
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
	//use dosamigos\fileupload\FileUpload;
?>


<?php $form = ActiveForm::begin([
	'id' => 'ajax-file-upload',
 	//'enableAjaxValidation' => true,
 	//'validationUrl' => 'validate',
 	'options'=>[
 		'enctype'=>'multipart/form-data'
 	]
]); ?>

	<div class="row">
		<div class="col-md-6"><?= $form->field($model, 'url')->textInput(['maxlength' => 255, ]); ?></div>
		<div class="col-md-6"><?= $form->field($model, 'name')->textInput(['maxlength' => 255,  ]) ?></div>
	</div>
		<?= $form->field($model, 'files')->fileInput(['class' => 'btn btn-default' ])->label('zdjęcie') ?>
	<br/>
   
  
   	<?=  Html::submitButton('Submit', ['class' => 'btn btn-success'])?>
   	
<?php ActiveForm::end(); ?>


<?php

$this->registerCss("
	#baners_scrolls img{}
	#baners_scrolls ul { margin: 0; padding: 0; list-style-type: none; }
	#baners_scrolls ul li{ padding: 5px; border:solid 1px #ccc; cursor:grab;}
	#baners_scrolls ul li:hover{ border:solid 1px #5CB85C;}
	
	#baners_scrolls ul li img{ max-height:75px; }
	
	#baners_scrolls .big{
		font-size:200%;
		color:#eee;
		margin-top:25px;
		
	}
	#baners_scrolls .edit-icon a span { margin-top:15px}
	#baners_scrolls .edit-icon a span.glyphicon-trash { color: #D75555}
	
	
	.fileinput-button {
	  position: relative;
	  overflow: hidden;
	  display: inline-block;
	}
	
	.fileinput-button input {
	  position: absolute;
	  top: 0;
	  right: 0;
	  margin: 0;
	  opacity: 0;
	  -ms-filter: 'alpha(opacity=0)';
	  font-size: 300px !important;
	  direction: ltr;
	  cursor: pointer;
	}

	/* Fixes for IE < 8 */
	@media screen\9 {
	  .fileinput-button input {
		filter: alpha(opacity=0);
		font-size: 100%;
		height: 100%;
	  }
}");   

?>

