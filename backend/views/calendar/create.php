<?php
use yii\helpers\Html;
$this->title = Yii::t('app', 'Nowe wydarzenie', [ 'modelClass' => 'Calendar', ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kalendarz'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('_form', [ 'model' => $model, ]) ?>