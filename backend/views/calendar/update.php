<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model common\models\Calendar */

$this->title = Yii::t('app', 'Aktualizuj: ', [ 'modelClass' => 'Calendar', ]) . ' ' . $model->event;
$this->params['breadcrumbs'][] = ['label' => 'Kalendarz', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->event, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Aktualizuj');
?>


<div class="calendar-update">
<h3><?= Html::encode($this->title) ?></h3>
<?= $this->render('_form', [ 'model' => $model, ]) ?>
</div>
