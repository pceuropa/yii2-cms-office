<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\Calendar */

$this->title = $model->event;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kalendarz'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
Yii::setAlias('@alias', 'http://pomorskieregion.eu/');?>
<div class="calendar-view">

<h1><?= Html::encode($this->title) ?></h1>

<p>
<?= Html::a(Yii::t('app', 'Aktualizuj'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<a href="<?php echo Url::to('@alias/events?date='.$model->date, true) ;?>" class ="btn btn-primary" >Link do strony</a>
<?= Html::a(Yii::t('app', 'Skasuj'), ['delete', 'id' => $model->id], [
'class' => 'btn btn-danger',
'data' => [
'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
'method' => 'post',
],
]) ?>
</p>

<?= DetailView::widget([
'model' => $model,
'attributes' => [
'date',
'place',
'event',
'content_event:html',
'czlonek:boolean',
'id',
],
]) ?>

</div>
