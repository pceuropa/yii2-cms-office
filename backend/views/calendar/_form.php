<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\datecontrol\Module;
use kartik\widgets\DatePicker;
use kartik\checkbox\CheckboxX;
$this->registerJsFile('@web/js/ckeditor/ckeditor.js');
?>


<?php $form = ActiveForm::begin(); ?>
<div class="row">
<div class="col-md-4">
<?php
	echo $form->field($model, 'date')->widget(DatePicker::classname(), [
	'options' => ['placeholder' => ''],
	'pluginOptions' => [
	'autoclose'=>true,
	'format' => 'yyyy-mm-dd'
	]
	])->label('Data wydarzenia');
?>
</div>
<div class="col-md-4">
	<?= $form->field($model, 'event')->textInput(['maxlength' => 255])->label('Nazwa wydarzenia') ?>
</div>
<div class="col-md-4">
	<?= $form->field($model, 'place')->textInput(['maxlength' => 255]) ?>
</div>

</div>

<?= $form->field($model, 'content_event', ['template' => "{input}\n{hint}\n{error}"])->textarea(['rows' => 6, 'class' => 'ckeditor',]) ?>
<div class="form-group">
<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Utwórz') : Yii::t('app', 'Aktualizuj'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

</div>


<?php ActiveForm::end(); ?>
