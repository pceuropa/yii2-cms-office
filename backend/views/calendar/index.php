<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SearchCalendar */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kalendarz';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= Html::a('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> '.Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-success']) ?>

<?= GridView::widget([
'dataProvider' => $dataProvider,
'filterModel' => $searchModel,
'columns' => [
['class' => 'yii\grid\SerialColumn'],
	'date',
	'event',
	'place',
	'id',
	'czlonek:boolean',
	['class' => 'yii\grid\ActionColumn'],
],
]); ?>
