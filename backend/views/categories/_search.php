<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="category-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'name') ?>
	<?= $form->field($model, 'id') ?>
	<?= $form->field($model, 'create') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::a( Yii::t('app', 'Reset search'), '@web/categories/index', ['class' => 'btn btn-default'] ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
