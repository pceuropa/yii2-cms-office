<?php
use yii\helpers\Html;

$this->title = Yii::t('app', 'Create Category', ['modelClass' => 'Category',]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('_form', ['model' => $model,]) ?>