<?php
	use yii\helpers\Html;
	use yii\grid\GridView;
	
	$this->title = Yii::t('app', 'Categories');
	$this->params['breadcrumbs'][] = $this->title;
?>

<h2>
	<?= Html::encode($this->title) ?> 
	<?= Html::a(Yii::t('app', 'Add', ['modelClass' => 'Category',]),['create'], ['class' => 'btn btn-success']) ?>
</h2>


<?= GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			'name',
			[
				'attribute' => 'group',
				'value' => function ($m, $key) {
							return  Yii::t('app', $m::getNameById($m->group));
						},
				'filter' => Html::activeDropDownList($searchModel, 'group', $searchModel::getAllCategories(),['class'=>'form-control','prompt' => Yii::t('app', 'All')]),
			],
			['class' => 'yii\grid\ActionColumn',],
	],
]); ?>
