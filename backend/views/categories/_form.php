<?php
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
	use common\models\Category;
	use yii\helpers\ArrayHelper;
	use kartik\widgets\Select2;
	
	$form = ActiveForm::begin();
	$group = ArrayHelper::map(Category::find()->all(), 'id', 'name');
?>

<div class="row">
	<div class="col-md-6"><?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?></div>
	<div class="col-md-6">
		<?php echo $form->field($model, 'group')->widget(Select2::classname(), [
			'data' => $group,
			'options' => ['placeholder' => Yii::t('app', 'Select category')],
			'pluginOptions' => [
				'allowClear' => true
			],
		]);?>
	</div>
</div>
	
<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>
