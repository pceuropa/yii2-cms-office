<div class="form-group">
	<label class="col-sm-2 control-label">Type</label>
	<div class="col-sm-10">
		<select id="type" class="form-control input-sm data-source">
			<option>text</option>
			<option>email</option>
			<option>password</option>
			<option>date</option>
			<option>number</option>
			<option>url</option>
			<option>tel</option>
			<option>color</option>
		</select>
	</div>
</div>
