<div class="form-group">

	<label class="col-sm-2 control-label">Width</label>
		<div class="col-sm-6">
			<select id="width" class="form-control input-sm data-source">
					<option value="col-md-12" selected>100% (col-md-12)</option>
					<option value="col-md-9">75% (col-md-9)</option>
					<option value="col-md-6">50% (col-md-6)</option>
					<option value="col-md-4">33% (col-md-4)</option>
					<option value="col-md-3">25% (col-md-3)</option>
			</select>
		</div>

</div>
