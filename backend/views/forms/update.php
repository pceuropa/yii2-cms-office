<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Update') ;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Forms') , 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo \pceuropa\forms\FormBuilder::widget([
	'table' => 'poll_',     // 'form_' . $id
	'config' => [
			'get'=> true, 
			'save'=> true, 
			'autosave' => true,
			//'controller_url' => Url::to(['forms/update', 'id' => $id]), 
		]
]);
?>
