<?php
#Copyright (c) 2017 Rafal Marguzewicz pceuropa.net
use yii\helpers\Html;

$this->title = 'Form Create';
$this->params['breadcrumbs'][] = ['label' => 'Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


echo \pceuropa\forms\FormBuilder::widget([
		'test_mode' => false,
		'easy_mode' => true,
		
]);
?>
