<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'Panel administracyjny';
?>
<div class="row">
	<div class="col-md-8">
	<h2>Manual</h2>

	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="heading1">
	      <h4 class="panel-title">
	        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
	          MAPA STRONY
	        </a>
	      </h4>
	    </div>
	    <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
	      <div class="panel-body">
	      <ul>

	      	
	      	</li>
	        <li><h4><?= Html::a ('Strona główna', '//pomorskieregion.eu/', ["target" => "new"] ) ?></h4>
	        <ul>
	          <li><?= Html::a ('kalendarz wydarzeń', '//pomorskieregion.eu/calendar', ["target" => "new"] ) ?></li>
	          <li><?= Html::a ('galeria', '//pomorskieregion.eu/gallery', ["target" => "new"] ) ?></li>
	          <li><?= Html::a ('mapa strony', '//pomorskieregion.eu/mapa-strony-p88', ["target" => "new"] ) ?></li>
	        </ul></li>
	        <li><h4>Panel administarcyjny	(url: /panelx)</h4>
	        <ul>
	          <li><?= Html::a ('Strony', ['pages/index']) ?></li>
	          <li><?= Html::a ('Aktualności', ['news/index']) ?></li>
	          <li><?= Html::a ('Wydarzenia', ['calendar/index']) ?></li>
	          <li><?= Html::a ('Galerie', ['gallery/index'] ) ?>)</li>
	          <li>Inne:<ul>
	         <li><?= Html::a ('Edytor menu głównego', ['menu/creator']) ?></li>
	            <li><?= Html::a ('Zdjęcia w kolumnie prawej', ['/banners-sidebar']) ?></li>
	            <li><?= Html::a ('Zdjęcia w stopce', ['/banners-footer']) ?></li>
	            <li><?= Html::a ('Kategorie', ['categories/index']) ?></li>
	          </ul></li>
	        </ul>
	        </li>
	        	      	<li><h4><?= Html::a ( 'Statystyki odwiedzin', "//pomorskieregion.eu/stats/index.php", ["target" => "new"] ) ?> L: staty H: St@ty2016</h4>
	      	<h4><?= Html::a ( 'Poczta Webowa', "https://mail.google.com", ["target" => "new"] ) ?> </h4>
	      	
	      </ul>


	      </div>
	    </div>
	  </div>


	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="heading2">
	      <h4 class="panel-title">
	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
	           STRONY
	        </a>
	      </h4>
	    </div>
	    <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
	      <div class="panel-body">
	      
	        <ol>
	        <li>Podstrony tworzone w tej sekcji służą do tworzenia strony typu: opis projektu, kontakt, mapa strony itp</li>
	          <li>Link do utworzonej przez nas strony nie pojawia się automatycznie w menu, lub na stronie głównej</li>
	          <li>Aby móc trafić do tej strony, możemy sami do niej zlinkować z <?= Html::a ( 'menu', ['menus/creator'], [] ) ?>, aktualności lub innej strony</li>
	          <li>Tytuł nie powinnien być długi (nie mieścić się w jednej linni na stronie)</li>
	          <li>Jeżeli jednak tytuł jest dłuższy niż 70 znaków, trzeba wpisać skróconą wersje tego tytułu w pole <code>Tytuł dla wyszukiwarek</code> ponieważ roboty wyszukiwarek (google, yahoo) pobierają tylko maksymalnie 70 znaków.</li>
	          <li>Pola <code>powiązane aktualności</code> i <code>strony</code> pomagają dodawać powiązania między stronami. Lista wyboru pokazuje wszyskie możliwości, wpisanie kilku liter rozpoczyna filtrowanie wyników listy.</li>
	        </ol>

	       
	      </div>
	    </div>
	  </div>

	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="heading3">
	      <h4 class="panel-title">
	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
	           AKTUALNOŚCI
	        </a>
	      </h4>
	    </div>
	    <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
	      <div class="panel-body">
	      <ol>
	        <li>Podstrony tworzone w tej sekcji służą do tworzenia <b>artykułów informujących o ostatnich wydarzeniach</b>. </li>
	        <li>Zamieszczane są automatycznie na stronie głównej od najnowszych do najstarszych</li>
	        <li>Tytuł nie powinnien być długi (nie mieścić się w jednej linni na stronie)</li>
	          <li>Jeżeli tytuł jest dłuższy niż 70 znaków, trzeba wpisać skróconą wersje tego tytułu w pole <code>Tytuł dla wyszukiwarek</code> ponieważ roboty wyszukiwarek (google, yahoo) pobierają tylko maksymalnie 70 znaków.</li>
	      </ol>
	       rszych. 
	      </div>
	    </div>
	  </div>

	   <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="heading4">
	      <h4 class="panel-title">
	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="Four">OPCJE EDYTORA
	        </a>
	      </h4>
	    </div>
	    <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
	      <div class="panel-body">
	      <?= Html::img('@web/images/edytor.png', ['class' => 'clearfix img-responsive']) ?>
	         <ol>
	             <li><b>Dodawanie zdjęcia:</b><ul>
	               <li>klikamy na ikonkę</li>
	               <li>2 zakładka - wyślij</li>
	               <li>browse</li>
	               <li>wybieramy zdjęcie z naszego dysku</li>
	               <li>załaduj zdjęcie</li>
	               <li>możemy zmienić tekst zastępczy na tekst związany z tematem artykułu</li>
	               <li>ok</li>
	             </ul>
	            </li>
	            
	             <li><b>Dodawanie linka:</b>
	             <ul>
	               <li>zaznaczmy tekst który za zostać linkiem</li>
	               <li>klikamy na ikonkę</li>
	               <li>wpisujemy adres URL</li>
	               <li>ok</li>
	             </ul></li>
	             
	             
	             <li><b>Tworzenie spisu treści:</b>
Tworzenie spisu treści możemy podzielić na 2 etapy, oznaczanie tekstów docelowych oraz tworzenie indeksu linków do tych elementów.
<ol>
  <li><b>Oznaczyczanie elementu jako docelowy:</b>
		<ul>
			<li>zaznaczmy tekst</li>
			<li>klikamy na flagę I wpisujemy tekst opisowy (np kotwica1)</li>
		</ul>
	</li>
  <li><b>Tworzenie elementu spisu treści:</b>
  <ul>
		<li>dopisujemy tekst spisu treści</li>
		<li>zaznaczamy go</li>
		<li>wybieramy ikonę linku</li>
		<li>W polu typ odnośnika wybieramy, odnośnik wewnątrz strony (kotwica)</li>
		<li>wybieramy kotwicę wg nazwy</li>
		<li>powtarzamy tą operację dla każdego punktu spisu treści</li>
	</ul>
	</li>
</ol><br />
</li>
	             <li><b>Tworzenie dwóch lub 3 kolumn:</b> kliknięcie na ikonkę</li>
	             <li><b>Uswanie formatowania tekstu:</b> Aby uniknąć nakładania się formatowania. Np gdy mamy tekst pogrubiony a chcemy zrobić z niego linka, warto usunąć pobrubienie a następnie ustawić tekst jako link<br />
	             </li>
	             <li><b>Rozwiązywanie problemów z formatowaniem (Html dokumentu)</b>
	             <ol>
	             <li>Edytując dokument w tym trybie trzeba zwracać uwagę na elementy HTML jak pudełka opakowujące <?= Html::a( 'Div', "http://www.w3schools.com/tags/tag_div.asp", [] ) ?></li>
	               <li>Jeżeli nie znasz jezyka <?= Html::a ( 'HTML5', "http://www.w3schools.com/html/html5_intro.asp", [] ) ?> to najlepiej nie doprowadzaj do błędów składni, poprzez używanie ikony cofania błędów oraz edycji tekstu w trybie źródło dokumentu.</li>
	             </ol>
	              </li>
	           </ol>
	      </div>
	    </div>
	  </div>

	   <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="heading5">
	      <h4 class="panel-title">
	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">KALENADRIUM</a>
	      </h4>
	    </div>
	    <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
	      <div class="panel-body">
	        Wydarzenia dodajemy podając date, nazwę I miejsce wydażenia (np. Bruksela
	      </div>
	    </div>
	  </div>

	   <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="heading6">
	      <h4 class="panel-title">
	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
	           GALERIA
	        </a>
	      </h4>
	    </div>
	    <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
	      <div class="panel-body">
	      Tworzenie galerii zdjęć.
	        <ul>
	          <li>Przechodzimy do <?= Html::a ( 'sekcji galleria', ['gallery/index'], ["target" => "blank"] ) ?></li>
	          <li>Klikamy <?= Html::a ( '+ Dodaj', ['gallery/create'], ["target" => "blank"] ) ?></li>
	          <li>Podając jej nazwę i opcjonalnie datę (domyślnie ustawia date dzisiejszą) utworzymy pustą galerię.</li>
	          <li>Następnie możemy dodać zdjęcia. Jeżeli twój system nie osługuje zaznaczania wielu zdjęć możesz użyć klawisza shift lub Ctrl</li>
	        </ul>
	        
	        <?= Html::a ( 'Lista galerii zdjęć', "//pomorskieregion.eu/gallery", ["target" => "blank"] ) ?> na frontend.
	      </div>
	    </div>
	  </div>

	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="heading6a">
	      <h4 class="panel-title">
	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6a" aria-expanded="false" aria-controls="collapse6a">KATEGORIE</a>
	      </h4>
	    </div>
	    <div id="collapse6a" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6a">
	      <div class="panel-body">
	       <?= Html::a ( 'Tworzenie kategorii', ['categories/index'], ["target" => "blank"] ) ?> pomaga segregować strony, oraz poszukiwaniu informacji na stronie.
	      </div>
	    </div>
	  </div>


	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="heading7">
	      <h4 class="panel-title">
	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse5">MENU GŁÓWNE</a>
	      </h4>
	    </div>
	    <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
	      <div class="panel-body">
	       <?= Html::a ( 'Ta sekcja pozwala', ['menu/index'], ["target" => "blank"] ) ?> na modyfikację poziomego menu głównego.
	      </div>
	    </div>
	  </div>

	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="heading8">
	      <h4 class="panel-title">
	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="false" aria-controls="collapse5">BANERY W KOLUMNIE PRAWEJ</a>
	      </h4>
	    </div>
	    <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
	      <div class="panel-body">
	       <?= Html::a ( 'Ta sekcja', ['banners-sidebar/index'], ["target" => "blank"] ) ?> pozwala dodać banery lub linki w kolumnie prawiej pod kalendarzem.
	      </div>
	    </div>
	  </div>

	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="heading9">
	      <h4 class="panel-title">
	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse9" aria-expanded="false" aria-controls="collapse5">ZDJĘCIA W STOPCE</a>
	      </h4>
	    </div>
	    <div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9">
	      <div class="panel-body">
	        <p>Zdjęcia dodane w <?= Html::a ( 'tej sekcji', ['banners-footer/index'], ["target" => "blank"] ) ?> to pływające banery na samym dole strony</p>
	        
	        
	        <?= Html::img('@web/images/manual/banners-footer.png', ['class' => 'clearfix img-responsive ']) ?>
	      </div>
	    </div>
	  </div>

	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="heading11">
	      <h4 class="panel-title">
	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse11" aria-expanded="false" aria-controls="collapse5">WYMOGI EDYCJI STRON POD WYSZYKIWARKI</a>
	      </h4>
	    </div>
	    <div id="collapse11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9">
	      <div class="panel-body">
	      Pamiętajmy o:
	      <ul>
	        <li>Najwaniejsze słowa kluczowe pogrubiamy (max 3 razy) (  <?= Html::a ( 'przykład poparawny', '//pomorskieregion.eu/relacja-z-seminarium-regionalnego-ograniczenie-niskiej-emisji-w-gminach-n790', ["target" => "blank"] ) ?> | <?= Html::a ( 'przykład zły', '//pomorskieregion.eu/posiedzenie-komisji-nat-i-coter-w-brukseli-w-dniach-1-3-n788', ["target" => "blank"] ) ?>)</li>
	        <li>Zamieszczenie kilku linków do innych podstron pomagających lepiej zrozumieć zagadnienie</li>
	        <li>Jeżeli tytuł strony jest za długi (+70 znaków) to wpisz skruconą wersję w polu Tytuł dla wyszukiwarek (<?= Html::a ( 'przykład poparawny', ['news/update', 'id' => 790], ["target" => "blank"] ) ?>)</li>
	        <li><?= Html::a ( '10 zasad SEO', "http://www.internetstandard.pl/news/395437/10.zasad.budowania.stron.WWW.przyjaznych.Google.html", [] ) ?></li>

	      </ul>
 <h3>Lista poprawnych stron</h3>
 
 <table class="table table-striped">
  <tr class="success"><td>Autor</td><td >Artykuł</td><td>Zapytanie google</td></tr>
  <tr><td>Karina Rembiewska</td><td ><?= Html::a ( 'Województwo Pomorskie', '//pomorskieregion.eu/wojewodztwo-pomorskie-p194', ["target" => "new"] ) ?></td><td><?= Html::a ( 'uprawianie aktywnego wypoczynku woj pomor', "https://www.google.pl/search?q=uprawianie+aktywnego+wypoczynku+woj+pomor", ["target" => "new"] ) ?></td></tr>
  
  <tr><td>Małgorzata Matkowska</td><td ><?= Html::a ( 'Relacja z seminarium regionalnego "Ograniczenie niskiej emisji w gminach"', '//pomorskieregion.eu/relacja-z-seminarium-regionalnego-ograniczenie-niskiej-emisji-w-gminach-n790', ["target" => "new"] ) ?></td><td>-</td></tr>
  
  <tr><td>Karina Rembiewska</td><td ><?= Html::a ( 'Instytucje Unii Europejskiej', '//pomorskieregion.eu/panelx/pages/update?id=61', ["target" => "new"] ) ?></td><td><?= Html::a ( 'obywateli państw członkowskich reprezentuje', "https://www.google.be/search?q=obywateli+pa%C5%84stw+cz%C5%82onkowskich+reprezentuje", ["target" => "new"] ) ?></td></tr>
  <tr><td>Karina Rembiewska</td><td ><?= Html::a ( 'Natura 2000 w przyszłym programowaniu 2014 – 2020', '//pomorskieregion.eu/natura-2000-w-przyszym-programowaniu-2014-2020-p109', ["target" => "new"] ) ?></td><td><?= Html::a ( 'Finansowanie sieci Natura 2000', "https://www.google.pl/search?site=&source=hp&q=Finansowanie+sieci+Natura+2000&oq=Finansowanie+sieci+Natura+2000", ["target" => "new"] ) ?></td></tr>
  
  <tr><td>Karina Rembiewska</td><td ><?= Html::a ( 'Programy i inicjatywy sektorowe UE 2014-2020', '//pomorskieregion.eu/programy-i-inicjatywy-sektorowe-ue-2014-2020-p92', ["target" => "new"] ) ?></td><td><?= Html::a ( "Programy i inicjatywy sektorowe UE", "https://www.google.pl/search?q=Programy+i+inicjatywy+sektorowe+UE+&oq=Programy+i+inicjatywy+sektorowe+UE", ["target" => "new"] ) ?></td></tr>
  
</table>	 
	      </div>
	    </div>
	  </div>


	</div></div>
	<div class="col-md-4">
	<?= Html::a ( 'Email do PCEUROPA', "mailto:info@pceuropa.net?subject=Wiadomość z pomorskieregion.eu" ) ?>
	</div>
</div>
