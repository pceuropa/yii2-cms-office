<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    

    <div class="row">
        <div class="col-lg-6">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
		<div class="row">
			<div class="col-md-6"><?= $form->field($model, 'username') ?></div>
			<div class="col-md-6"><?= $form->field($model, 'password')->passwordInput() ?></div>
		</div>
                <?= $form->field($model, 'rememberMe')->checkbox(['label' => 'Pamietaj',]) ?>
				<div style="color:#999;margin:1em 0">
                    <?= Html::a('Zapomniałem hasło', ['site/request-password-reset']) ?>.
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
