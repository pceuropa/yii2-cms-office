<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Użytkownicy', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Aktualizuj', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Usuń', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
		'username',
		 'Names',
		  'email:email',
            // 'id',
            // 'UserTypeId',
            
            // 'auth_key',
            // 'password_hash',
            // 'password_reset_token',
            
            
            // 'PreferredName',
            // 'Surname',
            // 'Gender',
            // 'Birthday',
            // 'Website',
            // 'FacebookId',
            // 'TwitterId',
            // 'IsLikedFanPage',
            // 'PhotoUrl:url',
            // 'IsPersonal',
            // 'IdentityNumber',
            // 'TaxNumber',
            // 'TaxOffice',
            // 'Comment:ntext',
            // 'role',
            // 'status',
            // 'lastLogin',
            // 'previousLogin',
            // 'created_by',
            // 'LastUpdatedBy',
            // 'created_at',
            // 'updated_at',
            // 'deleted_at',
        ],
    ]) ?>

</div>
