<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
<?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
	<?= $form->field($model, 'username') ?>

	<?= $form->field($model, 'email') ?>
	
<div class="form-group">
	<?= Html::submitButton('Aktualizuj', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
</div>
<?php ActiveForm::end(); ?>




</div>
