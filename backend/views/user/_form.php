<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

<div class="row">
	<div class="col-md-6"><?= $form->field($model, 'username') ?></div>
	<div class="col-md-6"><?= $form->field($model, 'email') ?></div>
	<div class="col-md-6"><?= $form->field($model, 'Names') ?></div>
	<div class="col-md-6"><?= $form->field($model, 'password')->passwordInput(); ?></div>
</div>

	
	<?= Html::submitButton('Dodaj', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
<?php ActiveForm::end(); ?>

