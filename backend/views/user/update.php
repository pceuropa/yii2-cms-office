<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Aktualizacja';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

	<div class="user-update">
	
	<h4><?= Html::encode($this->title) ?></h4>
	<?= $this->render('_update', [ 'model' => $model, ]) ?>
	
	</div>