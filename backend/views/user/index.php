<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>

    <h1><?= Html::encode($this->title) ?> <?= Html::a(Yii::t('app', 'add'), ['create'], ['class' => 'btn btn-success']) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'UserTypeId',
            'username',
            // 'auth_key',
            // 'password_hash',
            // 'password_reset_token',
             'email:email',
             'Names',
            // 'PreferredName',
            // 'Surname',
            // 'Gender',
            // 'Birthday',
            // 'Website',
            // 'FacebookId',
            // 'TwitterId',
            // 'IsLikedFanPage',
            // 'PhotoUrl:url',
            // 'IsPersonal',
            // 'IdentityNumber',
            // 'TaxNumber',
            // 'TaxOffice',
            // 'Comment:ntext',
            // 'role',
            // 'status',
            // 'lastLogin',
            // 'previousLogin',
            // 'created_by',
            // 'LastUpdatedBy',
            // 'created_at',
            // 'updated_at',
            // 'deleted_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

