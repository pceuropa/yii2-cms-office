<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;

$menu_left = [];
if (!Yii::$app->user->isGuest) {
	$menu_left = [
		['label' => Yii::t('app', 'Pages'), 'url' => ['pages/index']],
		['label' => Yii::t('app', 'News'), 'url' => ['news/index']],
		['label' => Yii::t('app', 'Calendar'), 'url' => ['/calendar']],
		['label' => Yii::t('app', 'Gallery'), 'url' => ['gallery/index']],
		['label' => Yii::t('app', 'Files'), 'url' => ['files/index']],
		['label' => 'Form', 'items' => [
			['label' => Yii::t('app', 'Forms'), 'url' => ['forms/index']],
		]],
		['label' => Yii::t('app', 'Inne'), 'items' => [
			['label' => Yii::t('app', 'Menus'), 'url' => ['menu/creator']],
			['label' => Yii::t('app', 'Banners in sidebar'), 'url' => ['/banners-sidebar']],
			['label' => Yii::t('app', 'Banners in footer'), 'url' => ['/banners-footer']],
			['label' => Yii::t('app', 'Categories'), 'url' => ['categories/index']],
		]],
	];
}

if (\Yii::$app->user->can('admin')) {
    $menu_left[] = 
		['label' => Yii::t('app', 'Users'), 'items' => [
			['label' => Yii::t('app', 'Users'), 'url' => ['/user/index']],
			['label' => Yii::t('app', 'Assignment'), 'url' => ['/rbac/assignment']],
			['label' => Yii::t('app', 'Role'), 'url' => ['/rbac/role']],
			['label' => Yii::t('app', 'Permission'), 'url' => ['/rbac/permission']],
			['label' => Yii::t('app', 'Rule'), 'url' => ['/rbac/rule']],
		]];
}
	



$menu_right = Yii::$app->language == 'en' ? 
				[['label' => 'PL', 'url' => Url::current(['lang'=> 'pl'])]] : 
				[['label' => 'EN', 'url' => Url::current(['lang'=> 'en'])]];

$menu_right[] =  Yii::$app->user->isGuest ?	
				['label' => 'Login', 'url' => ['/site/login']] :	
	 			['label' => 'Logout (' . Yii::$app->user->identity->username . ')', 'url' => ['/site/logout'], 'linkOptions' => ['data-method' => 'post']];

NavBar::begin([ 'brandLabel' => '<span class="glyphicon glyphicon-home" aria-hidden="true"></span>',
                'brandUrl' => ['site/index'],
                'options' => ['class' => 'navbar-inverse navbar-fixed-top',],
            ]);
			
            echo Nav::widget(['options' => ['class' => 'navbar-nav navbar-left'], 'items' => $menu_left]);
			echo Nav::widget(['options' => ['class' => 'navbar-nav navbar-right'],'items' => $menu_right]);
NavBar::end();
?>
