<?php
use yii\helpers\Html;

$this->title = Yii::t('app', 'Create', ['modelClass' => 'Buttons',]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Buttons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [ 'model' => $model,]) ?>
