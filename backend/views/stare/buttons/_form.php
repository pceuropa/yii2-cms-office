<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\sortinput\SortableInput;
use yii\helpers\Url;
 ?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
	
	<?= $form->field($model, 'url')->textInput(['maxlength' => 255, 'value' => $model->url ? $model->url : 'http://']); ?>
	<?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
	<em>Plik nie jest wymagany. Jeżeli nie wstawisz zdjęcia, to w kolumnie prawej będzie tylko link tekstowy</em>
	<?= $form->field($model, 'file')->fileInput() ?>
<br/>
   
<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
 ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

<?php ActiveForm::end(); ?>
