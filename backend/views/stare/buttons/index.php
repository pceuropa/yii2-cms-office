<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\sortinput\SortableInput;
use yii\helpers\Url;
use frontend\widgets\Url2;

$this->title = Yii::t('app', 'Create', ['modelClass' => 'Buttons',]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Buttons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
$array = [];
foreach ($model->find()->orderBy(['serialize' => SORT_ASC, ])->all() as $element): 
$array[$element['id']] = 
	['content' => 
	 Html::img(Yii::$app->params[Yii::$app->language == 'pl' ? 'url_img' : 'url_img_en'].'buttons/'.$element['image'], ['class' => 'img-responsive', 'alt' => $element['name']])
	 
	.Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['update', 'id' => $element['id']]))
	.Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::to(['delete', 'id' => $element['id'],]), [ 'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'), 'data-method'=> 'post'])
	] ;
endforeach; ?>


<?php $form = ActiveForm::begin(); ?>

<div class="col-md-2">
	<?php 
	echo $form->field($model, 'serialize')->widget(SortableInput::classname(), [
	'items' => $array,
	'hideInput' => true,
	'options' => ['class'=>'form-control', 'readonly'=>true]
	]);
	?>
</div>
    <div class="form-group">
        <?= Html::submitButton('<span class="glyphicon glyphicon-sort-by-attributes-alt" aria-hidden="true"></span> '.Yii::t('app', 'Sort'), ['class' => 'btn btn-success']) ?>
		<?= Html::a('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
		'.Yii::t('app', 'Create', ['modelClass' => 'Buttons',]), ['create'], ['class' => 'btn btn-success']) ?>
    </div>
<?php ActiveForm::end(); ?>
