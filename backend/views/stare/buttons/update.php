<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Update', ['modelClass' => 'Buttons',]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Buttons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>


    <h3><?= Html::encode($this->title) ?></h3>
    <?= $this->render('_form', ['model' => $model,]) ?>

