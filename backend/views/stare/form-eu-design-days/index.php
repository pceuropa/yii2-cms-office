<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\FormEuDesignDaysSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Form Eu Design Days');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-eu-design-days-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Form Eu Design Days'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'firstname',
            'lastname',
            'birth',
            'id_number',
            'phone',
            // 'email:email',
            // 'email3:email',
            // 'name_org',
            // 'depart',
            // 'section',
            // 'adress_org',
            // 'room',
            // 'room_person_firstname',
            // 'room_person_lastname',
            // 'attention:ntext',
            // 'newsletter',
            // 'id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
