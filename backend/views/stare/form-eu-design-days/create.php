<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FormEuDesignDays */

$this->title = Yii::t('app', 'Create Form Eu Design Days');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Form Eu Design Days'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-eu-design-days-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
