<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\FormOpenDays2015Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Form');
$this->params['breadcrumbs'][] = $this->title;
?>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Form Open Days2015'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'firstname',
            'lastname',
            'birth',
            'id_number',
            'phone',
            'email:email',
             'email3:email',
             'name_org',
            // 'depart',
            // 'section',
             'adress_org',
             'room',
             'room_person_firstname',
             'room_person_lastname',
             'attention:ntext',
            // 'newsletter',
            // 'id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
