<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FormOpenDays2015 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Form Open Days2015',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Form Open Days2015s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="form-open-days2015-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
