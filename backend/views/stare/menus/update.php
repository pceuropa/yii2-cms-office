<?php

$this->title = Yii::t('app', 'Update {modelClass}: ', [ 'modelClass' => 'Menus', ]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Menu'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<?= $this->render('_form', [ 'model' => $model,]) ?>