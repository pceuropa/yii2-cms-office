<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\Menus;
use kartik\sortinput\SortableInput;

$this->title = Yii::t('app', 'Menu');
$this->params['breadcrumbs'][] = $this->title;
?>

<p>
<?= Html::a('<span class="glyphicon glyphicon-list" aria-hidden="true"></span> '.Yii::t('app', 'Create Group', ['modelClass' => 'Menus',]), ['create-group'], ['class' => 'btn btn-success']) ?> 
<?= Html::a('<span class="glyphicon glyphicon-indent-left" aria-hidden="true"></span> '.Yii::t('app', 'Create Link', ['modelClass' => 'Menus',]), ['create'], ['class' => 'btn btn-success']) ?> 
<?= Html::a('<span class="glyphicon glyphicon-minus" aria-hidden="true"></span> '.Yii::t('app', 'Create Line', ['modelClass' => 'Menus',]), ['create-line'], ['class' => 'btn btn-success']) ?> 
</p>

   
<div class="row">

<?php
$array = [];
$Menus = Menus::find()->orderBy(['gr' => SORT_ASC, 'serialize' => SORT_ASC, ])->all();

$nr = 1;
foreach ($Menus as $link): 

 if ($link->serialize == 0){
 
$array[$link->gr] = [$link->id_menu => ['content' => '<h5><i class="glyphicon glyphicon-list"></i> '.$nr.'. '.$link->name.
	Html::a('<span class="pull-right glyphicon glyphicon-trash"></span>', Url::to(['delete-group', 'id' => $link->id_menu]) ,[ 'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'), 'data-method'=> 'post']).
	Html::a('<span class="pull-right glyphicon glyphicon-pencil"></span>', Url::to(['update', 'id' => $link->id_menu])).'</h5>'
	, 'disabled'=> 'true']];

$nr++;

} else {
$array[$link->gr][$link->id_menu] = ['content' => Html::a($link->name, Url::to($link->url, true))  .
	Html::a('<span class="pull-right glyphicon glyphicon-trash"></span>', Url::to(['delete', 'id' => $link->id_menu]) ,[ 'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'), 'data-method'=> 'post']).
	Html::a('<span class="pull-right glyphicon glyphicon-pencil"></span>', Url::to(['update', 'id' => $link->id_menu]))
	
];}

endforeach;

$form = ActiveForm::begin(); 
$i = 0;
foreach ($array as $elements): 
 ?><div class="col-md-3"><?php


echo SortableInput::widget([
'name'=> 'menu_0[]',
    'items' => $elements,
    'hideInput' => true,
    'sortableOptions' => [
	'itemOptions'=>['class'=>'alert alert-info small padding_small'],
    'connected'=>true,
	
    ],
    'options' => ['class'=>'form-control', 'readonly'=>true]
    ]);

?></div><?php
if ($i == 3) {echo '</div><hr><div class="row">';} 
$i++;
endforeach; 


?>
</div>
<div class="row">
<?php echo Html::submitButton(Yii::t('app', 'Segregate'), ['class' => 'btn btn-success']);
   ActiveForm::end(); ?>

</div>