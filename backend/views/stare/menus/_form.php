<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\widgets\Select2;

use common\models\Menus;
use yii\helpers\ArrayHelper;
$groups=Menus::find()->where('serialize = 0')->all();
$listData=ArrayHelper::map($groups,'id_menu','name');


 ?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => 200]) ?>

<?= $form->field($model, 'url')->textInput(['maxlength' => 255]); ?>


<?php echo $form->field($model, 'gr')->widget(Select2::classname(), [
'data' => $listData,
'options' => ['placeholder' => 'Wybierz kategorię'],
'pluginOptions' => [
'allowClear' => true
],
]);?>



<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>


<?php ActiveForm::end(); ?>