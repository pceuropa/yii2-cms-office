<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\FormNiebieskiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Form Niebieski';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-niebieski-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Form Niebieski', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'firstname',
            'lastname',
            'birth',
            'id_number',
            'phone',
            'email:email',
            'email3:email',
            'name_org',
            'depart',
            'section',
            'adress_org',
            'room',
            'room_person_firstname',
            'room_person_lastname',
            'attention:ntext',
            // 'newsletter',
            // 'id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
