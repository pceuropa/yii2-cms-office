<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\FormNiebieskiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Form Energy';
$this->params['breadcrumbs'][] = $this->title;
?>

    <p>
        <?= Html::a('Create Form Energy', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'firstname',
            'lastname',
            'birth',
            'id_number',
            'phone',
            'email:email',
            'email3:email',
            'name_org',
            'depart',
            'section',
            'adress_org',
            'room',
            'room_person_firstname',
            'room_person_lastname',
            'attention:ntext',
            // 'newsletter',
            // 'id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


