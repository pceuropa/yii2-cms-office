<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\FormInicjatywyEu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-inicjatywy-eu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'firstname')->textInput(['maxlength' => 90]) ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => 90]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => 35]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 35]) ?>

    <?= $form->field($model, 'email3')->textInput(['maxlength' => 35]) ?>

    <?= $form->field($model, 'name_org')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'depart')->textInput(['maxlength' => 90]) ?>

    <?= $form->field($model, 'section')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'adress_org')->textInput(['maxlength' => 90]) ?>

    <?= $form->field($model, 'room')->textInput() ?>

    <?= $form->field($model, 'room_person_firstname')->textInput(['maxlength' => 35]) ?>

    <?= $form->field($model, 'room_person_lastname')->textInput(['maxlength' => 35]) ?>
 <?= $form->field($model, 'dinner')->textInput(['maxlength' => 1]) ?>
    <?= $form->field($model, 'attention')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
