<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\FormInicjatywyEu */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Form Inicjatywy Eu', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-inicjatywy-eu-view">

    <p>
        <?= Html::a('Aktualizuj', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Usuń', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'firstname',
            'lastname',
          //  'birth',
          //  'id_number',
            'phone',
            'email:email',
            'email3:email',
            'name_org',
            'depart',
            'section',
            'adress_org',
            'room',
            'room_person_firstname',
            'room_person_lastname',
			'dinner:boolean',
            'attention:ntext',
            //'newsletter',
            'id',
        ],
    ]) ?>

</div>
