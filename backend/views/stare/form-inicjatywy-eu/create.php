<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FormInicjatywyEu */

$this->title = 'Nowy Form Inicjatywy Eu';
$this->params['breadcrumbs'][] = ['label' => 'Form Inicjatywy Eu', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-inicjatywy-eu-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
