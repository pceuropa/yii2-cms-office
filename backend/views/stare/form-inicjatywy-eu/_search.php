<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\FormInicjatywyEuSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-inicjatywy-eu-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'firstname') ?>

    <?= $form->field($model, 'lastname') ?>

    <?= $form->field($model, 'birth') ?>

    <?= $form->field($model, 'id_number') ?>

    <?= $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'email3') ?>

    <?php // echo $form->field($model, 'name_org') ?>

    <?php // echo $form->field($model, 'depart') ?>

    <?php // echo $form->field($model, 'section') ?>

    <?php // echo $form->field($model, 'adress_org') ?>

    <?php // echo $form->field($model, 'room') ?>

    <?php // echo $form->field($model, 'room_person_firstname') ?>

    <?php // echo $form->field($model, 'room_person_lastname') ?>

    <?php // echo $form->field($model, 'attention') ?>

    <?php // echo $form->field($model, 'newsletter') ?>

    <?php // echo $form->field($model, 'id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
