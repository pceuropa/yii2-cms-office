<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FormNiebieski */

$this->title = 'Form Niebieski: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Niebieski wzrost w Regionie Morza Bałtyckiego', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Aktualizuj';
?>
<div class="form-niebieski-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
