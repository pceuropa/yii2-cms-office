<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = Yii::t('app', 'Update', ['modelClass' => 'Buttons',]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Buttons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>


    <h3><?= Html::encode($this->title) ?></h3>
    
    <div class="row">
		<div class="col-md-4">
			<?php
				if ($model->__isset('image')){
					echo Html::img(Url::to('@banners', 'http').$model->image, ['class' => 'clearfix img-responsive ']);
				}
			?>
		</div>
		<div class="col-md-8"><?= $this->render('_form', [ 'model' => $model]) ?></div>
		
		<?= Html::a ( Yii::t('app', 'Back'), ['index'], ['class' => 'pull-right btn btn-primary'] ); ?>
	</div>

<?php
	//$this->registerJsFile('/panelx/js/yii2-upload/update.js', ['position' => 3, 'depends' => 'yii\web\YiiAsset']);
?>


