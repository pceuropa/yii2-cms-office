<?php
	$this->title = Yii::t('app', 'Create', ['modelClass' => 'BannersFooter',]);
	$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'BannersFooter'), 'url' => ['index']];
	$this->params['breadcrumbs'][] = $this->title;
?>
	
<div class="row">
	<div id="baners_scrolls" class="col-md-4">
		<ul id="lista">
			<?= $model->listImages() ?>
		</ul>
	</div>
	<div class="col-md-8"><?= $this->render('_form', [ 'model' => $model]) ?></div>
</div>

<?php
	$this->registerJsFile('/panelx/js/Sortable.min.js', ['position' => 3, 'depends' => 'yii\web\YiiAsset']);
	$this->registerJsFile('/panelx/js/yii2-upload/index.js', ['position' => 3, 'depends' => 'yii\web\YiiAsset']);
?>
