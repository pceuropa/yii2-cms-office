<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FormOpenDays2015 */

$this->title = Yii::t('app', 'Create Form Open Days2015');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Form Open Days2015s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-open-days2015-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
