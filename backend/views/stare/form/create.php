<?php
use yii\helpers\Html;

$this->title = Yii::t('app', 'Form');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Form Eu Design Days'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-eu-design-days-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', ['model' => $model,]) ?>

</div>