<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
/* @var $this yii\web\View */
/* @var $model common\models\Albums */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="row">
	<div class="col-md-6"><?= $form->field($model, 'name')->textInput() ?></div>
	<div class="col-md-4"><?php echo $form->field($model, 'timestamp')->widget(DatePicker::classname(), [
		'options' => ['placeholder' => ''],
		'type' => DatePicker::TYPE_COMPONENT_APPEND,
		'pluginOptions' => [
		'language' => 'pl',
		'autoclose'=>true,
		'format' => 'yyyy-mm-dd'
		]
		]);?>
</div>
</div>
<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update '), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

    <?php ActiveForm::end(); ?>
