<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use frontend\widgets\Url2;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Albums'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h2><?= Html::decode($this->title) ?> <small><?= Yii::$app->formatter->asDate($model->timestamp, 'dd-MM-yyyy')  ?></small></h3>
<hr>
		<div class="row">
			<div class="col-md-6">
				<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);?>

					<?= $form->field($photos, 'file[]')->widget(kartik\widgets\FileInput::classname(), [
						'options' => ['accept' => 'image/*',
						'multiple' => true],
							'pluginOptions' => [
							'browseLabel' => 'Dodaj zdjęcia',
							'showPreview' => false,
							'showCaption' => true,
							'showRemove' => true,
							'showUpload' => true
						]
					])->label(false);?>

				<?php ActiveForm::end(); ?>
			</div>

			<div class="col-md-6">
				<?= Html::a('Zmień nazwę lub datę', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

					<?= Html::a(Yii::t('app', 'Usuń album'), ['delete', 'id' => $model->id], [
						'class' => 'btn btn-danger',
						'data' => [
						    'confirm' => 'Jesteś pewna?',
						    'method' => 'post',
						],
					]) ?>
			</div>
		</div>
		<div class="row">
			<?php foreach ($img as $photo): 
				$p = Url2::img("galeria/max") . $photo['photo'];
	
				echo '<div class="col-md-3">';
				echo Html::a(Html::img($p, ['class' => 'img-thumbnail', ]), $p, ['data-lightbox' => 'album']);
	
				echo Html::a('<span class="glyphicon glyphicon-trash"></span>', 
							['delete-photo', 'id' => $photo['id'], 'album' => $photo['id_album']], 
							[ 'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'), 'data-method'=> 'post']
						);
				echo '</div>';
			endforeach; ?>
		</div>
