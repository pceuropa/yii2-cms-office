<?php

use yii\helpers\Html;
use yii\grid\GridView;
$this->title = Yii::t('app', 'Galleries');
$this->params['breadcrumbs'][] = $this->title;
?>
<?= Html::a('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> '.Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-success']) ?>

<?= GridView::widget([
'dataProvider' => $dataProvider,
'filterModel' => $searchModel,
'columns' => [
['class' => 'yii\grid\SerialColumn'],
'timestamp',
'name:text:Tytuł',
'id',


['class' => 'yii\grid\ActionColumn'],
],
]); ?>
