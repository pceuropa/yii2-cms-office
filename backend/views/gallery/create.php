<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Albums */

$this->title = 'Dodaj galerię';
$this->params['breadcrumbs'][] = ['label' => 'Albums', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', ['model' => $model,]) ?>

