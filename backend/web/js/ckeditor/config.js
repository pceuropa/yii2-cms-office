﻿/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	
	CKEDITOR.on('dialogDefinition', function (ev) {
        // Take the dialog name and its definition from the event data.
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;
        // Check if the definition is from the dialog we're
        // interested in (the 'image' dialog).
        if (dialogName == 'image') {
            // Get a reference to the 'Image Info' tab.
            var infoTab = dialogDefinition.getContents('info');
            // Remove unnecessary widgets/elements from the 'Image Info' tab.
           // infoTab.remove('browse');
            infoTab.remove('txtHSpace');
            infoTab.remove('txtVSpace');
            infoTab.remove('txtBorder');
           // infoTab.remove('txtAlt');
            infoTab.remove('txtWidth');
            infoTab.remove('txtHeight');
            infoTab.remove('htmlPreview');
           // infoTab.remove('cmbAlign');
            infoTab.remove('ratioLock');
        }
    });
	
config.enterMode = CKEDITOR.ENTER_BR;
config.shiftEnterMode = CKEDITOR.ENTER_P;
config.autoParagraph = false;
config.skin = 'bootstrapck';
config.contentsCss = '/css/mysitestyles.css';
config.language = 'pl';
config.height = 400;
config.filebrowserBrowseUrl = '/panelx/js/ckeditor/kcfinder/browse.php';
config.filebrowserUploadUrl = '/panelx/js/ckeditor/kcfinder/upload.php';
config.entities = false;
config.entities_latin = true;
config.plugins = 'dialogui,dialog,dialogadvtab,basicstyles,bidi,button,panelbutton,panel,floatpanel,colorbutton,colordialog,menu,contextmenu,toolbar,enterkey,entities,popup,filebrowser,fakeobjects,floatingspace,listblock,richcombo,font,format,horizontalrule,htmlwriter,wysiwygarea,image,indent,indentblock,indentlist,justify,menubutton,link,list,liststyle,magicline,removeformat,showborders,sourcearea,tab,table,tabletools,undo,lineutils,widget,widgetbootstrap';
config.skin = 'bootstrapck';
config.toolbar = [
		{ name: 'document', items: ['Undo', 'Redo'] },
		{ name: 'basicstyles', items: [  'Bold', 'Italic', 'Underline', 'Strike','TextColor', 'BGColor', 'RemoveFormat'] },
		{ name: 'links', items: ['Image','Link', 'Anchor'] },
		{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock',  ] },
		{ name: 'insert', items: [ 'Glyphicons', 'WidgetbootstrapLeftCol', 'WidgetbootstrapRightCol', 'WidgetbootstrapTwoCol', 'WidgetbootstrapThreeCol', 'Table', 'HorizontalRule',  ] },
		{ name: 'styles', items: [ 'Styles', 'Format',  'FontSize' ] },
		{ name: 'tools', items : [ 'Source' ] }
	];
};
