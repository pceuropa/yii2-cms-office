var MyFORM =  MyFORM || {};
MyFORM.test = (function(){


var form = MyFORM.events.form, fields = {
    "title": "Title-test",
    "action": "action-test",
    "method": "get",
    "language": "Polish-test",
	"class": 'class',
    "body": [
        [
            {
                "field": "input",
                "type": "text",
                "label": "Label input",
                "name": "input1",
                "placeholder": "place",
                "helpBlock": "desc input text",
                "width": "col-md-6",
                "require": true,
                "value": "value1",
                "id": "id",
                "class": "class"
            },
			{
                "field": "textarea",
                "label": "Label textarea",
                "name": "textarea2",
				"value": "value2",
                "placeholder": "place",
                "helpBlock": "desc textarea",
				"rows": 3,
                "width": "col-md-6",
                "require": true,
                "id": "id",
                "class": "class"
            } // delete in test after

        ],
        [
            {
                "field": "radio",
                "label": "Label Radio",
                "helpBlock": "desc radio",
				"name": "radio3",
                "width": "col-md-6",
				"require": true,
				"id": "id",
                "class": "class",
                "elements": [
                    {"text": "radio1", "value": "radio-value1", "id": "", "checked": true},
                    {"text": "radio2", "value": "radio-value1", "class": "", "checked": false}
                ]
            },
            {
                "field": "checkbox",
                "label": "Label Checkbox",
                "helpBlock": "desc checkbox",
                "name": "checkbox4",
                "width": "col-md-6",
				"id": "id",
                "class": "class",
                "elements": [
                    {"text": "checkbox1", "value": "checkbox-value1", "id": "", "checked": true},
                    {"text": "checkbox2", "value": "checkbox-value1", "class": "", "checked": false}
                ]
            }
        ],
		[
            {
                "field": "select",
                "label": "select label",
                "helpBlock": "desc checkbox",
                "width": "col-md-6",
				"require": true,
				"id": "",
                "class": "",
                "elements": [
                    {"text": "select1", "value": "select-value1", "id": "", "checked": true},
                    {"text": "select2", "value": "select-value1", "class": "", "checked": false},
                    {"text": "select2", "value": "select-value1", "class": "", "checked": false},
                    {"text": "select2", "value": "select-value1", "class": "", "checked": false},
                    {"text": "select2", "value": "select-value1", "class": "", "checked": false},
                    {"text": "select2", "value": "select-value1", "class": "", "checked": false},
                    {"text": "select2", "value": "select-value1", "class": "", "checked": false},
                ]
            },
            {
                "field": "description",
				"text" : "<h1> Lorem Ipsum </h1>jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. <br /> Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym.",	
				"width": "col-md-12",
				"class": "class",
				"id": "id"
                
            }
        ],
		[
            {
                "field": "input",
                "type": "text",
                "width": "col-md-6",
            },
			

        ],
		[{
                "field": "submit",
                "label": "Submit",
                "width": "col-md-6",
            },
		]
    ]
}
if(false){
	form.generate(fields);
	form.deleteField(0, 1)
	form.body = [];
	form.generate(fields)
	form.add(fields.body[0][0]);
	form.add(fields.body[2][1]);
}



/**
1. Delete 1,2
2. Move   3,4
4 Delete All
5 Add One
6 Add All

// filter: puste, nie pasujace do typu, 

**/
return null

})();



