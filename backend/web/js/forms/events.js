"use strict";


// sprawdzic funkcje add (czyste zmienne bez referencji)
// ustawić brak dziedziczenia field

var MyFORM =  MyFORM || {};
MyFORM.events = (function(){

	var form = new MyFORM.Form(), 
		field = {}, act = {},

		Form = $("#preview-form"),
		Field = $("#preview-field"),

		sidebarDivsOptions = $("#sidebar div.options"),
			optionsForm = $("#options-form"),
		
		tabOptions = $("#tabs-options"),
			selectField = $("select#select-field"),
			tabForm = 	  $("#tab-form"),
			tabField = 	  $("#tab-field"),
			tabUpdate =   $("#tab-update"),
			tabDelete =   $("#tab-delete"),
		
		updateDiv = $("#update"),
		deleteDiv = $("#delete"),
		save = $("#save");


tabOptions.on("click", "#tab-form", function (){
	console.log('form-btn');
	actionForm(); 
});

tabOptions.on("click", "#tab-field", function (){
	console.log('field-btn');
	actionField();
});


tabForm.click();

$("#view-mode").change(function (){
		form.setView(this.value);
		form.render();
}).change();

$("#save-form").click(function(){form.save()});

sidebarDivsOptions.on("click", "#add-to-form", function (){
	form.add(field.body);
	tabField.click();
	});


Form.delegate(".edit", "click", function(e){edit(e)});
Form.delegate(".clone", "click", function(e){clone(e)});
Form.delegate(".delete", "click", function(e){del(e)});
			
			

function actionForm(){

			tabForm.addClass('act');
			tabField.removeClass("act");

			sidebarDivsOptions.hide();
			Field.hide();
			tabUpdate.hide();
			tabDelete.hide();
			selectField.hide();

			optionsForm.show().find('span').find('input, select').on( "keyup change", function() {
				form[this.id] = this.value;
				form.render();
			});
		};



	function actionField() { // FIELD TAB
		tabField.addClass("act");
		tabForm.removeClass("act");
		optionsForm.hide();

		tabUpdate.hide();
		tabDelete.hide();

		function toggle() {
			$("#edit1, #edit2").toggle();
		};
	

		selectField.show().unbind().change(function () {

			var v = this.value, fieldIdSelector = $('#' + v), el = {}, id;

			sidebarDivsOptions.hide();
			Field.show();
			fieldIdSelector.show().find('span').find('input, select, textarea').on("keyup change", 
				function() {
					field.body[this.id] = (this.type === 'checkbox') ? this.checked : this.value;
					field.render();
				}
			);

			field = new MyFORM.field.Field({field: v});
			field.uiHelper();
			field.render();
		
		$("button.add").unbind().click(function(){
			field.addItem()
		});

			

		fieldIdSelector.on('change', '#selectFieldToUpdate', function() {
	
			var s, id = this.value, el = field.elements[id];

			function update() {
				el[this.id] = (this.type === 'checkbox') ? this.checked : this.value;
				field.render();
			};
			toggle();

			for (var prop in el) {
				s = $('#'+ v +' #'+ prop);
				if(typeof el[prop] === 'string'){
					s.val(el[prop]);
				}
				if(typeof el[prop] === 'boolean'){
					s.prop('checked', el[prop]);
				}
			};

			fieldIdSelector.find('.well input').on("keyup change", update);

		});

		fieldIdSelector.on('click', '.cancel', function() {
				toggle();
				field.render();
			}).on('click', '.clone', function() {
				field.cloneItem(id);
				toggle();
			}).on('click', '.delete', function() {
				field.deleteItem(id);
				toggle();
			});

		}).change();

	};

function edit(e){
		var map = e.target.dataset, o = form.body[map.row][map.index], el = [], id = 0; 

			form.setFieldType(o.field);
			function update(){
				o[this.id] = (this.type === 'checkbox') ? this.checked : this.value;
				form.render();
			};

			function toggle() {
				o.selectCreate();
			};

			function updateElement() {
				el[this.id] = (this.type === 'checkbox') ? this.checked : this.value;
				form.render();
			};

			function add() {
				o.elements.push(form.sourceElementToForm());
				o.selectCreate();
				form.render();
			}

			Field.hide();
			tabUpdate.addClass("act");
			tabField.removeClass("act");
			tabForm.removeClass("act");

			tabUpdate.show();
			sidebarDivsOptions.hide();
			updateDiv.show();
			selectField.hide();
			updateDiv.html($("#"+ o.field).html());

			$('button#save').hide();
			$("#update span").find('input, textarea, select').on("keyup change", update);


			for (var prop in o) {
				if (o.hasOwnProperty(prop) && prop !== 'field'){

					if(typeof o[prop] === 'string'){
						$('#update #'+ prop).val(o[prop]);
					}

					if(typeof o[prop] === 'boolean'){
						$('#update #'+ prop).prop('checked', o[prop]);
					}
				} 
			}


				if (o.hasOwnProperty('elements')){
					o.selectCreate();
					updateDiv.unbind().on('click', 'button.add', add);
					updateDiv.on('change', '#selectFieldToUpdate', function() {

						id = this.value;
						el = o.elements[id];
						updateDiv.find("#edit1").html(form.buttons);

						for (var prop in el) {
							if(typeof el[prop] === 'string'){
								$('#update #'+ prop).val(el[prop]);
							}
							if(typeof el[prop] === 'boolean'){
								$('#update #'+ prop).prop('checked', el[prop]);
							}
						};

					$("#update .well input").on("keyup change", updateElement);

					$(".cancel").click(function(){
							$("#update #edit1").html(form.button);
							$("#update .well input").unbind();
							o.selectCreate();
						})

					$(".clone").click(function(){
							form.cloneElement(map.row, map.index, id);
							$("#update #edit1").html(form.button);
							$("#update .well input").unbind();
							o.selectCreate();
						})

					$(".delete").click(function(){
						console.log('text');
						
							form.deleteElement(map.row, map.index, id);
							$("#update #edit1").html(form.button);
							$("#update .well input").unbind();
							o.selectCreate();
						});
					});
				}
	}
	
	function clone(e){
		var map = e.target.dataset;	
			form.cloneField(map.row, map.index);
	}

	function del(e){
		var map = e.target.dataset,
			field = form.body[map.row][map.index];

			Field.hide();

			tabDelete.addClass("act");
			tabUpdate.removeClass("act");
			tabField.removeClass("act");
			Form.removeClass("act");

			selectField.hide();
			sidebarDivsOptions.hide();

			tabDelete.show();
			deleteDiv.show();

		$("button#btn-delete-confirm").click(function () {
			form.deleteField(map.row, map.index);
			form.render();
			tabField.click();
		});	

		$("button#btn-cancel").click(function () {
			tabField.click();
		});
	}
		

 // end act class




return { 
		form: form,
	}

})();
