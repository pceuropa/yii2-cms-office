"use strict";

var h = {
		clearObj: function(o){
		    for (var i in o) {
		        if (o[i] === null || o[i] === undefined) {
		            delete o[i];
		        }
		    }
		    return o;
		},
		clone: function (o) {
			var clone = {};

			for (var prop in o) {
				if (clone[prop]){
					if(Array.isArray(o[prop])){
						for (var i=0; i < o[prop].length; i++){
							clone[prop][i] = o[prop][i];
						}
					} else {
						clone[prop] = o[prop];
					}
				}
		    }
			return clone;
		},
		getName: function() { 
   			var funcNameRegex = /function (.{1,})\(/;
   			var results = (funcNameRegex).exec((this).constructor.toString());
   		return (results && results.length > 1) ? results[1] : "";
		},
		capitalizeFirstLetter: function (s) {
    		return s.charAt(0).toUpperCase() + s.slice(1);
		}


	
};

String.prototype.replaceAll = function(search, replacement) {
    return this.replace(new RegExp(search, 'g'), replacement);
};
