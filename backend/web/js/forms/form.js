"use strict";


var MyFORM =  MyFORM || {};
MyFORM = (function(){

	var hello = 'hello',
		version = '3.1',
	
	Form = function (){
		this.title =  "FormBuilder";
		this.action = "";
		this.method = "post";
		this.language = "English"; 
		this.body = [];
		this.get();
	};


	Form.prototype = {
		// filter
		// get
		// add
		// save
		// modyfications items of form
		// view Forms


		constructor: Form,
		viewMode: 'html',
		fieldType: 'options-form', // select, textarea
		map: { index: "0", row: "0" },
		config: {get: true, save: true, autosave: false},

	

	filter: function (o){
		var clear = {};
		for (var prop in o){
				if (o[prop]){
					if(Array.isArray(o[prop])){
							clear[prop] = [];
							for (var i = o[prop].length; i--;){
            					clear[prop][i] = o[prop][i];
								
							}
							o[prop] = [];
					} else {
						if(prop !== 'view'){
							clear[prop] = o[prop];
						}
					}
				}
		    }
		return clear;
	},

	get: function (){
			var form = this;
			if(this.config.get){
				$.getJSON(document.URL, function(r){
					console.log('upload from base correct');
					form.generate(r)
				});
			}
		},
	add: function (o){
		try {
			if (o.hasOwnProperty('field')){
				this.body.push([o]);
				this.render();
			} 	
		}
		catch (err) {
			console.log(err);
			alert('Error add field to form: bad data')
		}
		
		
	},

	save: function (){
			var form = this, csrfToken = $('meta[name="csrf-token"]').attr("content");

			$.ajax({
				url: document.URL,
				type: 'post',
				dataType:'JSON',
				data: { update: true, _csrf : csrfToken, body: JSON.stringify(form) },
				dataType: 'json',		
				success: function (r){
						if (r.success === false){ console.log(r); }
						if (r.success === true){ 
							console.log('save from base correct');
							form.operations++;
								
							if (r.url){ window.location.href = r.url;}
						}
				},
				error: function (XMLHttpRequest, textStatus, errorThrown){
					alert(textStatus);
				}
			});
		},


// modyfication items of Form

	cloneField: function(row, index){
			var o = jQuery.extend(true, {}, this.body[row][index]); ;
			this.body[row].splice(index, 0, o); // wstawiamy obiekt na odpowiednie miejsce bez usuwania
			this.render();
		},
	cloneItem: function(row, index, id){
				var clone = {}, element = this.body[row][index]['elements'][id];
					for (var prop in element){
						if (element.hasOwnProperty(prop)){
				        	clone[prop] = element[prop];
						}
					}
			this.body[row][index].elements.splice(id, 0, clone);
			this.render();
		},

	deleteField: function(row, index){
			try {
				if(this.body[row].splice(index, 1)){
					console.log('Delete item [', row , index, ']');
					this.render();
				}
			}
			catch(err){
				console.log('Item [', row , index, '] not exsist');
				
			}
		},
	
	deleteItem: function (row, index, id){
			this.body[row][index]['elements'].splice(id, 1);
			this.render();
		},

	generate: function(o){
 	
			this.title = o.title || '';;
			this.action = o.action || '';
			this.method = o.method || '';
			this.id = o.id || '';
			this.body['class'] = o['class'] || '';
			this.body = o.body || {};
		    if(this.body.length > 0){
		    	this.render();
		    }
		},



// <---- View

	attr: function(){
			var i, temp = '', max = arguments.length;
	
			for (var i = 0;  i < max; i += 1){
				temp += this[arguments[i] + 'Attr']();
			}
			return temp;
		},

	idAttr: function (){
			return this.id ? ' id="'+ this.id + '"' : '';
		},
	classAttr: function (){
			return this['class'] ? ' class=" '+ this['class'] + '"' : ' class=""';
		},
	
	// rows -> row - > fields - > field - > render

	rows: function(){
		    var rows = '';
			if(this.body.length == 0) return rows;
		    for (var i = 0, max = this.body.length; i < max; i += 1){
		        rows += this.row(i);
		    }
	    return rows;
		},

	row: function(id){
		    return '\n<div id="row'+ id +'" class="row">\n'+ this.fields(id) +' \n</div>\n';
		}, 
	fields: function(id){
		    var fields = '';
		    for (var i = 0, max = this.body[id].length; i < max; i++){
		        fields += this.field(id, i);
		    }
    	return fields;
		},

	field: function(row, index){
			var f = this.body[row][index],
			field = new MyFORM.field.Field(f);
			
			// test ----- console.log(row, index, f.field, Array(20 - f.field.length).join("-"), field);

			if(Form.prototype.viewMode === 'html'){
				
				field.divEnd = function (){
					return Form.prototype.edit(row, index) +  '\n</div>\n';
				}
			}
			
		return field.html();
		},
	render: function(){
			var divForm = $("#preview-form");
			$("h1").text(this.title);
			
			switch(Form.prototype.viewMode){
			    case 'text': divForm.html('<pre><code> </code></pre>').find('code').text(this.view('html')); break;
				case 'json': divForm.html('<pre><code> </code></pre>').find('code').text(this.view('json')); break;
				case 'yii2': divForm.html('<pre><code> </code></pre>').find('code').text('Yii2'); break;
			    default: 	 divForm.html(this.view('html')); this.sort(this);
		        break;
			}
	},

	view: function (mode){
			 switch(mode){
				case 'html': 
					return '<form  action="' + this.action + '" method="' + this.method + '" ' + this.attr('id', 'class') + '>' +
							this.rows() + '\n</form>';; break;
				case 'json': return JSON.stringify(this, null, 4); break;
				case 'h1': return this.title; break;
				default: null;
			} 

		},

		
	setEdit: function (row, index){
			Form.prototype.beforeEndDiv = this.edit(row, index);
		},

	setView: function(view){
			Form.prototype.viewMode = view || '';
		},
	setFieldType: function(field){
			Form.prototype.fieldType = field || '';
		},

	edit: function(row, index){
			function glyphicon(row, index, cl){
				return '<span class="glyphicon ' + cl + '" data-row="' + row + '" data-index="' + index + '" aria-hidden="true"></span>';
			};

			if(Form.prototype.viewMode === 'html'){
				return 	'<div class="edit-field pull-right">' + 
					glyphicon(row, index, 'edit glyphicon-pencil') +
					glyphicon(row, index, 'clone glyphicon-duplicate') +
					glyphicon(row, index, 'delete glyphicon-trash') + '</div>';
			}
			
			return null;
		},
	
	sort: function(){

			var form = this,
				rowId = function (row){
		    			return row.substring(3); // row1 || row11 = 1 || 11
				};
			for (var i = this.body.length; i--;){
				var id = document.getElementById('row' + i);

				Sortable.create(id, {
					group: "row",
					animation: 0,
					ghostClass: "ghost",

					onAdd: function (e){
						var row = rowId(e.from.id),
							newRow = rowId(e.target.id),
							index = e.oldIndex,
							newIndex = e.newIndex,
							objToMove = form.body[row].splice(index, 1)[0]; // pobieramy obiekt ze starej lokalizacji i usuwamy

						form.body[newRow].splice(newIndex, 0, objToMove);	// wstawiamy obiekt na odpowiednie miejsce
						form.render();
					},

					onUpdate: function (e){
						var row = rowId(e.from.id),
							index = e.oldIndex,
							newIndex = e.newIndex;

						form.body[row].splice(newIndex, 0, form.body[row].splice(index, 1)[0]);
						form.render();
					}
				});
			}
		}
	};

return {
		version: version,
		Form: Form,
	}

})();
