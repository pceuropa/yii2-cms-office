"use strict";
function renderList(o) {
	var text= '', img ='', item = o.list;
	
	for (var i = 0; i < item.length; i++) {
	    
	    if(typeof item[i].image == "string"){
	    	img = '<img src="' + o.url.upload_url +  item[i].image + '" alt="' + item[i].name + '" title="' + item[i].name + '" />';
	    } else {
	    	img = "<a href='" + item[i].url + "' >" + item[i].name + "</a>"
	    }
	    
	    
		text +='<li><div class="row">' +
				'<div class="col-md-9">' + img + '</div>' +
				'<div class="col-md-1 edit-icon">' +
					'<a href="'+ o.url.update +"?id=" + item[i].id +'" id="'+ item[i].id +'"><span class="pull-right glyphicon glyphicon-pencil"></span></a><br/>' +
					'<a href="'+ o.url.delete +"?id=" + item[i].id +'" id="'+ item[i].id +'" data-confirm="confirm" data-method="post"><span class="glyphicon glyphicon-trash"></span></a>' +
				'</div>' +
				'<div class="col-md-1">' +
					'<span class="glyphicon glyphicon-resize-vertical big" aria-hidden="true"></span>' +
			//		'<a href="#" id="'+ item[i].id +'"><span class="pull-right glyphicon glyphicon-pencil"></span></a>' +
			//		'<a href="#" id="'+ item[i].id +'"><span class="pull-right glyphicon glyphicon-trash"></span></a>' +
				'</div>' +
			'</div></li>' 
	}
	$("#lista").html(text)
}

function getList() {
	$.ajax({
	  url: document.URL,
	  type: 'post',
	  dataType:'JSON',
	  data: {get: true },
	  success: function (r) {
				if (r.success === false) {console.log(r.message);}
				if (r.success === true) {
					console.log(r);
					renderList(r);
				}
			},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			alert(textStatus);
		}
	}); 
}
        
        
var config = {
	group: "nav",
	animation: 0,
	ghostClass: "ghost",

	onUpdate: function (evt) {
		var a = [];
	
		$("#lista li").each(function (index) {
       		a.push($(this)[0].id)
        }) 
	
	$.ajax({
	  url: document.URL,
	  type: 'post',
	  dataType:'JSON',
	  data: { serialize: true, array: a },
	  success: function (r) {
				if (r.success === false) {console.log(r.message);}
			},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			alert(textStatus);
		}
	}); 
	
	
	},
}  // end config


Sortable.create(lista, config);



