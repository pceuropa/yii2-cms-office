<?php
namespace backend\controllers;

use Yii;
use common\models\FormTechMed;
use common\models\FormTechMedSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class FormTechMedController extends Controller
{
public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


public function actionIndex()
    {
        $nameForm = 'Technologie medyczne w Horyzoncie 2020 oraz AAL';
		
		$searchModel = new FormTechMedSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		
        return $this->render('@backend/views/form/index', [
            'nameForm' => $nameForm,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

public function actionView($id)
    {
        return $this->render('@backend/views/form/view', ['model' => $this->findModel($id),]);
    }


public function actionCreate()
    {
        $model = new FormTechMed();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['@backend/views/form/view', 'id' => $model->id]);
        } else {
            return $this->render('@backend/views/form/create', [
                'model' => $model,
            ]);
        }
    }

public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['@backend/views/form/view', 'id' => $model->id]);
        } else {
            return $this->render('@backend/views/form/update', [
                'model' => $model,
            ]);
        }
    }

public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = FormTechMed::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
