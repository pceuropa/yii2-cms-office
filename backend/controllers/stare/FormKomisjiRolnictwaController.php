<?php
namespace backend\controllers;

use Yii;
use common\models\FormKomisjiRolnictwa;
use common\models\FormKomisjiRolnictwaSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class FormKomisjiRolnictwaController extends Controller
{
public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


public function actionIndex()
    {
        $nameForm = 'Form';
		
		$searchModel = new FormKomisjiRolnictwaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		
        return $this->render('@backend/views/form-room/index', [
            'nameForm' => $nameForm,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

public function actionView($id)
    {
        return $this->render('@backend/views/form-room/view', ['model' => $this->findModel($id),]);
    }


public function actionCreate()
    {
        $model = new FormKomisjiRolnictwa();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['@backend/views/form-room/view', 'id' => $model->id]);
        } else {
            return $this->render('@backend/views/form-room/create', [
                'model' => $model,
            ]);
        }
    }

public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['@backend/views/form-room/view', 'id' => $model->id]);
        } else {
            return $this->render('@backend/views/form-room/update', [
                'model' => $model,
            ]);
        }
    }

public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = FormKomisjiRolnictwa::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
