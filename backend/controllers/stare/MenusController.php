<?php

namespace backend\controllers;

use Yii;
use common\models\Menus;
use common\models\MenusSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query ;


class MenusController extends Controller
{
public $enableCsrfValidation = false;
public function behaviors()
{
return [
'verbs' => [
'class' => VerbFilter::className(),
'actions' => [
'delete' => ['post'],
],
],
];
}


public function actionIndex()
{
	$model = new Menus();

if (Yii::$app->request->post()) {


$array_menu = Yii::$app->request->post('menu_0');


foreach ($array_menu as $elements): 

	$array = explode(',', $elements );

	$i = 0;
	foreach ($array as $elementy){
	
	$model = $this->findModel($elementy);
	echo $model->serialize = $i;
	echo $model->gr = $array[0];
	
	if ($model->save()) {echo 'zapisalo';} else {print_r($model->getErrors()); die;}
	$i++;
	
	}

endforeach;

	return $this->redirect(['index']);
	} else {
	return $this->render('index', ['model' => $model,]);
	}
}



public function actionCreate()
{
$model = new Menus();

if ($model->load(Yii::$app->request->post()) && $model->save()) {

return $this->redirect('index');
} else {
return $this->render('create', [ 'model' => $model, ]);
}
}

public function actionCreateLine()
{
$model = new Menus();
$group = Menus::find()->one();
$model->gr = $group->gr;
$model->name = '--------------';
$model->url = '#';
if (!$model->save()) {print_r($model->getErrors());die;}
return $this->redirect('index');
}

public function actionCreateGroup()
{
$model = new Menus();
$model->scenario = 'group';

if ($model->load(Yii::$app->request->post()) ) {

$model->serialize = 0;
	if ($model->save())
	{
	$model->gr = $model->id_menu;
	$model->save();
	} else {
			print_r($model->getErrors());die;
			}

return $this->redirect('index');
	} else {
return $this->render('create-group', [ 'model' => $model, ]);
}
}


public function actionUpdate($id)
{
	$model = $this->findModel($id);

	if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['menus/index']);
	} else {
			return $this->render('update', [ 'model' => $model, ]);
	}
}


public function actionDelete($id)
{
	$this->findModel($id)->delete();

return $this->redirect(['index']);
}

public function actionDeleteGroup($id)
{
	$this->findModel($id)->delete();
	Menus::deleteAll('gr = '.$id);

return $this->redirect(['index']);
}


protected function findModel($id)
{
if (($model = Menus::findOne($id)) !== null) {
return $model;
} else {
throw new NotFoundHttpException('The requested page does not exist.');
}
}
}
