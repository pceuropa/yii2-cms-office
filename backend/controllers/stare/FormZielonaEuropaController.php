<?php
namespace backend\controllers;

use Yii;
use common\models\FormZielonaEuropa;
use common\models\FormZielonaEuropaSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class FormZielonaEuropaController extends Controller
{
public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


public function actionIndex()
    {
        $nameForm = 'Form';
		
		$searchModel = new FormZielonaEuropaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		
        return $this->render('@backend/views/form-zielona-europa/index', [
            'nameForm' => $nameForm,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

public function actionView($id)
    {
        return $this->render('@backend/views/form-zielona-europa/view', ['model' => $this->findModel($id),]);
    }


public function actionCreate()
    {
        $model = new FormZielonaEuropa();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['@backend/views/form-zielona-europa/view', 'id' => $model->id]);
        } else {
            return $this->render('@backend/views/form-zielona-europa/create', [
                'model' => $model,
            ]);
        }
    }

public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['@backend/views/form-zielona-europa/view', 'id' => $model->id]);
        } else {
            return $this->render('@backend/views/form-zielona-europa/update', [
                'model' => $model,
            ]);
        }
    }

public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = FormZielonaEuropa::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
