<?php
namespace backend\controllers;

use Yii;
use common\models\BannersFooter;
use yii\web\UploadedFile;
use yii\helpers\Url;

class BannersFooterController extends \yii\web\Controller{

public $enableCsrfValidation = false;

public function behaviors(){
	return [
	'verbs' => [
	'class' => \yii\filters\VerbFilter::className(),
	'actions' => [
	'delete' => ['post'],
			],
		],
	];
}
   
        
public function actionIndex(){
    
	$model = new BannersFooter;
	$request = Yii::$app->request;
	
	if ($request->isAjax){
	
		$array = $request->post('array');
		
		foreach ($array as $key => $val){
	
			$m = $this->findModel($val);
			$m->serialize = $key;
			$m->save();
		}
		//$serial = $model::findAll()->orderBy('serialize')->asArray();
		
		return  \yii\helpers\Json::encode(['success' => true,]);
	}
	
	
	
	if ($model->load($request->post())) {
		//	$upload_handler = new UploadHandler(['upload_dir' => Url::to('@img_path/banners/'),'upload_url' => Url::to('@banners', 'http'),]);
			$file  = UploadedFile::getInstance($model, 'files');
		
		if ($file){
			$file_name = $file->name;
			$file_path = Url::to('@img_path/banners/') . $file_name;
			$file_url = Url::to('@banners', 'http') . $file_name;
			
			if ($file->saveAs($file_path)){
				$model->image = $file_name;
			}
			
		} 	
		
		$model->save();
			
				
				
		return $this->render('index', ['model' => $model]);
	} else {
		return $this->render('index', ['model' => $model]);
	}

}


public function actionUpdate($id){

	$model = $this->findModel($id);
	$request = Yii::$app->request;
	
	
	if ($model->load($request->post())) {
		//	$upload_handler = new UploadHandler(['upload_dir' => Url::to('@img_path/banners/'),'upload_url' => Url::to('@banners', 'http'),]);
		$file  = UploadedFile::getInstance($model, 'files');
		
		if ($file){
			$file_name = $file->name;
			$file_path = Url::to('@img_path/banners/') . $file_name;
			$file_url = Url::to('@banners', 'http') . $file_name;
			if ($file->saveAs($file_path)){
				$model->image = $file_name;
			}
		}
			
		$model->save();
			
		return $this->redirect(['index']);
	} else {
		return $this->render('update', ['model' => $model,]);
	}
}

public function actionDelete($id){
	$this->findModel($id)->delete();

	return $this->redirect(['index']);
}


protected function findModel($id){
	if (($model = BannersFooter::findOne($id)) !== null) {
	return $model;
	} else {
	throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
	}
}


}
