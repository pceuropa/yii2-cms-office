<?php

namespace backend\controllers;

use Yii;
use common\models\Buttons;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\db\IntegrityException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\BaseInflector;
use yii\helpers\Url;


class ButtonsController extends Controller
{
public $enableCsrfValidation = false;
public function behaviors()
{
	return [
	'verbs' => [
	'class' => VerbFilter::className(),
	'actions' => [
	'delete' => ['post'],
			],
		],
	];
}

public function actionIndex()
{

	$model = new Buttons;
	$model->scenario = 'serialize';
	
	if ($model->load(Yii::$app->request->post())) {
	
	$array = explode(',', $model->serialize );
	$i =1;
	
	foreach ($array as $elementy){
	
	$model = $this->findModel($elementy);
	$model->serialize = $i;
	$model->save();
	$i++;
	}
	
	return $this->redirect(['index']);
	} else {
	return $this->render('index', ['model' => $model,]);
	}

}

public function actionCreate()
	{
	$model = new Buttons();

	if ($model->load(Yii::$app->request->post()) && $model->save()) {

	$this->upload($model);

	return $this->redirect(['index']);
	} else {
	return $this->render('create', ['model' => $model,]);
	}
}


public function actionUpdate($id)
{
	$model = $this->findModel($id);

	if ($model->load(Yii::$app->request->post()) && $model->save()) {
		$this->upload($model);
		return $this->redirect(['index']);
	} else {
		return $this->render('update', ['model' => $model,]);
	}
}


public function actionDelete($id)
	{
		
try {
	if ($this->findModel($id)->delete()) {
		return $this->redirect(['index']);
	}

} catch (IntegrityException $e) {
	Yii::$app->session->setFlash('error', Yii::t('app', 'Problem with deleting button'));
}

	
	
	
	
	
}


protected function findModel($id)
{
	if (($model = Buttons::findOne($id)) !== null) {
	return $model;
	} else {
	throw new NotFoundHttpException('The requested page does not exist.');
	}
}

protected function upload($model)
{
	if ($model->validate()) {   

	if ($model->file = UploadedFile::getInstance($model, 'file')) {
	$file_name = BaseInflector::slug($model->file->baseName, "_");
	$model->image = $file_name . '-'.$model->id.'.' . $model->file->extension;
	
	if ($model->save()) { 
		Yii::$app->language == 'pl' ? 
						$patch = Url::to('@frontend/web/images/buttons/') :
						$patch = Url::to('@frontend/web/images/en/buttons/');
	$model->file->saveAs($patch . $file_name . '-'.$model->id.'.' . $model->file->extension);
	} else {
			print_r($model->getErrors());exit;
			}
	


	return true;
	} 
} 
}
}
