<?php
namespace backend\controllers;

use Yii;
use common\models\AnkietaUczelni;
use common\models\AnkietaUczelniSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class AnkietaUczelniController extends Controller
{
public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


public function actionIndex()
    {
        $nameForm = 'Form Uczelnie';
		
		$searchModel = new AnkietaUczelniSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		
        return $this->render('@backend/views/ankiety/index', [
            'nameForm' => $nameForm,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

public function actionView($id)
    {
        return $this->render('@backend/views/ankiety/view', ['model' => $this->findModel($id),]);
    }


public function actionCreate()
    {
        $model = new AnkietaUczelni();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['@backend/views/ankiety/view', 'id' => $model->id]);
        } else {
            return $this->render('@backend/views/ankiety/create', [
                'model' => $model,
            ]);
        }
    }

public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['@backend/views/ankiety/view', 'id' => $model->id]);
        } else {
            return $this->render('@backend/views/ankiety/update', [
                'model' => $model,
            ]);
        }
    }

public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = AnkietaUczelni::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
