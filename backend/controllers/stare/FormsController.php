<?php

namespace backend\controllers;

use Yii;
use common\models\Forms;
use common\models\FormsSearch;

use yii\web\Controller;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
/**
 * FormsController implements the CRUD actions for Forms model.
 */
class FormsController extends Controller
{
public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new FormsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate(){
		$m = new Forms();
		$r = Yii::$app->request;
	
		 if ($r->isAjax && $r->isPost) {
			$m->body = $r->post('body');
			\Yii::$app->response->format = Response::FORMAT_JSON;
			return ['success' => $m->save(), 'url' => Url::to(['index'])];
		} else {
			return $this->render('create');
		}
	}


   public function actionUpdate($id){
		$m = Forms::findModel($id);
		$r = Yii::$app->request;
	
		 if ($r->isAjax) {
			
			switch (true) { 
				case $r->isGet: echo $m->body; break;
				case $r->post('update'): 
					\Yii::$app->response->format = Response::FORMAT_JSON;

					$m->body = $r->post('body');
					return $m->save() ? ['success' => true] : ['success' => false]; 
				default: return $this->render('update');
			}
		} else {
			return $this->render('update');
		}
	
		
	}


    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    protected function findModel($id)
    {
        if (($model = Forms::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
