<?php
namespace backend\controllers;

use Yii;
use common\models\Category;
use common\models\SearchCategory;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class CategoriesController extends Controller{

	public function behaviors(){
		return [
			'access' => [
		            'class' => AccessControl::className(),
		            'only' => ['index', 'view', 'create', 'update', 'delete'],
		            'rules' => [
		                [
						'roles' => ['?'],
		                    'allow' => true,
		                    'actions' => ['login'],  
		                ],
		                [
						'roles' => ['@'],
		                    'allow' => true,
		                    'actions' => ['index', 'view', 'create', 'update', 'delete'],  
		                ],
		            ],
		        ],
		    'verbs' => [
		            'class' => VerbFilter::className(),
		            'actions' => ['delete' => ['post'],],
		        ],
		    ];
		}


	public function actionIndex(){
		    $searchModel = new SearchCategory();
		    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		    return $this->render('index', [
		        'searchModel' => $searchModel,
		        'dataProvider' => $dataProvider,
		    ]);
		}


	public function actionView($id){
		return $this->render('view', ['model' => $this->findModel($id),]);

	}


	public function actionCreate(){
		    $model = new Category();
		
		    if ($model->load(Yii::$app->request->post()) && $model->save()) {
		        return $this->redirect(['index']);
		    } else {
		        return $this->render('create', ['model' => $model]);
		    }
		}


	public function actionUpdate($id)
		{
		$model = $this->findModel($id);

		    if ($model->load(Yii::$app->request->post()) && $model->save()) {
		        return $this->redirect(['index']);
		    } else {
		        return $this->render('update', ['model' => $model,]);
		    }
		}


	public function actionDelete($id){
		    $this->findModel($id)->delete();
		    return $this->redirect(['index']);
		}


	protected function findModel($id)
		{
		if (($model = Category::findOne($id)) !== null) {
		        return $model;
		} else {
		        throw new NotFoundHttpException('The requested page does not exist.');
		    }
		}
}
