<?php

namespace backend\controllers;

use Yii;

use common\models\User;
use common\models\SearchUser;
use backend\models\SignupForm;

use yii\web\NotFoundHttpException;

class UserController extends \yii\web\Controller {
	public function behaviors()
		{
		    return [
			'access' => [
		            'class' => \yii\filters\AccessControl::className(),
		            'only' => ['login', 'logout', 'index', 'create'],
		            'rules' => [
		                [
		                    'allow' => true,
		                    'actions' => ['logout'],
		                    'roles' => ['@'],
		                ],
		                
		                [
		                    'allow' => true,
		                    'actions' => ['index', 'create'],
		                    'roles' => ['admin'],
		                ],
		                
		            ],
		        ],
		        'verbs' => [
		            'class' => \yii\filters\VerbFilter::className(),
		            'actions' => [
		                'delete' => ['post'],
		            ],
		        ],
		    ];
		}

	public function actionIndex()
		{
		    $searchModel = new SearchUser();
		    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
		        'searchModel' => $searchModel,
		        'dataProvider' => $dataProvider,
		    ]);
		    
		}


	public function actionView($id)
		{
		return $this->render('view', ['model' => $this->findModel($id),]);
		}


	public function actionCreate()
		{
		
			$model = new SignupForm();
		    if ($model->load(Yii::$app->request->post()))  {
		
		        if ($user = $model->signup()) {
		            if (Yii::$app->getUser()->login($user)) { return $this->goHome(); }
		        }
			
		    }
		    return $this->render('create', ['model' => $model,]);
		}


	public function actionUpdate($id)
		{
		    $model = $this->findModel($id);

		    if ($model->load(Yii::$app->request->post()) && $model->save()) {
		        return $this->redirect(['view', 'id' => $model->id]);
		    } else {
		        return $this->render('update', ['model' => $model]);
		    }
		}


	public function actionDelete($id)
		{
		    $this->findModel($id)->delete();
			return $this->redirect(['index']);
		}


	protected function findModel($id)
		{
		    if (($model = User::findOne($id)) !== null) {
		        return $model;
		    } else {
		        throw new NotFoundHttpException('The requested page does not exist.');
		    }
		}
	
}
