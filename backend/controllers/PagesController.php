<?php
namespace backend\controllers;

use Yii;
use common\models\Pages;
use common\models\PagesSearch;
use common\models\PagesAdd;
use common\models\Category;
use common\models\Relationships;

use yii\helpers\BaseInflector;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class PagesController extends Controller
{
public $enableCsrfValidation = false;

public function behaviors()
{
	return [
	'access' => [
			'class' => AccessControl::className(),
			'only' => ['index', 'view', 'create', 'update', 'delete'],
			'rules' => [
				[
				'roles' => ['?'],
					'allow' => true,
					'actions' => ['login'],
					
				],
				[
				'roles' => ['@'],
					'allow' => true,
					'actions' => ['index', 'view', 'create', 'update', 'delete'],
					
				],
			],
		],
		'verbs' => [
			'class' => VerbFilter::className(),
			'actions' => ['delete' => ['post'],],
		],
	];
}


public function actionIndex()
{
	$searchModel = new PagesSearch();
	$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

	return $this->render('index', [
		'searchModel' => $searchModel,
		'dataProvider' => $dataProvider,
	]);
}


public function actionView($id)
{
	return $this->render('view', [ 'model' => $this->findModel($id), ]);
}


public function actionCreate()
{
	$model = new Pages();
	$model->data = date('Y-m-j');
	$model->autor = Yii::$app->user->identity->Names;
	
	if ($model->load(Yii::$app->request->post())) {
	
		$model->cat_list = join(',', $model->category_id);
	
		if ($model->save()){
			
			foreach ($model->category_id as $elements):
				$relation = new Relationships;
				$relation->category_id = $elements;
				$relation->pages_id = $model->id;
				$relation->save();
			endforeach;
			
			
		PagesAdd::setPagesAddNews($model);
		PagesAdd::setPagesAddPages($model);
		}
		
		return $this->redirect(['view', 'id' => $model->id]);
	} else {
		return $this->render('create', ['model' => $model,]);}
}


public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

			$model->cat_list = join(',', $model->category_id);
		
			if ($model->save()) {
				
				Relationships::deleteAll(['pages_id' => $model->id]);
				foreach ($model->category_id as $elements):
					$relation = new Relationships;
					$relation->category_id = $elements;
					$relation->pages_id = $model->id;
					$relation->save();
				endforeach;
				
		
		
		PagesAdd::deleteAll(['pages_id' => $model->id]);	
		PagesAdd::setPagesAddNews($model);
		PagesAdd::setPagesAddPages($model);	

		
		

			}
		
			
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', ['model' => $model]);
        }
    }
	
public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

	
public function actionRelation()
    {
    $pages = Pages::findOne(196);
	$category = $pages->categories;
 
	foreach($category as $element) :
 
		echo $element->name.'<br>';
 
	endforeach;
 
    }

protected function findModel($id)
    {
        if (($model = Pages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
	
}