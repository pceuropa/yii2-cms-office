<?php
namespace backend\controllers;

use Yii;
use common\models\BannersFooter;
use yii\web\UploadedFile;
use yii\helpers\Url;
use \frontend\widgets\Url2;
class BannersFooterController extends \yii\web\Controller{

public $enableCsrfValidation = false;

public function behaviors(){
	return [
	'verbs' => [
	'class' => \yii\filters\VerbFilter::className(),
	'actions' => [
	'delete' => ['post'],
			],
		],
	];
}
   
        
public function actionIndex(){
    
	$model = new BannersFooter;
	$request = Yii::$app->request;
	
	if ($request->isAjax && $request->post('serialize')){
	
		$array = $request->post('array');
		
		foreach ($array as $key => $val){
	
			$m = $this->findModel($val);
			$m->serialize = $key;
			$m->save();
		}
		return  \yii\helpers\Json::encode(['success' => true,]);
	}
	
	
	
	if ($model->load($request->post())) {
		
		$this->uploadFile($model, 'files');
		$model->save();
				
		return $this->render('@views/images-sort/index', ['model' => $model]);
	} else {
		return $this->render('@views/images-sort/index', ['model' => $model]);
	}

}


public function actionUpdate($id){

	$model = $this->findModel($id);
	$request = Yii::$app->request;
	
	if ($model->load($request->post())) {
		
		$this->uploadFile($model, 'files');
		$model->save();
			
		return $this->redirect(['index']);
	} else {
		return $this->render('@views/images-sort/update', ['model' => $model,]);
	}
}

public function actionDelete($id){
	$this->findModel($id)->delete();

	return $this->redirect(['index']);
}

public function upload($id){
	$this->findModel($id)->delete();

	return $this->redirect(['index']);
}


protected function uploadFile($model, $nameInput){
		$file  = UploadedFile::getInstance($model, $nameInput);
		
		if ($file){
			$file_path = Url2::imgPath($model::IMG_FOLDER) . $file->name;
			
			if ($file->saveAs($file_path)){
				$model->image = $file->name;
			}
		} 
}


protected function findModel($id){
	if (($model = BannersFooter::findOne($id)) !== null) {
	return $model;
	} else {
	throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
	}
}


}
