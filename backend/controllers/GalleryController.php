<?php
namespace backend\controllers;
use Yii;

use common\models\Albums;
use common\models\AlbumsSearch;
use common\models\Photos;

use yii\helpers\Url;
use frontend\widgets\Url2;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\BaseInflector;


class GalleryController extends Controller {

	public $enableCsrfValidation = true;

	public function behaviors() {
		    return [
		        'verbs' => [
		            'class' => VerbFilter::className(),
		            'actions' => [
		                'delete-photo' => ['post'],
		                'delete' => ['post'],
		            ],
		        ],
		    ];
	}
	

	public function actionIndex() {
		$searchModel = new AlbumsSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			]);
	}

	public function actionView($id) {

		$img = Photos::find() ->where(['id_album' => $id]) ->orderBy('id') ->all();
		$photos = new Photos();

		if (Yii::$app->request->isPost) {
		        $photos->file = UploadedFile::getInstances($photos, 'file');
		        
		        if ($photos->file && $photos->validate()) {
			
		            foreach ($photos->file as $file) {
		            
						$photo = new Photos();
						$photo->photo = BaseInflector::slug($file->baseName, "_") .'_'.$id.'.' . $file->extension;
						$photo->id_album = $id;
						$photo->save();
							$f = Url2::imgPath('galeria/max');
				
						$file->saveAs($f.$photo->photo);
		            }
					return $this->redirect(['view', 'id' => $id]);
		        } 
		}
	
	return $this->render('view', [ 'model' => $this->findModel($id), 'img' => $img, 'photos' => $photos,]);

	}


	public function actionCreate() {
		$model = new Albums();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
	
		return $this->redirect(['view', 'id' => $model->id]);
		} else {
		return $this->render('create', [ 'model' => $model, ]);}
	
	}


	public function actionUpdate($id) {
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
		return $this->redirect(['view', 'id' => $model->id]);
			} else {
		return $this->render('update', [ 'model' => $model, ]);
	}
	}


	public function actionDeletePhoto($id, $album) {
		$this->findModel2($id)->delete();
		return $this->redirect(['view', 'id' => $album]);
	}

	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}


	protected function findModel($id) {
		if (($model = Albums::findOne($id)) !== null) {return $model;
			} else {
		throw new NotFoundHttpException('The requested page does not exist.'); 				}
	}

	protected function findModel2($id) {
		if (($model = Photos::findOne($id)) !== null) { return $model;
			} else {
		throw new NotFoundHttpException('The requested page does not exist.');
			}
	}
}
