<?php
namespace backend\controllers;

use Yii;
use common\models\News;
use common\models\SearchNews;
use common\models\PagesAdd;

use yii\helpers\BaseInflector;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\helpers\Url;

class NewsController extends Controller
{public $enableCsrfValidation = false;

public function behaviors()
    {
        return [
		'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
					'roles' => ['?'],
                        'allow' => true,
                        'actions' => ['login'],
                        
                    ],
                    [
					'roles' => ['@'],
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


public function actionIndex()
    {
    $searchModel = new SearchNews();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
			'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


public function actionView($id)
    {
        return $this->render('view', ['model' => $this->findModel($id),]);
    }


public function actionCreate()
{
	$model = new News();
	$model->data = date('Y-m-j');
	$model->autor = Yii::$app->user->identity->Names;
	
    if ($model->load(Yii::$app->request->post())) {
	
		if (is_array($model->tags)) {
			$model->tags = join(',', $model->tags);
		}
		
		if ($model->save()) {
			$this->upload($model);
			PagesAdd::setNewsAddPages($model);
		}
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', ['model' => $model,]);}
    }


public function actionUpdate($id)
    {
    $model = $this->findModel($id);
	
    if ($model->load(Yii::$app->request->post())) {

		if (is_array($model->tags)) {
			$model->tags = join(',', $model->tags);
		}
		
		if ($model->save()) {
			$this->upload($model);
			PagesAdd::deleteAll(['news_add' => $model->id]);	
			PagesAdd::setNewsAddPages($model);
		}
		
		return $this->redirect(['view', 'id' => $model->id]);
	} else {
		return $this->render('update', ['model' => $model,]);
	}
 }

public function actionDelete($id)
{
	$this->findModel($id)->delete();
	return $this->redirect(['index']);
}


protected function findModel($id)
{
	if (($model = News::findOne($id)) !== null) {
		return $model;
	} else {
		throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
	}
}
	
protected function upload($model)
{
	if ($model->validate()) {   

		if ($model->file = UploadedFile::getInstance($model, 'file')) {
			$file_name = BaseInflector::slug($model->file->baseName, "_");
			$model->zdjecie = $file_name . '-'.$model->id.'.' . $model->file->extension;
			

			Yii::$app->language == 'pl' ? 
				$f = Url::to('@frontend/web/images/news/') : 
				$f = Url::to('@frontend/web/images/en/news/');
			
			if ($model->save()) { 
			$model->file->saveAs(
				$f . $file_name . '-'.$model->id.'.' . $model->file->extension);
			} else {
				print_r($model->getErrors());exit;
			}
			
		return true;
		} 
	} 
return false;
}

}