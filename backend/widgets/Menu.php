<?php
namespace backend\widgets;
use Yii;

class Menu {
	public function PcMenu(){

		return  [
					['label' => Yii::t('app', 'Pages'), 'url' => ['/pages']],
					['label' => Yii::t('app', 'News'), 'url' => ['/news']],
					['label' => Yii::t('app', 'Calendar'), 'url' => ['/calendar']],
					['label' => Yii::t('app', 'Gallery'), 'url' => ['/gallery/index']],


					['label' => 'Form', 'items' => [
                        ['label' => Yii::t('app', 'Generator wersja produkcjyna'), 'url' => ['/forms']],
                        ['label' => Yii::t('app', 'Zielona Europa'), 'url' => ['/form-zielona-europa']],
						['label' => Yii::t('app', 'Tydzień Regionów'), 'url' => ['/form-tydzien-regionow']],
						['label' => Yii::t('app', 'Technologie Medyczne'), 'url' => ['/form-tech-med']],
						['label' => Yii::t('app', 'Uczelnie'), 'url' => ['/form-uczelnie']],
						['label' => Yii::t('app', 'Ankieta Uczelni'), 'url' => ['/ankieta-uczelni']]

					]],
					['label' => Yii::t('app', 'Inne'), 'items' => [
						['label' => Yii::t('app', 'Menus'), 'url' => ['/menu/creator/']],
						['label' => Yii::t('app', 'Column right'), 'url' => ['/buttons/index']],
						['label' => Yii::t('app', 'Footer banners'), 'url' => ['/banners-footer/index']],
						['label' => Yii::t('app', 'Users'), 'url' => ['/user/index']],
						['label' => Yii::t('app', 'Categories'), 'url' => ['/categories/index']],
					]]
			];
	}

}
