<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'pomorskie-backend',
		'homeUrl' => '/panelx',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [    'menu' => [
            'class' => '\pceuropa\menu\Module',
        ],
        
        ],
        
        
        
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
		'request' => [
            'baseUrl' => '/panelx',
            'enableCsrfValidation'=>false
        ],
		'urlManager' => [
                'rules' => [
               '' => 'site/index',
               	'<controller:\w+>' => '<controller>/index',
				'<controller:\w+>/<id:\d+>' => '<controller>/view',
				'<controller:\w+>/update/<id:\d+>' => '<controller>/update',
				'<controller:\w+>/delete/<id:\d+>' => '<controller>/delete',
                ],
],

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];
