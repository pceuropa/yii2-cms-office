<?php
return [
	'root_img' => '/var/www/yii2-pomorskie/frontend/web/images/',
	'root_img_en' => '/var/www/yii2-pomorskie/frontend/web/images/en/',
	
	'root_img_gallery' => '/var/www/yii2-pomorskie/frontend/web/images/galeria\max/',
	'root_img_gallery_en' => '/var/www/yii2-pomorskie/frontend/web/images/\en\galeria\max/',
	
	'img_gallery' => 'http://pomorskieregion.eu/images/galeria/max/',
	'img_gallery_en' => 'http://pomorskieregion.eu/images/en/galeria/max/',
	
	'root_img_news' => '/var/www/yii2-pomorskie/frontend/web/images/news/',
	'root_img_news_en' => '/var/www/yii2-pomorskie/frontend/web/images/en/news/',
	
	'root_img_banners' => '/var/www/yii2-pomorskie/frontend/web/images/banners/',
	'root_img_banners_en' => '/var/www/yii2-pomorskie/frontend/web/images/en/banners/',
	
	'root_img_buttons' => '/var/www/yii2-pomorskie/frontend/web/images/buttons/',
	'root_img_buttons_en' => '/var/www/yii2-pomorskie/frontend/web/images/en/buttons/',
	

];
